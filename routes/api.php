<?php

use Illuminate\Http\Request;

Route::post('/login','API\AuthController@login');
Route::post('/register','API\AuthController@register');

Route::get('/user/{token?}','API\AuthController@user');

Route::post('branch/{branch_id?}/reservation','API\OrderController@reservation');

Route::get('order/{token?}','API\OrderController@index');
Route::post('order','API\OrderController@store');
Route::get('order/{id?}/{token?}','API\OrderController@show');
Route::get('order/delete/{id?}/{token?}','API\OrderController@destroy');
Route::get('most/order','API\OrderController@most_order');

Route::resource('feedback','API\FeedbackController');

Route::get('message/{token?}','API\MessageController@index');

Route::resource('like','API\LikeController');

Route::get('offer','API\OfferController@index');

Route::resource('ad','API\AdController');

Route::get('branch/nearest/lat/{lat?}/lng/{lng?}','API\BranchController@nearestBranch');
Route::get('branch/{id?}/offer','API\BranchController@show_offer');
Route::get('branch/{id?}','API\BranchController@show');
Route::get('branch/{id?}/menu','API\BranchController@show_menu');
Route::get('branch/menu/{id?}/food','API\BranchController@show_menu_food');
Route::get('branch/menu/food/{id?}/details','API\BranchController@show_menu_food_detials');
Route::get('branch/filter/type/{type?}/area/{area_id?}','API\BranchController@area_filter');
Route::get('branch/filter/type/{type?}','API\BranchController@type_filter');


Route::get('area/{id?}','API\CountryController@area');
Route::resource('country','API\CountryController');
