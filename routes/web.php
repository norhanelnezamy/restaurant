<?php

Route::group(['prefix' => '/admin'], function () {
  Route::get('/login','Admin\AuthController@getLogin');
  Route::post('/login','Admin\AuthController@postLogin');
  Route::group(['middleware' => 'auth:admin'] , function () {

    Route::get('/setting/social','Admin\SettingController@getScoial');
    Route::post('/setting/social','Admin\SettingController@postScoial');

    Route::get('/setting/about','Admin\SettingController@getAbout');
    Route::post('/setting/about','Admin\SettingController@postAbout');

    Route::get('/setting/why','Admin\SettingController@getWhy');
    Route::post('/setting/why','Admin\SettingController@postWhy');

    Route::get('/setting/what','Admin\SettingController@getWhat');
    Route::post('/setting/what','Admin\SettingController@postWhat');

    Route::resource('/site/feedback','Admin\SiteFeedbackController');

    Route::get('/b/brand/{type?}','Admin\BrandController@index');
    Route::resource('/brand','Admin\BrandController');

    Route::resource('/branch','Admin\BranchController');

    Route::post('/ad/active/{id?}','Admin\AdController@active');
    Route::resource('/ad','Admin\AdController');

    Route::get('/logout', 'Admin\AuthController@getLogout');
    Route::get('/about','Admin\IndexController@getAbout');
    Route::post('/about','Admin\IndexController@postAbout');
    // Route::resource('/feedback','Admin\FeedbackController');
    Route::resource('/admin','Admin\AdminController');
    Route::resource('/category','Admin\CategoryController');

    Route::get('city/{country?}/create','Admin\CityController@create');
    Route::get('area/{city?}/create','Admin\AreaController@create');
    Route::resource('city','Admin\CityController');
    Route::resource('/country','Admin\CountryController');
    Route::resource('/user','Admin\UserController');
    Route::post('/send/email','Admin\IndexController@postSendEmail');
    Route::get('/','Admin\IndexController@getIndex');

  });

});

Route::get('lang/switch/{lang}', 'Language@switchLanguage');

Route::get('active/account/{id}/{token}', 'AuthController@active');

Route::group(['middleware' => 'auth:web'] , function () {

  Route::group(['prefix' => '/dashboard'], function () {
    Route::group(['middleware' => 'dashboard'] , function () {
      Route::resource('/ad', 'Restaurant\AdController');
      Route::resource('/feedback', 'Restaurant\FeedbackController');
      Route::resource('/branch', 'Restaurant\BranchController');
      Route::resource('/brand', 'Restaurant\BrandController');
      Route::resource('/feedback', 'Restaurant\FeedbackController');
      Route::resource('/index', 'Restaurant\IndexController');

    });
  });

  Route::get('/msg', 'Restaurant\IndexController@msg');

  Route::group(['prefix' => '/dashboard/b'], function () {
    Route::group(['middleware' => 'branch_authorize'] , function () {
      Route::get('message/accept/order/{id}', 'Branch\MessageController@acceptMessage');
      Route::get('message/refuse/order/{id}', 'Branch\MessageController@refuseMessage');
      Route::get('message/order/{id}', 'Branch\MessageController@getSendMessage');
      Route::post('message/order/{id}', 'Branch\MessageController@postSendMessage');
      Route::resource('/photo', 'Branch\PhotoController');
      Route::resource('/offer', 'Branch\OfferController');
      Route::get('/order/{id?}', 'Branch\OrderController@show');
      Route::get('/order/index/{type}', 'Branch\OrderController@index');
      Route::get('/order/r/reservation', 'Branch\OrderController@reservation');
      Route::resource('/additional', 'Branch\AdditionalController');
      Route::get('/feedback', 'Branch\FeedbackController@index');
      Route::resource('/order', 'Branch\OrderController');
      Route::resource('/food', 'Branch\FoodController');
      Route::resource('/menu', 'Branch\MenuController');
    });
  });

  Route::get('/logout','AuthController@logout');

  Route::post('user/data/update','UserController@updateData');
  Route::post('user/password/update','UserController@updatePassword');
  Route::get('user/profile','UserController@profile');
  Route::get('user/me/message','UserController@message');
  Route::get('user/me/order','UserController@order');

});

Route::resource('cart','Site\CartController');
Route::get('checkout/delivery/{id?}','Site\OrderController@checkout');
Route::post('brand/{brand_id?}/branch/{branch_id?}/reservation','Site\OrderController@reservation');
Route::resource('order','Site\OrderController');

Route::get('branch/offer','Site\BrandController@offer');
Route::get('branch/{branch_id?}/menu/{menu_id?}/food','Site\BrandController@food');

Route::get('brand/show/{id?}','Site\BrandController@show');
Route::get('brand/feedback/{id?}','Site\BrandController@getFeedback');
Route::post('brand/feedback/{id?}','Site\BrandController@postFeedback');
Route::get('brand/p/category/{id?}','Site\BrandController@category');
Route::get('brand/{type?}','Site\BrandController@index');
Route::post('brand/{type?}','Site\BrandController@filter');
Route::post('brand/filter/restaurant','Site\BrandController@filterRestaurant');

Route::resource('site/feedback', 'Site\SiteFeedbackController');
Route::resource('country','Site\CountryController');

Route::get('area/{id?}','Site\CountryController@area');
Route::get('set/area/{id?}','Site\CountryController@set_area');

Route::get('/terms','Site\IndexController@getTerms');

Route::get('/register','AuthController@getRegister');
Route::post('/register','AuthController@postRegister');

Route::post('/restaurant/register','Restaurant\BrandController@store');

Route::get('/login','AuthController@getLogin')->name('login');
Route::post('/login','AuthController@postLogin');

Route::get('/', 'Site\IndexController@index');
