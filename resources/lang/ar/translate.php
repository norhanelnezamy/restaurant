<?php

return [
  'authorize' => 'خطأ ف بيانات المستخدم .',
  'login_title' => 'تسجيل دخول ',
  'register_title' => 'تسجيل  ',
  'register_type' => 'نوع تسجيل',
  'add_category' => 'اضافة فئه',
  'Ar_name' => 'اسم بالعربى ',
  'En_name' => ' اسم بالانجليزى ',

  'Ar_name_' => 'اسم الوجبه عربى ',
  'En_name_' => ' اسم الوجبه بالانجليزى ',
  'Ar_name__' => ' التفاصيل بالعربى ',
  'En_name__' => '  التفاصيل بالانجليزى ',

  'save_' => ' حفظ',
  'delet_' => 'حذف',
  'browse_' => 'تصفح ',
  'edit_' => 'تعديل ',
  'food_menu' => 'قائمة الطعام ',
  '_menu' => 'القائمه ',
  'additions_' => 'الاضافات ',
  'additions_meal' => 'اضافات الوجبه ',

  'additional_menu' => 'قائمة الاضافات ',

  'add_meals' => 'اضافة وجبات',
  'additions_' => 'الاضافات ',
  'add_meals' => 'اضافة وجبة ',
  'Offers_' =>'العروض',
  'Browse_Offers' =>'تصفح العروض ',
  'Add_Offers' =>'اضافة عروض ',
  'table_reservation' =>'حجز طاوله',
  'processing_order' =>'تجهيز طلب ',
  'deliveries' =>'توصيل الدليفرى ',
  'rating' =>'التقييمات ',
  'type_' =>'النوع',
  'price_' =>'السعر ',
  'select_type' =>'اختر النوع',
  'salad' =>'سلطه',
  'drink' =>'مشروبات',
  'appetizer' =>'مقبلات',
  'en_describe'=>'الوصف بالعربي ',
  'ar_describe'=>'الوصف بالانجليزي',
  'other' =>'اخرى',
  'details_' =>'تفاصيل الوجبه ',
  'select_category' => 'اختر الفئه ',
  'image_upload' =>'تحميل صوره ',
  'meal_size' =>'الاحجام المتاحة ',
  'Ar_name' =>'الاسم عربى ',
  'En_name' =>'الاسم انجليزى ',
  'meal_size' =>'احجام الوجبة ',
  'indx'=>'الرئيسيه ',
  'all_restaurant'=>'كل المطاعم ',
  '_restaurant'=>'مطعم',
  'rest_name'=>'اسم المطعم ',
  'five_stars'=>'مطاعم راقيه ',
  'families_prroduced'=>'الاسر المنتجه ',
  'log_out'=>'تسجيل خروج ',
  'sin_in'=>'تسجيل خول ',
  'sin_up'=>'تسجيل ',
  'arabic'=>'عربى',
  'discover_location'=>'اكتشف موقعك',
  'country'=>'الدوله',
  'city'=>'المدينه',
  'district'=>'الحى ',
  'rest_type'=>'نوع المطاعم ',
  'discover_food'=>'اكتشف طعامك',
  'rest_Offers'=>'عروض المطاعم ',
  'add_offer'=>'اضف عرض',
  'view_more'=>'عرض المزيد ',
  'rest_ads'=>'اعلانات المطاعم',
  'rest_popular'=>'مطاعم اكثر طلبا ',
  'cutomers'=>'آراء العملاء ',
  'about'=>'عن اسرع طعام ',
  'great_food'=>'الاكلات المميزه',
  'rest_type'=>'انواع المطاعم',
  'follow_us'=>'تابعنا على ',
  'name_'=>'الاسم',
  'email_'=>'الايميل',
  'phone_'=>'الجوال ',
  'password_'=>'كلمة المرور ',

  'applicant'=>'طالب خدمه ',
  'rest_owner'=>'مالك مطعم ',
  'terms_'=>'شروط التسجيل ',
  'agreement_'=>'اوافق على شروط التسجيل ',
  'regist_'=>'تسجيل',

  'one_size'=>'حجم واحد',
  'large'=>'كبير',
  'medium'=> 'وسط',
  'small'=>'صغير',

  'authorized' => 'خطأ في رقم الجوال او كلمة المرور .',
  'delete_message'=> 'تم عملية المسح .',
  'update_message'=> 'تم عملية التعديل .',
  'add_message'=> 'تم عملية الاضافة بنجاح .',

  'offer_details' =>'تفاصيل العرض ',
  'add_account'=> 'تم انشاء الحساب من فضلك قم بتفعيل الحساب .',

  'info' => 'المعلومات',
  'show_branch' => 'تصفح الفروع',
  'add_branch' => 'اضافة فرع',
  'feedback' => 'التقييمات',
  'ad' => 'الاعلانات',
  'show_ad' => 'تصفح الاعلانات',
  'add_ad' => 'اضافة اعلان',

  'ar_title' => 'العنوان بالعربي ',
  'en_title' => 'العنوان بالانجليزي',
  'photo'=>'الصور',
  'smart_restaurant'=>'المطاعم الراقية ',
  'productive_family'=>'الاسر المنتجة',
  'restaurant'=>'جميع المطاعم',
  'numder'=>'عدد الافراد ',
  'date'=>'التاريخ ',
  'time'=>'الوقت ',
  'occasion'=>'المناسبة ',
  'other_phone'=>'رقم اخر للتواصل',
  'floor'=>'رقم الطابق',
  'flat_number'=>'رقم الشقة',
  'details'=>'التفاصيل',
  'notes'=>'الملاحظات',

  'header_title_1' => 'اطلب الأكل اللى نفسك فية !',
  'header_title_2'=> 'اكتشف مطاعم محلية تصل الى باب منزلك',
  'footer_1'=>'جميع الحقوق محفوظة لموقع ',
  'title'=>'اسرع طعام',
  'footer_2'=>'تصميم وبرمجة شركة ',
  'company'=>'خليج البرمجة',
  'add_site_feedback'=>'اضف رأيك عن الموقع',
  'account'=>'الحساب',
  'user_account'=>'حساب المستخدم',
  'change_password'=>'تغيير كلمة المرور',
  'name'=>'الإسم',
  'email'=>'البريد الإلكترونى',
  'phone'=>'الجوال',
  'new_password'=>'كلمة المرور الجديدة',
  'confirm_password'=>'تأكيد كلمة المرور',
  'my_order'=>'طلباتى',
  'wait_orders'=>'طلبات قيد التنفيذ',
  'prev_orders'=>'الطلبات السابقة',
  'messages'=>'الرسائل',
  'about_brand'=>'عن المطبخ',
  'min_charge'=>'الحد الادنى للطلب',
  'work_time'=>'ساعات العمل',
  'to'=>'الى',
  'from'=>'من',
  'brand_type'=>'انواع المطابخ',
  'address' =>'مكان الإستلام',
  'reservation'=>'حجز طاولة',
  'prepare'=>'تجهيز طلب',
  'delivery'=>'خدمة التوصيل',
  'services'=>'حسن التعامل',
  'food_feedback'=>'جودة الأكل',
  'sum'=>'المجموع',
  'submit_order'=>'تنفيذ الطلب',
  'add_feedback'=>'شاركنا رأيك',
  'feedback_notes'=>'يرجى المشاركة بأى ملاحظات اخرى حول تجربتك مع اسرع طعام',
  'send'=>'ارسال',
  'write_comment'=>'اكتب تعليقك هنا',
  'more'=>'المزيد',
  'choose_order'=>'خيارات الطلب',
  'order'=>'طلبات',
  'reservation_submit'=>'تأكيد الحجز',
  'menu_catgory'=>'فئات قائمة الطعام',
  'super_food'=>'الوجبات القيمة',
  'multiple_additonal'=>'يمكنك اختيار عدد اصناف غير محدد',
  'add'=>'اضافة',
  'galary'=>'معرض الصور',
  'information'=>'معلومات',
  'feedbacks'=>'تقييمات',
  'menu'=>'الاصناف',
  'cart'=>'سلة مشترياتك',
  'empty_cart'=>'لايوجد اصناف فى سلة المشتريات',
  'add_notes'=>'أضف ملاحطاتك هنا',
  'admin_panel'=>'لوحة التحكم' ,
  'write_message'=>'اكتب رسالتك هنا' ,
  'send_msg'=>'ارسال رسالة',
  'chat'=>'مراسلة',
  'save'=>'حفظ',
  'add'=>'اضافة',
  'add_image'=>'اضافة صورة',
  'add_logo'=>'اضافة لوجو',
  'about_'=>'عن المطعم',
  'image'=>'الصورة ',
  'update'=>'تعديل',
  'delete'=>'مسح',
  'show'=>'مشاهدة',
  'descripe'=>'الوصف',
  'price'=>'السعر',
  'city'=>'المدينة',
  'area'=>'الحى',
  'commercial_registry'=>'السجل التجارى',
  'civil_registry'=>'السجل المدنى'  ,
  'management'=>'الادارة',
  'manager_name'=>'اسم المدير',
  'data'=>'البيانات',
  'delivery_price'=>'سعر التوصيل',
  'address_'=>'العنوان',
  'restaurant_photo'=>'صور المطعم',
  'search_categories'=>'البحث عن اصناف',
];
