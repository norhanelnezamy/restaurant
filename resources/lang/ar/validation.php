<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'alpha'                => 'يجب ادخال :attribute حروف .',
    'alpha_dash'           => 'يجب ادخال :attribute حروف , ارقام , - فقط .',
    'alpha_num'            => 'يجب ادخال :attribute حروف وارقام فقط .',
    'between'              => [
        'numeric' => ' يجب ادخال :attribute  بين :min and :max.',
        'file'    => 'يجب ادخال :attributeبين :min and :max kilobytes.',
        'string'  => 'يجب ادخال :attribute  بين :min and :max حروف .',
        'array'   => 'يجب ادخال :attribute  بين :min and :max عنصر .',
    ],
    'confirmed'            => ':attribute غير متطابقة .',
    'date'                 => 'صيغة :attribute غير صحيحة .',
    'email'                => 'يجب ادخال :attribute صحيح ',
    'image'                => 'يجب اختيار :attribute صحيح',

    'integer'              => 'يجب ادخال  :attributeعدد صحيح .',
    'max'                  => [
        'numeric' => 'يجب ان لا يزيد :attribute  عن :max.',
        'file'    => 'يجب ان لا يزيد :attribute مساحة الملف عن :max kilobytes.',
        'string'  => 'يجب ان لا يزيد :attribute عدد الحروف عن :max .',
        'array'   => 'يجب ان لا يزيد :attribute عدد القيم  عن :max .',
    ],
    'mimes'                => 'يجب ادخال  :attribute الملف بصيغة :values. ',
    'mimetypes'            =>  'يجب ادخال  :attribute الملف بصيغة :values. ',
    'min'                  => [
        'numeric' => 'يجب ادخال :attribute قيمة على الاقل :min. ',
        'file'    => 'يجب ادخال :attribute على الاقل :min kilobytes. ',
        'string'  => 'يجب ادخال  :attribute  على الاقل :min حرف.',
        'array'   => 'يجب ادخال :attribute عدد عناصر على الاقل :min عنصر.',
    ],
    'not_in'               => ' المختارة غير صحيحة :attribute .',
    'numeric'              => 'يجب ادخال :attribute قيمة عددية .',
    'regex'                => 'يجب ادخال :attribute صيغة صحيحة .',
    'required'             => 'يجب ادخال :attribute',
    'size'                 => [
        'numeric' => 'يجب ادخال :attribute  يساوى :size.',
        'file'    => 'يجب ادخال :attribute  يساوي :size kilobytes.',
        'string'  => 'يجب ادخال :attribute  كلمة تساوي :size .',
        'array'   => 'يجب ادخال :attribute  العناصر يساوي  :size عنصر .',
    ],
    'string'               => 'يجب ادخال :attribute قيمة string .',
    'unique'               => 'تم استخدام :attribute من قبل .',

    'required_if'          => 'يجب ادخال قيمة :attribute طبقا لاختيار :other-:value.',
    'required_without_all' => 'يجب اختيار حجم واحد علي الاقل" :attribute-:values "',


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attrيجب ادخال قيمةibute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
      'name' => 'الاسم',
      'password' => 'كلمه المرور ',
      'phone'=>'رقم الجوال ',
      'email'=>'البريد الالكتروني ',
      'role'=> 'النوع ',
      'country_id'=>'الدولة',
      'password'=>'كلمة المرور ',
      'restaurant_name'=>'اسم المطعم ',
      'terms'=>'اختيار موافقة الشروط ',
      'category_id'=>'نوع المطبخ ',
      'commercial_registry'=>'السجل التجاري ',
      'civil_registry'=>'السجل المدني',
      'address'=>'العنوان',
      'delivery'=>'خدمه التوصيل',
      'ar_name'=>'الاسم بالعربي',
      'en_name'=>'الاسم بالانجليزي',
      'price'=>'السعر',
      'type'=>'النوع',
      'area_id'=>'الحي',
      'menu_id'=>'فئة قائمة الطعام',
      'photo'=>'الصورة',
      'ar_food_name'=>'الاسم بالعربي',
      'en_food_name'=>'الاسم بالانجليزي',
      'ar_food_describe'=>'الوصف بالعربي',
      'en_food_describe'=>'الوصف بالانجليزي',
      'one_size'=>'حجم واحد',
      'one_size_price'=>'السعر',
      'large'=>' الكبير',
      'large_price'=>'السعر',
      'medium'=>' الوسط',
      'medium_price'=>'السعر',
      'small'=>' الصغير',
      'small_price'=>'السعر',
      'food' => 'تقييم الطعام ',
      'delivary'=>'تقييم سرعة التوصيل ',
      'services'=>'تقييم الخدمة ',
      'ar_title' => 'العنوان بالعربي ',
      'en_title' => 'العنوان بالانجليزي',
      'ar_content'=> 'المحتوى بالعربي ',
      'en_content'=>'المحتوي بالانجليزي ',
      'facebook'=>'رابط الفيسبوك ',
      'twitter'=>'رابط تويتر ',
      'instgram'=>'رابط انستجرام ',
      'google'=>'رابط جوجل بلس ',
      'numder'=>'عدد الافراد ',
      'date'=>'التاريخ ',
      'time'=>'الوقت ',
      'occasion'=>'المناسبة ',
      'other_phone'=>'رقم اخر للتواصل',
      'floor'=>'رقم الطابق',
      'flat_number'=>'رقم الشقة',
      'details'=>'التفاصيل',
      'notes'=>'الملاحظات',
      'latLng'=> 'المكان على خريطة جوجل',
    ],

];
