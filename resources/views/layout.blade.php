<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="robots" content="index/follow">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="HandheldFriendly" content="true">

    <title>Fast Food</title>
    <link rel="shortcut icon" type="image/png" href="{{asset('public/site/images/fav-icon.png')}}">

    <meta name="theme-color" content="#d00000">
    <meta name="msapplication-navbutton-color" content="#d00000">
    <meta name="apple-mobile-web-app-status-bar-style" content="#d00000">

    <link rel="stylesheet" type="text/css" href="{{asset('public/site/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/site/css/icofont.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/site/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/site/css/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/site/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/site/css/style.css')}}">
    @if (App::isLocale('en'))
      <link rel="stylesheet" type="text/css" href="{{asset('public/site/css/style-en.css')}}">
    @else
      <link rel="stylesheet" type="text/css" href="{{asset('public/site/css/style-ar.css')}}">
    @endif

    <script src="{{asset('public/site/js/jquery-1.11.1.min.js')}}"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->

</head>

<body id="top">

<!--===============================
    LOADER
===================================-->

<div class="pre-load">
    <div class="loader"></div>
</div>


<div class="header parallax-window" data-parallax="scroll" data-image-src="{{asset('public/site/images/main-header-bg.png')}}">


    <!--===============================
        NAV
    ===================================-->

    <nav class="navbar">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNavbar" aria-expanded="false">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="{{asset('/')}}" class="navbar-brand" title="Fast Food"><img src="{{asset('public/site/images/logo.png')}}" alt="LOGO"></a>
                </div>

                <div class="collapse navbar-collapse" id="mainNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="home"><a href="{{asset('/')}}" data-count="1" title="الرئيسية" >@lang('translate.indx')</a></li>
                        <li class="restaurant"><a href="{{asset('/brand/restaurant')}}" data-count="2" title="جميع المطاعم"> @lang('translate.all_restaurant')</a></li>
                        <li class="smart_restaurant"><a href="{{asset('/brand/smart_restaurant')}}" data-count="3" title="المطاعم الراقية"> @lang('translate.five_stars')</a></li>
                        <li class="productive_family"><a href="{{asset('/brand/productive_family')}}" data-count="4" title="الأسر المنتجة"> @lang('translate.families_prroduced')</a></li>
                    </ul>
                    @if (session()->has('nav'))
                      <?php $nav = session()->get('nav');?>
                      <script>
                      $(function(){
                        $('li.{{ $nav }}').addClass('active');
                      });
                      </script>
                    @endif
                    <ul class="nav navbar-nav navbar-left">
                      @if (Auth::check())
                            <li class="dropdown">
                                <a class="dropdown-toggle hidden-xs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ asset('user/profile') }}"><i class="icofont icofont-user-alt-3"></i>حسابى</a></li>
                                    <li><a href="{{ asset('user/me/message') }}"><i class="icofont icofont-mail"></i>الرسائل</a></li>
                                    <li><a href="{{ asset('user/me/order') }}"><i class="icofont icofont-table"></i>طلباتى</a></li>
                                    <li><a href="{{ asset('logout') }}">@lang('translate.log_out')</a></li>
                                </ul>
                            </li>
                        @else
                        <li><a href="{{ asset('login') }}" title="دخول">@lang('translate.sin_in')</a></li>
                        <li><a href="{{ asset('register') }}" title="تسجيل">@lang('translate.sin_up')</a></li>
                      @endif
                    </ul>
                    <ul class="nav navbar-nav navbar-left no-border">
                        <li><a href="{{ asset('lang/switch/ar') }}" title="عربي">@lang('translate.arabic')</a></li>
                        <li><a href="{{ asset('lang/switch/en') }}" title="English">English</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>



    <!--===============================
        MAIN CONTENT
    ===================================-->

    @yield('location_header')

</div>

<!--===============================
    MAIN SECTION
===================================-->

@yield('content')

@if (session()->has('add_account'))
  <div class="modal fade" id="message" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="panel-body">
                  <p style="color:green">@lang('translate.add_acount')</p>
              </div>
          </div>
      </div>
  </div>

  <script>
  $(function(){
    $('#message').modal('show');
  });
  </script>
@endif

<!--===============================
    FOOTER
===================================-->
<?php
$categories = category();
$offers = offer();
$productive_families = productive_family();
?>
<footer>

    <div class="text-center logo-holder">
        <div>
            <img src="{{asset('public/site/images/footer-logo.png')}}" alt="logo">
        </div>
    </div>

    <div class="div-padding">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <h1 class="f-header">@lang('translate.great_food')</h1>

                    @foreach ($offers as $key => $offer)
                      @if ($key == 4)
                        <?php break?>
                      @endif
                      <a href="{{ asset('branch/'.$offer->brand_id) }}" class="table-view din-font animate-box left-in">
                        <div class="table-cell"><img src="{{asset('/public/uploads/'.$offer->photo)}}"></div>
                        <div class="table-cell">
                          <p>
                            @if (App::isLocale('en'))
                              {{ $offer->en_name }}
                            @else
                              {{ $offer->ar_name }}
                            @endif
                          </p>
                          <div class="table-view">
                            <div class="table-cell">
                              @if (App::isLocale('en'))
                                {{ $offer->en_describe }}
                              @else
                                {{ $offer->ar_describe }}
                              @endif
                            </div>
                          </div>
                        </div>
                      </a>
                      <hr>
                    @endforeach

                </div>


                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <h1 class="f-header">@lang('translate.rest_type')</h1>

                    @foreach ($categories as $key => $category)
                      <a href="#" class="table-view din-font animate-box left-in">
                        {{-- <div class="table-cell sm"><img src="{{asset('public/site/images/f-2-1.png')}}"></div> --}}
                        <div class="table-cell">
                          @if (App::isLocale('en'))
                            {{ $category->en_name }}
                          @else
                            {{ $category->ar_name }}
                          @endif
                        </div>
                      </a>
                      <hr>
                    @endforeach
                </div>

                <div class="clearfix hidden-lg"></div>

                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <h1 class="f-header">@lang('translate.families_prroduced')</h1>
                    @foreach ($productive_families as $key => $family)
                      <a href="#" class="table-view din-font animate-box left-in">
                        <div class="table-cell"><img src="{{asset('/public/uploads/'.$family->logo)}}"></div>
                        <div class="table-cell">
                          <p>
                            @if (App::isLocale('en'))
                              {{ $family->en_name }}
                            @else
                              {{ $family->ar_name }}
                            @endif
                          </p>
                          <p>
                            @if (App::isLocale('en'))
                              {{ $family->cate_en_name }}
                            @else
                              {{ $family->cate_ar_name }}
                            @endif
                          </p>
                        </div>
                      </a>
                      <hr>
                    @endforeach

                </div>

                <?php $setting = setting();?>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <h1 class="f-header">@lang('translate.about')</h1>
                    <div class="animate-box left-in">
                        <p class="din-font">
                          @if (App::isLocale('en'))
                            {{ $setting->en_about }}
                          @else
                            {{ $setting->ar_about }}
                          @endif
                        </p>
                    </div>

                    <h1 class="f-header">@lang('translate.follow_us')</h1>
                    <ul class="list-unstyled list-inline social">
                        <li class="animate-box left-in"><a href="{{ $setting->facebook }}" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="icofont icofont-social-facebook"></i></a></li>
                        <li class="animate-box left-in"><a href="{{ $setting->twitter }}" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="icofont icofont-social-twitter"></i></a></li>
                        <li class="animate-box left-in"><a href="{{ $setting->google }}" data-toggle="tooltip" data-placement="bottom" title="Google+"><i class="icofont icofont-social-google-plus"></i></a></li>
                        <li class="animate-box left-in"><a href="{{ $setting->instgram }}" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i class="icofont icofont-social-instagram"></i></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

    <div class="fixed-burger">
        <a href="#top" class="smooth-a"><img src="{{asset('public/site/images/burger.png')}}"></a>
    </div>

    <div class="f-burger">
        <img src="{{asset('public/site/images/footer-burger.png')}}" class="animate-box right-in">
    </div>

</footer>



<!--===============================
    SMALL FOOTER
===================================-->

<div class="small-footer">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-6 text-align-right">
                <p class="din-font">@lang('translate.footer_1')<a href="{{ asset('/') }}" class="text-red">@lang('translate.title')</a> 2017</p>
            </div>

            <div class="col-xs-12 col-sm-6 text-align-left">
                <p class="din-font">@lang('translate.footer_2')<a href="#" class="text-red">@lang('translate.company')</a> 2017</p>
            </div>

        </div>
    </div>
</div>


<!--===============================
    SCRIPT
===================================-->

<script src="{{asset('public/site/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/site/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('public/site/js/parallax.min.js')}}"></script>
<script src="{{asset('public/site/js/jquery.fancybox.min.js')}}"></script>
<script src="{{asset('public/site/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('public/site/js/script.js')}}"></script>
</body>
</html>
