@extends('layout')
  @section('location_header')
    @include('location_header')
  @endsection
  @section('content')
    <!--===============================
        OFFERS SECTION
    ===================================-->

    <div class="div-padding">
        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-sm-6 col-sm-pull-3 text-center main-header">
                    <h1 class="text-red animate-box">@lang('translate.rest_Offers')</h1>
                </div>

                <div class="col-xs-6 col-sm-3 col-sm-push-6 animate-box right-in">
                </div>

                <div class="col-xs-6 col-sm-3 text-left">
                    <button class="owl-btn btn-white text-red btn-border offers-first-prev"><i class="icofont icofont-arrow-right"></i></button>
                    <button class="owl-btn btn-white text-red btn-border offers-first-next"><i class="icofont icofont-arrow-left"></i></button>
                </div>

            </div>


            <div class="animate-box">
                <div class="owl-carousel owl-offers m-t-25" id="first">
                  <?php
                  $categories = category();
                  $offers = offer();
                  $productive_families = productive_family();

                  $DateBegin = date('Y-m-d');
                  $DateEnd = date('Y-m-d', strtotime("-5 days"));
                  ?>
                  @foreach ($offers as $key => $offer)
                    <div class="item">
                      <div class="panel offers-panel text-center">
                        <div class="panel-body offer-bg">
                          @if ($offer->create_at > $DateBegin && $offer->create_at < $DateEnd)
                            <div class="discount din-font"><span>جديد</span></div>
                          @endif
                          <img src="{{asset('public/uploads/'.$offer->photo)}}">
                        </div>

                        <div class="panel-body p-5">
                          <a href="{{ asset('brand/p/category/'.$offer->brand_id) }}" class="btn btn-white text-red btn-border basket-btn" title="اضافة للعربة">
                            <i class="icofont icofont-basket"></i>
                          </a>
                          <h1>
                            @if (App::isLocale('en'))
                              {{ $offer->en_name }}
                            @else
                              {{ $offer->ar_name }}
                            @endif
                          </h1>
                          <p class="text-muted din-font">
                            @if (App::isLocale('en'))
                              {{ $offer->en_describe }}
                            @else
                              {{ $offer->ar_describe }}
                            @endif
                          </p>
                        </div>

                        <div class="p-5 clearfix text-center din-font">
                          <div class="col-xs-6 p-0 border-left">
                            <div class="btn btn-block">{{ $offer->price }}</div>
                          </div>
                          <div class="col-xs-6 p-0">
                            <a href="{{ asset('brand/p/category/'.$offer->brand_id) }}" class="btn btn-block text-black" title="اطلب الأن">اطلب الأن</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  @endforeach


                </div>
            </div>

        </div>
    </div>



    <!--===============================
        ADS SECTION
    ===================================-->

    <div class="div-padding ads-div parallax-window" data-parallax="scroll" data-image-src="{{asset('public/site/images/ads-bg.png')}}">
        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-sm-6 col-sm-pull-3 text-center main-header">
                    <h1 class="text-red animate-box">@lang('translate.rest_ads')</h1>
                </div>

                <div class="col-xs-6 col-sm-3 col-sm-push-6"></div>

                <div class="col-xs-6 col-sm-3 text-left">
                    <button class="owl-btn btn-white bg-transparent text-red btn-border ads-prev"><i class="icofont icofont-arrow-right"></i></button>
                    <button class="owl-btn btn-white bg-transparent text-red btn-border ads-next"><i class="icofont icofont-arrow-left"></i></button>
                </div>

            </div>



            <div class="animate-box">
                <div class="owl-carousel owl-ads m-t-25">
                  @for ($i=0; $i < sizeof($ads)-1 ; $i+=2)
                    <div class="item">
                      <div class="panel offers-panel">
                        <div class="media">
                          <div class="media-left">
                            <div class="offer-bg">
                              <img src="{{asset('public/uploads/'.$ads[$i]['photo'])}}">
                            </div>
                          </div>
                          <div class="media-body p-5">
                            <h1 class="text-red">
                              @if (App::isLocale('en'))
                                {{ $ads[$i]['en_title'] }}
                              @else
                                {{ $ads[$i]['ar_title'] }}
                              @endif
                            </h1>
                            <p class="text-muted din-font">
                            @if (App::isLocale('en'))
                              {{ $ads[$i]['en_describe'] }}
                            @else
                              {{ $ads[$i]['ar_describe'] }}
                            @endif
                            </p>
                            <div class="table-view din-font">
                              <div class="table-cell text-left">
                                <a href="{{asset('brand/show/'.$ads[$i]['brand_id'])}}" class="btn btn-border border-red text-red" >@lang('translate.more')</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="panel offers-panel">
                        <div class="media">
                          <div class="media-left">
                            <div class="offer-bg">
                              <img src="{{asset('public/uploads/'.$ads[$i+1]['photo'])}}">
                            </div>
                          </div>
                          <div class="media-body p-5">
                            <h1 class="text-red">
                              @if (App::isLocale('en'))
                                {{ $ads[$i+1]['en_title'] }}
                              @else
                                {{ $ads[$i+1]['ar_title'] }}
                              @endif
                            </h1>
                            <p class="text-muted din-font">
                            @if (App::isLocale('en'))
                              {{ $ads[$i+1]['en_describe'] }}
                            @else
                              {{ $ads[$i+1]['ar_describe'] }}
                            @endif
                            </p>
                            <div class="table-view din-font">
                              <div class="table-cell text-left">
                                <a href="{{asset('brand/show/'.$ads[$i+1]['brand_id'])}}" class="btn btn-border border-red text-red">@lang('translate.more')</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  @endfor

                </div>
            </div>


        </div>
    </div>



    <!--===============================
        MOST WANTED SECTION
    ===================================-->

    <div class="div-padding">
        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-sm-6 col-sm-pull-3 text-center main-header">
                    <h1 class="text-red animate-box">@lang('translate.rest_popular')</h1>
                </div>

                <div class="col-xs-6 col-sm-3 col-sm-push-6"></div>

                <div class="col-xs-6 col-sm-3 text-left">
                    <button class="owl-btn btn-white text-red btn-border offers-snd-prev"><i class="icofont icofont-arrow-right"></i></button>
                    <button class="owl-btn btn-white text-red btn-border offers-snd-next"><i class="icofont icofont-arrow-left"></i></button>
                </div>

            </div>


            <div class="animate-box">
                <div class="owl-carousel owl-offers m-t-25" id="second">
                  @foreach ($brands as $key => $brand)
                    <div class="item">
                      <div class="panel offers-panel text-center">
                        <div class="panel-body offer-bg">
                          <img src="{{ asset('public/uploads/'.$brand->logo) }}">
                        </div>

                        <div class="panel-body p-5">
                          <a href="{{ asset('/brand/show/'.$brand->id) }}" class="btn btn-white text-red btn-border basket-btn" title="اضافة للعربة">
                            <i class="icofont icofont-heart"></i>
                          </a>
                          <h1>
                            @if (App::isLocale('en'))
                              {{ $brand->en_name }}
                            @else
                              {{ $brand->ar_name }}
                            @endif
                          </h1>
                          <p class="text-muted din-font">
                            @if (App::isLocale('en'))
                              {{ $brand->category_en_name }}
                            @else
                              {{ $brand->category_ar_name }}
                            @endif
                          </p>
                        </div>

                        <div class="p-5 clearfix text-center din-font">
                          <div class="col-xs-6 p-0 border-left">
                            <div class="btn btn-block">
                              <?php
                              $feedback_food=rate('food', $brand->id);
                              $feedback_service=rate('services', $brand->id);
                              $feedback_delivery=rate('delivery', $brand->id);
                              if ($feedback_food==0&&$feedback_service==0&&$feedback_delivery==0) {
                                $total_average = 0;
                              }
                              else {
                                $total_average = ($feedback_food+$feedback_service+$feedback_delivery)/3;
                              }
                              ?>
                              @for ($i=1; $i <= $total_average ; $i++)
                                <i class="icofont icofont-star text-yellow"></i>
                              @endfor
                              @for ($i=5; $i > $total_average ; $i--)
                                <i class="icofont icofont-star text-muted"></i>
                              @endfor
                            </div>
                          </div>
                          <div class="col-xs-6 p-0">
                            <a href="{{ asset('/brand/show/'.$brand->id) }}" class="btn btn-block text-black" >@lang('translate.more')</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  @endforeach
                </div>
            </div>

        </div>
    </div>



    <!--===============================
        TESTIMONIAL SECTION
    ===================================-->

    <div class="div-padding bg-gray p-b-80">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-md-6">

                    <div class="clearfix">
                        <div class="col-xs-12 text-center main-header">
                            <h1 class="text-red animate-box">@lang('translate.cutomers')</h1>
                        </div>
                    </div>

                    <div class="animate-box m-t-50">
                        <div class="panel panel-br0">
                            <div class="panel-body">

                                <div class="owl-carousel owl-single up-70 text-center">
                                  @foreach ($feedbacks as $key => $feedback)
                                    <div class="item">
                                      <img src="{{asset('public/site/images/user-pic.jpg')}}" alt="username" class="user-pic">
                                      <h4>{{ $feedback->name }}</h4>
                                      <p class="text-muted din-font">{{ $feedback->content }}</p>
                                    </div>
                                  @endforeach

                                </div>


                                <div class="text-align-left">
                                  @if (Auth::check())
                                    <a data-toggle="modal" data-target="#testModal" class="btn btn-gray btn-border" title="اضف رأيك">اضف رأيك</a>
                                  @else
                                    <a href="{{ asset('/login') }}" class="btn btn-gray btn-border" title="اضف رأيك">اضف رأيك</a>
                                  @endif
                                </div>

                            </div>
                        </div>
                    </div>

                </div>


                <div class="col-xs-12 col-md-6">

                    <div class="clearfix">
                        <div class="col-xs-12 text-center main-header">
                            <h1 class="text-red animate-box">@lang('translate.about')</h1>
                        </div>
                    </div>
                    <?php $setting = setting();?>

                    <div class="animate-box left-in m-t-25">
                        <h1 class="f-s-20"><img src="{{asset('public/site/images/circle.png')}}"class="right-fa"> ماهو موقع اسرع طعام ؟</h1>
                        <p class="din-font">
                          @if (App::isLocale('en'))
                            {{ $setting->en_what }}
                          @else
                            {{ $setting->ar_what }}
                          @endif
                        </p>
                        <hr>
                        <h1 class="f-s-20"><img src="{{asset('public/site/images/circle.png')}}" class="right-fa"> لماذا اسرع طعام ؟</h1>
                        <p class="din-font">
                          @if (App::isLocale('en'))
                            {{ $setting->en_why }}
                          @else
                            {{ $setting->ar_why }}
                          @endif
                        </p>
                    </div>

                </div>

            </div>
        </div>
    </div>



    <!--===============================
        TESTIMONIAL MODAL
    ===================================-->

    <div class="modal fade" id="testModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="panel-body">
                        <h4 class="text-red">@lang('translate.add_site_feedback')</h4>
                        <form action="{{ asset('site/feedback') }}" class="boxed-form" method="post">
                          {{ csrf_field() }}
                            <textarea placeholder="اكتب رأيك عن الموقع هنا" name="content"></textarea>
                            <div class="text-left"><button type="submit" class="btn btn-red">ارسال</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

  @endsection
