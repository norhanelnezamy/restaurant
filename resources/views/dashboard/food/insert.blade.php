
@extends('dashboard.layout')
@section('content')
  <div class="container div-padding">
    <div class="main-line-header text-center">
      <h1><span>@lang('translate.details_')</span></h1>
    </div>

    <form action="{{ asset('dashboard/b/food') }}" method="post" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="col-xs-12 div-padding">
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <div class="clearfix">

              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.select_category')</label>
              </div>

              <div class="col-xs-12 col-sm-9 p-0">
                <select name="menu_id">
                  @if (App::isLocale('en'))
                    @foreach ($menus as $key => $menu)
                      <option value="{{ $menu->id }}">{{ $menu->en_name }}</option>
                    @endforeach
                  @else
                    @foreach ($menus as $key => $menu)
                      <option value="{{ $menu->id }}">{{ $menu->ar_name }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
            </div>
            @if ($errors->has('menu_id'))
              <p style="color:red">{{ $errors->first('menu_id') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.image_upload')</label>
              </div>
              <div class="col-xs-10 col-sm-7 p-0">
                <input name="photo" type="file" placeholder="">
              </div>
              <div class="col-xs-2 p-0 text-center">
                <i class="icofont icofont-image font-i-50"></i>
              </div>
            </div>
            @if ($errors->has('photo'))
              <p style="color:red">{{ $errors->first('photo') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.Ar_name')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="ar_food_name" type="text" placeholder="" value="{{ old('ar_food_name') }}">
              </div>
            </div>
            @if ($errors->has('ar_food_name'))
              <p style="color:red">{{ $errors->first('ar_food_name') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.En_name')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="en_food_name" type="text" placeholder="" value="{{ old('en_food_name') }}">
              </div>
            </div>
            @if ($errors->has('en_food_name'))
              <p style="color:red">{{ $errors->first('en_food_name') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.ar_describe')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="ar_food_describe" type="text" placeholder="" value="{{ old('ar_food_describe') }}">
              </div>
            </div>
            @if ($errors->has('ar_food_describe'))
              <p style="color:red">{{ $errors->first('ar_food_describe') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.en_describe')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="en_food_describe" type="text" placeholder="" value="{{ old('en_food_describe') }}">
              </div>
            </div>
            @if ($errors->has('en_food_describe'))
              <p style="color:red">{{ $errors->first('en_food_describe') }}</p>
            @endif
          </div>

        </div>

      </div>

      <div class="div-padding bg-white">

        <div class="clearfix">
          <div class="main-line-header text-center">
            <h1><span>@lang('translate.meal_size') </span></h1>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-3">
            <label class="checkbox-holder choose-size">
              <input type="checkbox" name="one_size" value="one_size" @if(old('one_size')) checked @endif>
                <span class="checkbox-icon"><i class="icofont icofont-check"></i></span>
                <span>@lang('translate.one_size')</span>
              </label>
              <input name="one_size_price" type="number" placeholder="السعر" value="{{ old('one_size_price') }}" >
              @if ($errors->has('one_size_price'))
                <p style="color:red">{{ $errors->first('one_size_price') }}</p>
              @endif
              @if ($errors->has('one_size'))
                <p style="color:red">{{ $errors->first('one_size') }}</p>
              @endif
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
              <label class="checkbox-holder choose-size">
                <input type="checkbox" name="large" value="large" @if(old('large')) checked @endif>
                  <span class="checkbox-icon"><i class="icofont icofont-check"></i></span>
                  <span>@lang('translate.large')</span>
                </label>
                <input name="large_price" type="number" placeholder="السعر" value="{{ old('large_price') }}">
                @if ($errors->has('large'))
                  <p style="color:red">{{ $errors->first('large') }}</p>
                @endif
                @if ($errors->has('large_price'))
                  <p style="color:red">{{ $errors->first('large_price') }}</p>
                @endif
              </div>

              <div class="col-xs-12 col-sm-6 col-md-3">
                <label class="checkbox-holder choose-size">
                  <input type="checkbox" name="medium" value="medium" @if(old('medium')) checked @endif>
                    <span class="checkbox-icon"><i class="icofont icofont-check"></i></span>
                    <span>@lang('translate.medium')</span>
                  </label>
                  <input name="medium_price" type="number" placeholder="السعر" value="{{ old('medium_price') }}">
                  @if ($errors->has('medium_price'))
                    <p style="color:red">{{ $errors->first('medium_price') }}</p>
                  @endif
                  @if ($errors->has('medium'))
                    <p style="color:red">{{ $errors->first('medium') }}</p>
                  @endif
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3">
                  <label class="checkbox-holder choose-size">
                    <input type="checkbox" name="small" value="small" @if(old('small')) checked @endif>
                      <span class="checkbox-icon"><i class="icofont icofont-check"></i></span>
                      <span>@lang('translate.small')</span>
                    </label>
                    <input name="small_price" type="number" placeholder="السعر" value="{{ old('small_price') }}">
                    @if ($errors->has('small_price'))
                      <p style="color:red">{{ $errors->first('small_price') }}</p>
                    @endif
                    @if ($errors->has('small'))
                      <p style="color:red">{{ $errors->first('small') }}</p>
                    @endif
                  </div>

                </div>

              </div>


              <div class="div-padding">

                {{-- <div class="hidden new-form-data">
                  <div class="row div-to-duplicate">
                    <div class="col-xs-9 col-sm-10 col-md-11 p-0">

                      <div class="col-xs-12 col-md-6">
                        <div class="clearfix">
                          <div class="col-xs-12 col-sm-3 p-0">
                            <label>@lang('translate.Ar_name')</label>
                          </div>
                          <div class="col-xs-12 col-sm-9 p-0">
                            <input name="ar_additional_name[]" type="text" placeholder="">
                          </div>
                        </div>
                      </div>

                      <div class="col-xs-12 col-md-6">
                        <div class="clearfix">
                          <div class="col-xs-12 col-sm-3 p-0">
                            <label>@lang('translate.En_name')</label>
                          </div>
                          <div class="col-xs-12 col-sm-9 p-0">
                            <input name="en_additional_name[]" type="text" placeholder="">
                          </div>
                        </div>
                      </div>

                      <div class="col-xs-12 col-md-6">
                        <div class="clearfix">
                          <div class="col-xs-12 col-sm-3 p-0">
                            <label>@lang('translate.select_type') </label>
                          </div>
                          <div class="col-xs-12 col-sm-9 p-0">
                            <select name="additionsl_type[]">
                              <option selected disabled>@lang('translate.select_type')</option>
                              <option value="drink">@lang('translate.drink')</option>
                              <option value="appetizer">@lang('translate.appetizer')</option>
                              <option value="salad">@lang('translate.salad')</option>
                              <option value="other">@lang('translate.other')</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="col-xs-12 col-md-6">
                        <div class="clearfix">
                          <div class="col-xs-12 col-sm-3 p-0">
                            <label>@lang('translate.price_')</label>
                          </div>
                          <div class="col-xs-12 col-sm-9 p-0">
                            <input name="additional_price[]" type="text" placeholder="">
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="col-xs-3 col-sm-2 col-md-1">
                      <a class="remove-parent btn btn-red btn-block" title="حذف"><i class="icofont icofont-trash"></i></a>
                    </div>
                  </div>
                </div>  --}}

                {{-- <div class="new-rows">
                </div> --}}

                <div class="text-center m-20">
                  {{-- <a class="duplicate-btn btn btn-red">@lang('translate.additions_meal')</a> --}}
                  <button type="submit" class="btn btn-red">@lang('translate.save_')</button>
                </div>

              </div>

            </form>

          </div>

@endsection
