@extends('dashboard.layout')
  @section('content')
      <div class="container container-sm div-padding">
          <form action="{{ asset('dashboard/b/photo') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
              <div class="text-center m-20">
                  <div class="images-upload-block">
                      <label class="upload-img">
                          <input type="file" class="image-uploader" accept="image/*" name="photo" required>
                          <i class="icofont icofont-arrow-up text-red"></i>
                      </label>
                  </div>
                  <h4><strong>@lang('translate.add_image')</strong></h4>
              </div>
              <div class="text-center">
                  <button type="submit" class="btn btn-red">@lang('translate.save')</button>
              </div>
          </form>
      </div>

  @endsection
