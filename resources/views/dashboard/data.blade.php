@extends('dashboard.layout')
  @section('content')

            <div class="container container-sm div-padding">
                <form action="{{ asset('dashboard/brand/'.$brand->id) }}" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}
                    <div class="panel">
                        <div class="panel-body">
                            <div class="main-line-header text-center">
                                <h1><span>@lang('translate.about_')</span></h1>
                            </div>
                            <label>عربى</label>
                            <input type="text" placeholder="" class="gray-input" value="{{ old('ar_name', $brand->ar_name) }}" name="ar_name">
                            <label>انجليزى</label>
                            <input type="text" placeholder="" class="gray-input" value="{{ old('en_name', $brand->en_name) }}" name="en_name">


                            <div class="clearfix">

                                <div class="images-upload-block add-next">
                                    <label class="upload-img">
                                        <input type="file" class="image-uploader" accept="image/*" name="menu_photos[]">
                                        <img src="{{asset('public/dashboard/images/upload-plate.png')}}">
                                        @if (!empty($brand->menus_photos))
                                          <?php $photos = json_decode($brand->menus_photos, true);?>
                                          @foreach ($photos as $key => $photo)
                                            {{-- <img src="{{asset('uploads/'.$photo)}}"> --}}
                                          @endforeach
                                        @endif
                                    </label>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-4"><label>@lang('translate.min_charge')</label></div>
                        <div class="col-xs-12 col-sm-8"><input type="text" placeholder="" value="{{ old('min_charge', $brand->min_charge) }}" name="min_charge"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4"><label>@lang('translate.work_time')</label></div>
                        <div class="col-xs-12 col-sm-8"><input type="text" placeholder="" value="{{ old('work_time', $brand->work_time) }}" name="work_time"></div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-xs-12 col-sm-4"><label>انواع المطابخ</label></div>
                        <div class="col-xs-12 col-sm-8"><input type="text" placeholder="" value="{{ $brand->category }}"></div>
                    </div> --}}
                    @if (Auth::guard('web')->user()->role == 'productive_family')
                      <div class="row">
                          <div class="col-xs-12 col-sm-4"><label>@lang('translate.address')</label></div>
                          <div class="col-xs-12 col-sm-8"><input type="text" placeholder="" value="{{ old('address', $brand->address) }}" name="address"></div>
                      </div>
                    @endif
                    <div class="text-center m-20">
                        <div class="images-upload-block">
                            <label class="upload-img">
                                <input type="file" class="image-uploader" accept="image/*" name="logo">
                                <i class="icofont icofont-arrow-up text-red"></i>
                            </label>
                        </div>

                        <h4><strong>@lang('translate.add_logo')</strong></h4>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-red">@lang('translate.save')</button>
                    </div>

                </form>

            </div>
  @endsection
