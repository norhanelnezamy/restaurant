@extends('dashboard.layout')
  @section('content')
    <div class="container div-padding">

        <div class="main-line-header text-center">
            <h1><span>@lang('translate.chat')</span></h1>
        </div>

        <h3>@lang('translate.send_msg')</h3>

        <form method="post" action="{{ asset('/dashboard/b/message/order/'.$id) }}" class="boxed-form">
          {{ csrf_field() }}
            <textarea name="content" placeholder="@lang('translate.write_message')"></textarea>
            <div class="text-left">
                <button class="btn btn-red">@lang('translate.send')</button>
            </div>
        </form>

    </div>
  @endsection
