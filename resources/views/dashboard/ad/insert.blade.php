
@extends('dashboard.layout')
@section('content')
  <div class="container div-padding">
    <div class="main-line-header text-center">
      <h1><span>@lang('translate.ad')</span></h1>
    </div>

    <form action="{{ asset('dashboard/ad') }}" method="post" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="col-xs-12 div-padding">
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.ar_title')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="ar_title" type="text" placeholder="" value="{{ old('ar_title') }}">
              </div>
            </div>
            @if ($errors->has('ar_title'))
              <p style="color:red">{{ $errors->first('ar_title') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.en_title')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="en_title" type="text" placeholder="" value="{{ old('en_title') }}">
              </div>
            </div>
            @if ($errors->has('en_title'))
              <p style="color:red">{{ $errors->first('en_title') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.ar_describe')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="ar_describe" type="text" placeholder="" value="{{ old('ar_describe') }}">
              </div>
            </div>
            @if ($errors->has('ar_describe'))
              <p style="color:red">{{ $errors->first('ar_describe') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.en_describe')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="en_describe" type="text" placeholder="" value="{{ old('en_describe') }}">
              </div>
            </div>
            @if ($errors->has('en_describe'))
              <p style="color:red">{{ $errors->first('en_describe') }}</p>
            @endif
          </div>
        </div>
      </div>

      <div class="text-center m-20">
          <div class="images-upload-block">
              <label class="upload-img">
                  <input type="file" class="image-uploader" accept="image/*" name="photo">
                  <i class="icofont icofont-arrow-up text-red"></i>
              </label>
          </div>
          <h4><strong>@lang('translate.image_upload')</strong></h4>
          @if ($errors->has('photo'))
            <p style="color:red">{{ $errors->first('photo') }}</p>
          @endif
      </div>

      <div class="div-padding">
        <div class="text-center m-20">
          <button type="submit" class="btn btn-red">@lang('translate.save_')</button>
        </div>
      </div>

    </form>

  </div>

@endsection
