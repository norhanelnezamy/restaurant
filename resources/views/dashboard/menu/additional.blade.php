@extends('dashboard.layout')
  @section('content')
    <div class="container div-padding">
        <form action="{{ asset('dashboard/b/additional') }}" method="post">
            {{ csrf_field() }}
            <div class="main-line-header text-center">
                <h1><span>@lang('translate.add_category')</span></h1>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="clearfix">
                        <div class="col-xs-12 col-sm-3 p-0">
                            <label>@lang('translate.Ar_name')</label>
                        </div>
                        <div class="col-xs-12 col-sm-9 p-0">
                            <input type="text" placeholder="" name="ar_name" value="{{ old('ar_name') }}" >
                            @if ($errors->has('ar_name'))
                            <p style="color:red">{{ $errors->first('ar_name') }}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="clearfix">
                        <div class="col-xs-12 col-sm-3 p-0">
                          <label>@lang('translate.En_name')</label>
                        </div>
                        <div class="col-xs-12 col-sm-9 p-0">
                            <input type="text" placeholder="" name="en_name" value="{{ old('en_name') }}">
                            @if ($errors->has('en_name'))
                              <p style="color:red">{{ $errors->first('en_name') }}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="clearfix">
                        <div class="col-xs-12 col-sm-3 p-0">
                            <label>@lang('translate.type_')</label>
                        </div>
                        <div class="col-xs-12 col-sm-9 p-0">
                          <select title="النوع " id="type" value="{{ old('type') }}" name="type">
                            <option disabled selected>@lang('translate.select_type')</option>
                            <option value="salad">@lang('translate.salad')</option>
                            <option value="drink">@lang('translate.drink')</option>
                            <option value="appetizer">@lang('translate.appetizer')</option>
                            <option value="other">@lang('translate.other')</option>
                          </select>
                            @if ($errors->has('type'))
                              <p style="color:red">{{ $errors->first('type') }}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="clearfix">
                        <div class="col-xs-12 col-sm-3 p-0">
                            <label>@lang('translate.price_')</label>
                        </div>
                        <div class="col-xs-12 col-sm-9 p-0">
                            <input type="text" placeholder="" name="price" value="{{ old('price') }}">
                            @if ($errors->has('price'))
                              <p style="color:red">{{ $errors->first('price') }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center m-20">
                <button type="submit" class="btn btn-red">@lang('translate.save_')</button>
            </div>
        </form>
    </div>

    <div class="div-padding bg-white">
        <div class="container">

            <div class="main-line-header text-center">
                <h1><span>@lang('translate.additional_menu')</span></h1>
            </div>
            @foreach ($additionals as $key => $additional)
              <div class="panel clearfix">
                <div class="col-xs-2 col-md-1 p-0 border-bottom-sm">
                  <span class="p-8 bg-gray">{{ $additional->id }}</span>
                </div>
                <div class="col-xs-10 col-md-3 p-8 border-bottom-sm">{{ $additional->ar_name }}</div>
                <div class="col-xs-12 col-sm-4 col-md-3 p-8 border-bottom-xs">{{ $additional->en_name }}</div>
                <div class="col-xs-12 col-sm-4 col-md-2 p-8 border-bottom-xs">{{ $additional->price }}</div>
                <div class="col-xs-12 col-sm-4 col-md-3 p-0">
                  <div class="table-view text-center bordered-btns">
                    <div class="table-cell"><button class="p-8 btn-block">@lang('translate.edit_')</button></div>
                    <div class="table-cell"><button class="p-8 btn-block text-red">  @lang('translate.delet_')</button></div>
                  </div>
                </div>
              </div>
            @endforeach
            <ul class="pagination">
                {{ $additionals->links() }}
            </ul>

        </div>
    </div>
  @endsection
