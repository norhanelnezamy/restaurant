@extends('dashboard.layout')
  @section('content')
    <div class="container div-padding">

      <div class="panel">
        <div class="clearfix panel-body">
          <div class="col-xs-4 col-md-3 p-0">#</div>
          <div class="col-xs-4 col-md-3 p-0">@lang('translate.image')</div>

          <div class="col-xs-4 col-md-3 p-0">@lang('translate.name')</div>

        </div>

        @foreach ($foods as $key => $food)

          <hr class="m-0">
          <div class="clearfix panel-body">
            <div class="col-xs-4 col-md-3 p-0">{{ $food->id }}</div>
            <div class="col-xs-4 col-md-3 p-0"><img src="{{ asset('uploads/'.$food->photo) }}" alt="" style="width:70px;height:70px;"></div>
            <div class="col-xs-4 col-md-3 p-0">
              @if (App::isLocale('en'))
                {{ $food->en_name }}
              @else
                {{ $food->ar_name }}
              @endif
            </div>

            <div class="col-xs-12 col-md-4 col-lg-3 p-0">
              <div class="table-view">
                <button data-toggle="modal" data-target="{{ '#viewModal_'.$food->id }}" class="btn btn-default table-cell block">مشاهدة</button>
                <form action="{{ asset('dashboard/b/food/'.$food->id.'/edit') }}" method="get">
                  <button type="submit" class="btn btn-default table-cell block">تعديل</button>
                </form>
                <form action="{{ asset('dashboard/b/food/'.$food->id) }}" method="post">
                  {{ csrf_field() }}
                  {{ method_field("DELETE") }}
                  <button type="submit" class="btn btn-default table-cell block">مسح</button>
                </form>
              </div>
            </div>

          </div>

          <div class="modal fade" id="{{ 'viewModal_'.$food->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-body">

                  <div class="image-block">
                    <img src="{{ asset('uploads/'.$food->photo) }}">
                  </div>

                  <div class="row">

                    <div class="col-xs-12 col-sm-6 col-md-4">
                      <div class="clearfix">

                        <div class="col-xs-12 col-sm-5 p-0">
                          <label>الإسم</label>
                        </div>

                        <div class="col-xs-12 col-sm-7 p-0">
                          @if (App::isLocale('en'))
                            <input type="text" value="{{ $food->en_name }}" disabled class="input-b-bottom">
                          @else
                            <input type="text" value="{{ $food->ar_name }}" disabled class="input-b-bottom">
                          @endif
                        </div>

                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                      <div class="clearfix">

                        <div class="col-xs-12 col-sm-5 p-0">
                          <label>الفئة</label>
                        </div>

                        <div class="col-xs-12 col-sm-7 p-0">
                          <div class="col-xs-12 col-sm-7 p-0">
                            @if (App::isLocale('en'))
                              <input type="text" value="{{ $menu->en_name }}" disabled class="input-b-bottom">
                            @else
                              <input type="text" value="{{ $menu->ar_name }}" disabled class="input-b-bottom">
                            @endif
                          </div>

                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="clearfix">

                          <div class="col-xs-12 col-sm-5 p-0">
                            <label>الوصف</label>
                          </div>

                          <div class="col-xs-12 col-sm-7 p-0">
                            <div class="col-xs-12 col-sm-7 p-0">
                              @if (App::isLocale('en'))
                                <input type="text" value="{{ $food->en_additional }}" disabled class="input-b-bottom">
                              @else
                                <input type="text" value="{{ $food->ar_additional }}" disabled class="input-b-bottom">
                              @endif
                            </div>

                          </div>
                        </div>
                      </div>

                      <div class="main-line-header text-center">
                        <h1><span>الأحجام المتاحة</span></h1>
                      </div>
                      <?php $size = json_decode($food->size, true);?>
                      <div class="table-view text-center">
                        @foreach ($size as $key => $one)
                          <div class="table-cell"><span class="btn btn-default">@lang('translate.'.$key){{' = '. $one }}</span></div>
                        @endforeach
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach

      </div>

    </div>
  @endsection
