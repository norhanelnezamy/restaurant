@extends('dashboard.layout')
  @section('content')
    <div class="container div-padding">
        <form action="{{ asset('dashboard/b/menu') }}" method="post">
            {{ csrf_field() }}
            <div class="main-line-header text-center">
              <h1><span>@lang('translate.add_category')</span></h1>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="clearfix">
                        <div class="col-xs-12 col-sm-3 p-0">
                          <label>@lang('translate.Ar_name')</label>
                        </div>
                        <div class="col-xs-12 col-sm-9 p-0">
                            <input type="text" placeholder="" name="ar_name" value="{{ old('ar_name') }}" >
                            @if ($errors->has('ar_name'))
                            <p style="color:red">{{ $errors->first('ar_name') }}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="clearfix">
                        <div class="col-xs-12 col-sm-3 p-0">
                            <label>@lang('translate.En_name')</label>
                        </div>
                        <div class="col-xs-12 col-sm-9 p-0">
                            <input type="text" placeholder="" name="en_name" value="{{ old('en_name') }}">
                            @if ($errors->has('en_name'))
                            <p style="color:red">{{ $errors->first('en_name') }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center m-20">
                <button type="submit" class="btn btn-red">@lang('translate.save_')</button>
            </div>
        </form>
    </div>

    <div class="div-padding bg-white">
        <div class="container">

            <div class="main-line-header text-center">
                <h1><span>@lang('translate.food_menu')</span></h1>
            </div>
                @foreach ($menus as $key => $menu)
                  <div class="panel clearfix">
                    <div class="col-xs-2 col-md-1 p-0 border-bottom-sm">
                      <span class="p-8 bg-gray">{{ $menu->id }}</span>
                    </div>
                    <div class="col-xs-10 col-md-3 p-8 border-bottom-sm">{{ $menu->ar_name }}</div>
                    <div class="col-xs-12 col-sm-8 col-md-5 p-8 border-bottom-xs">{{ $menu->en_name }}</div>
                    <div class="col-xs-12 col-sm-4 col-md-3 p-0">
                      <div class="table-view text-center bordered-btns">
                        <div class="table-cell"><a href="{{ asset('dashboard/b/menu/'.$menu->id) }}" class="p-8 btn-block">@lang('translate.browse_')</a></div>
                        <div class="table-cell"><a data-toggle="modal" data-target="{{'#editModal_'.$menu->id}}" class="p-8 btn-block">@lang('translate.edit_')</a></div>
                        <div class="table-cell"><button class="p-8 btn-block text-red">@lang('translate.delet_')</button></div>
                      </div>
                    </div>
                  </div>

                  <div class="modal fade" id="{{'editModal_'.$menu->id}}" tabindex="-1" role="dialog">
                      <div class="modal-dialog" role="document">
                          <div class="modal-content">
                              <div class="panel-body">

                                  <form action="{{ asset('dashboard/b/menu/'.$menu->id) }}" method="post">
                                      <div class="row">

                                          <div class="col-xs-12 col-sm-6">
                                            <label>@lang('translate.Ar_name')</label>
                                            <input type="text" placeholder="" name="ar_name" value="{{ old('ar_name', $menu->ar_name) }}" >
                                            @if ($errors->has('ar_name'))
                                            <p style="color:red">{{ $errors->first('ar_name') }}</p>
                                            @endif
                                          </div>

                                          <div class="col-xs-12 col-sm-6">
                                            <label>@lang('translate.En_name')</label>
                                            <input type="text" placeholder="" name="en_name" value="{{ old('en_name', $menu->en_name) }}" >
                                            @if ($errors->has('en_name'))
                                            <p style="color:red">{{ $errors->first('en_name') }}</p>
                                            @endif
                                          </div>

                                      </div>

                                      <div class="text-center">
                                          <button type="submit" class="btn btn-red">@lang('translate.update')</button>
                                      </div>
                                  </form>

                              </div>
                          </div>
                      </div>
                  </div>
                @endforeach

            <ul class="pagination">
                {{ $menus->links() }}
            </ul>

        </div>
    </div>
  @endsection
