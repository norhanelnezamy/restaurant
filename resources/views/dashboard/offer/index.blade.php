@extends('dashboard.layout')
  @section('content')
    <div class="container div-padding">

      <div class="panel">
        <div class="clearfix panel-body">
          <div class="col-xs-4 col-md-3 p-0">#</div>
          <div class="col-xs-4 col-md-3 p-0">@lang('translate.image')</div>

          <div class="col-xs-4 col-md-3 p-0">@lang('translate.name')</div>

        </div>

        @foreach ($offers as $key => $offer)

          <hr class="m-0">
          <div class="clearfix panel-body">
            <div class="col-xs-4 col-md-3 p-0">{{ $offer->id }}</div>
            <div class="col-xs-4 col-md-3 p-0"><img src="{{ asset('uploads/'.$offer->photo) }}" alt="" style="width:70px;height:70px;"></div>
            <div class="col-xs-4 col-md-3 p-0">
              @if (App::isLocale('en'))
                {{ $offer->en_name }}
              @else
                {{ $offer->ar_name }}
              @endif
            </div>

            <div class="col-xs-12 col-md-4 col-lg-3 p-0">
              <div class="table-view">
                <button data-toggle="modal" data-target="#viewModal_{{ $offer->id }}" class="btn btn-default table-cell block">مشاهدة</button>
                <form action="{{ asset('dashboard/b/offer/'.$offer->id.'/edit') }}" method="get">
                  <button type="submit" class="btn btn-default table-cell block">@lang('translate.update')</button>
                </form>
                <form action="{{ asset('dashboard/b/offer/'.$offer->id) }}" method="post">
                  {{ csrf_field() }}
                  {{ method_field("DELETE") }}
                  <button type="submit" class="btn btn-default table-cell block">@lang('translate.delete')</button>
                </form>
              </div>
            </div>

          </div>

          <div class="modal fade" id="#viewModal_{{ $offer->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-body">

                  <div class="image-block">
                    <img src="{{ asset('uploads/'.$offer->photo) }}">
                  </div>

                  <div class="row">

                    <div class="col-xs-12 col-sm-6 col-md-4">
                      <div class="clearfix">

                        <div class="col-xs-12 col-sm-5 p-0">
                          <label>@lang('translate.name')</label>
                        </div>

                        <div class="col-xs-12 col-sm-7 p-0">
                          @if (App::isLocale('en'))
                            <input type="text" value="{{ $offer->en_name }}" disabled class="input-b-bottom">
                          @else
                            <input type="text" value="{{ $offer->ar_name }}" disabled class="input-b-bottom">
                          @endif
                        </div>

                      </div>
                    </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="clearfix">

                          <div class="col-xs-12 col-sm-5 p-0">
                            <label>@lang('translate.descripe')</label>
                          </div>

                          <div class="col-xs-12 col-sm-7 p-0">
                            <div class="col-xs-12 col-sm-7 p-0">
                              @if (App::isLocale('en'))
                                <input type="text" value="{{ $offer->en_describe }}" disabled class="input-b-bottom">
                              @else
                                <input type="text" value="{{ $offer->ar_describe }}" disabled class="input-b-bottom">
                              @endif
                            </div>

                          </div>
                        </div>

                      </div>
                      <div class="main-line-header text-center">
                        <h1><span>@lang('translate.price') </span></h1>
                      </div>
                      <div class="table-view text-center">
                        <div class="table-cell"><span class="btn btn-default">{{ $offer->price }}SR</span></div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach

      </div>

    </div>
  @endsection
