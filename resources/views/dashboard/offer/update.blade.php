
@extends('dashboard.layout')
@section('content')
  <div class="container div-padding">
    <div class="main-line-header text-center">
      <h1><span>@lang('translate.offer_details')</span></h1>
    </div>

    <form action="{{ asset('dashboard/b/offer/'.$offer->id) }}" method="post" enctype="multipart/form-data">
      {{ csrf_field() }}
      {{ method_field("PUT") }}
      <div class="col-xs-12 div-padding">
        <div class="row">

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.Ar_name')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="ar_food_name" type="text" placeholder="" value="{{ old('ar_food_name' , $offer->ar_name) }}">
              </div>
            </div>
            @if ($errors->has('ar_food_name'))
              <p style="color:red">{{ $errors->first('ar_food_name') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.En_name')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="en_food_name" type="text" placeholder="" value="{{ old('en_food_name' , $offer->en_name) }}">
              </div>
            </div>
            @if ($errors->has('en_food_name'))
              <p style="color:red">{{ $errors->first('en_food_name') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.price_')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="price" type="text" placeholder="" value="{{ old('price' , $offer->price) }}">
              </div>
            </div>
            @if ($errors->has('price'))
              <p style="color:red">{{ $errors->first('price') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.image_upload')</label>
              </div>
              <div class="col-xs-10 col-sm-7 p-0">
                <input name="photo" type="file" placeholder="" accept="image/*" value="{{ old('photo') }}">
              </div>
              <div class="col-xs-2 p-0 text-center">
                <i class="icofont icofont-image font-i-50"></i>
              </div>
            </div>
            @if ($errors->has('photo'))
              <p style="color:red">{{ $errors->first('photo') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.ar_describe')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="ar_food_describe" type="text" placeholder="" value="{{ old('ar_food_describe' , $offer->ar_describe) }}">
              </div>
            </div>
            @if ($errors->has('ar_food_describe'))
              <p style="color:red">{{ $errors->first('ar_food_describe') }}</p>
            @endif
          </div>

          <div class="col-xs-12 col-md-6">
            <div class="clearfix">
              <div class="col-xs-12 col-sm-3 p-0">
                <label>@lang('translate.en_describe')</label>
              </div>
              <div class="col-xs-12 col-sm-9 p-0">
                <input name="en_food_describe" type="text" placeholder="" value="{{ old('en_food_describe' , $offer->en_describe) }}">
              </div>
            </div>
            @if ($errors->has('en_food_describe'))
              <p style="color:red">{{ $errors->first('en_food_describe') }}</p>
            @endif
          </div>

        </div>

      </div>

      <div class="div-padding">
        <div class="text-center m-20">
          <button type="submit" class="btn btn-red">@lang('translate.save_')</button>
        </div>

      </div>

    </form>

  </div>

@endsection
