@extends('dashboard.layout')
  @section('content')
    <div class="container container-sm div-padding two-colored-panels">
      @foreach ($feedbacks as $key => $feedback)
        <div class="panel panel-br0 light-gray din-font">
          <div class="panel-body">
            <div class="table-view">
              <div class="table-cell"><h1 class="f-s-20 m-t-0">{{ $feedback->name }}</h1></div>
              <div class="table-cell text-muted text-left">{{ $feedback->created_at }}</div>
            </div>
            <p class="text-muted">{{ $feedback->content }}</p>
            <div class="row text-align-right">
              <div class="col-xs-12 col-sm-4">
                <span class="label label-default bg-white">@lang('translate.delivery')</span>
                <br>
                @for ($i=1; $i <= $feedback->delivery ; $i++)
                  <i class="icofont icofont-star text-yellow"></i>
                @endfor
                @for ($i=5; $i > $feedback->delivery ; $i--)
                  <i class="icofont icofont-star text-muted"></i>
                @endfor
              </div>
              <div class="col-xs-12 col-sm-4">
                <span class="label label-default bg-white">@lang('translate.services')</span>
                <br>
                @for ($i=1; $i <= $feedback->services ; $i++)
                  <i class="icofont icofont-star text-yellow"></i>
                @endfor
                @for ($i=5; $i > $feedback->services ; $i--)
                  <i class="icofont icofont-star text-muted"></i>
                @endfor
              </div>
              <div class="col-xs-12 col-sm-4">
                <span class="label label-default bg-white">@lang('translate.food_feedback')</span>
                <br>
                @for ($i=1; $i <= $feedback->food ; $i++)
                  <i class="icofont icofont-star text-yellow"></i>
                @endfor
                @for ($i=5; $i > $feedback->food ; $i--)
                  <i class="icofont icofont-star text-muted"></i>
                @endfor
              </div>
            </div>
          </div>
        </div>
      @endforeach

    </div>
  @endsection
