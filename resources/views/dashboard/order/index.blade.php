@extends('dashboard.layout')
  @section('content')
    <div class="container div-padding">
        <div class="panel">
            <div class="clearfix panel-body">
                <div class="col-xs-6 col-md-3 p-0">@lang('translate.name')</div>
                <div class="col-xs-6 col-md-4 p-0">@lang('translate.date')</div>
                <div class="col-xs-6 col-md-5 p-0 hidden-xs">@lang('translate.additions_')</div>
            </div>

            @foreach ($orders as $key => $order)
              <hr class="m-0">
              <div class="clearfix panel-body">
                <div class="col-xs-6 col-md-3 p-0">{{ $order->name }}</div>
                <div class="col-xs-6 col-md-4 p-0">
                  <input type="text" value="{{ $order->created_at }}" class="m-0 max-width" readonly disabled>
                </div>
                <div class="col-xs-12 col-md-5 p-0">
                  <div class="table-view">
                    @if ($order->status == 1)
                      <div class="table-cell block"><a href="{{ asset('/dashboard/b/message/accept/order/'.$order->id) }}" class="btn btn-default">قبول</a></div>
                      <div class="table-cell block"><a href="{{ asset('/dashboard/b/message/refuse/order/'.$order->id) }}" class="btn btn-default">الغاء</a></div>
                    @endif
                    <div class="table-cell block"><a href="{{ asset('/dashboard/b/message/order/'.$order->id) }}" class="btn btn-default">مراسلة</a></div>
                    <div class="table-cell block"><a href="{{ asset('dashboard/b/order/'.$order->id) }}" class="btn btn-default">مشاهدة</a></div>
                  </div>
                </div>
              </div>
            @endforeach

            <ul class="pagination">
                {{ $orders->links() }}
            </ul>

        </div>

    </div>
  @endsection
