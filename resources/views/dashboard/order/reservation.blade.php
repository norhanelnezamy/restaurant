@extends('dashboard.layout')
  @section('content')
    <div class="container div-padding">
        <div class="panel">
            <div class="clearfix panel-body">
                <div class="col-xs-6 col-md-3 p-0">@lang('translate.name_')</div>
                <div class="col-xs-6 col-md-4 p-0">@lang('translate.date')</div>
                <div class="col-xs-6 col-md-4 p-0">@lang('translate.numder')</div>
                <div class="col-xs-6 col-md-4 p-0">@lang('translate.occasion')</div>
                <div class="col-xs-6 col-md-4 p-0">@lang('translate.other_phone')</div>
            </div>

            @foreach ($reservations as $key => $reservation)
              <hr class="m-0">
              <div class="clearfix panel-body">
                <div class="col-xs-6 col-md-3 p-0">{{ $reservation->name }}</div>
                <div class="col-xs-6 col-md-4 p-0">
                  <input type="text" value="{{ $reservation->date.'-'.$reservation->time }}" class="m-0 max-width" readonly disabled>
                </div>
                <div class="col-xs-6 col-md-3 p-0">{{ $reservation->numder }}</div>
                <div class="col-xs-6 col-md-3 p-0">{{ $reservation->occasion }}</div>
                <div class="col-xs-6 col-md-3 p-0">{{ $reservation->other_phone }}</div>
              </div>
            @endforeach

            <ul class="pagination">
                {{ $reservations->links() }}
            </ul>

        </div>

    </div>
  @endsection
