@extends('dashboard.layout')
@section('content')
  <div class="padding-bottom">
    <div class="container">

      <div class="main-line-header text-center print-hide">
        <h1><span>مشاهدة الطلب</span></h1>
      </div>


      <div class="row m-20">

        <div class="col-xs-12 col-md-6">
          <div class="clearfix">

            <div class="col-xs-12 col-sm-4 p-0">
              <label>اسم الشخص</label>
            </div>

            <div class="col-xs-12 col-sm-8 p-0">
              <input type="text" value="{{ $order->name }}" class="input-b-bottom" readonly disabled>
            </div>

          </div>
        </div>

        <div class="col-xs-12 col-md-6">
          <div class="clearfix">

            <div class="col-xs-12 col-sm-4 p-0">
              <label>رقم الموبايل</label>
            </div>

            <div class="col-xs-12 col-sm-8 p-0">
              <input type="text" value="{{ $order->phone }}" class="input-b-bottom" readonly disabled>
            </div>

          </div>
        </div>

        <div class="col-xs-12 col-md-6">
          <div class="clearfix">

            <div class="col-xs-12 col-sm-4 p-0">
              <label>الإيميل</label>
            </div>

            <div class="col-xs-12 col-sm-8 p-0">
              <input type="text" value="{{ $order->email }}" class="input-b-bottom" readonly disabled>
            </div>

          </div>
        </div>

        <div class="col-xs-12 col-md-6">
          <div class="clearfix">

            <div class="col-xs-12 col-sm-4 p-0">
              <label>ملاحظات</label>
            </div>

            <div class="col-xs-12 col-sm-8 p-0">
              <input type="text" value="{{ $order->notes }}" class="input-b-bottom" readonly disabled>
            </div>

          </div>
        </div>

      </div>

      <div class="table-responsive m-20">
        <table class="table view-table border-last">
          <thead>
            <tr>
              <th>العدد</th>
              <th>اسم الوجبة</th>
              <th>الحجم</th>
              <th>السعر</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($products as $key => $product)
              <?php $product_data = getProductData($product);?>
              <tr>
                <td><span>{{ $product->quantity }}</span></td>
                <td>
                  <span>
                    @if (App::isLocale('en'))
                      {{ $product_data->en_name }}
                    @else
                      {{ $product_data->ar_name }}
                    @endif
                  </span>
                </td>
                <td>
                  <span>
                    @if (!empty($product->size))
                      @lang('translate.'.$product->size)
                    @else
                      @lang('translate.one_size')
                    @endif
                  </span>
                </td>
                <td>
                  <span>
                    @if (isset($product_data->price))
                      {{ $product_data->price }}
                    @else
                      <?php $size = json_decode($product_data->size, true);?>
                      {{ $size[$product->size] }}
                    @endif
                  </span>
                </td>
              </tr>
            @endforeach

          </tbody>
        </table>
      </div>


      <div class="text-center print-hide">
        <span class="btn btn-red">الإجمالى {{ $order->total }}</span>
        <button class="btn btn-default" onclick="window.print()">طباعة</button>
      </div>

    </div>
  </div>
@endsection
