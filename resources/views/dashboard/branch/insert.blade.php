@extends('dashboard.layout')
  @section('content')
    <div class="container div-padding">
        <form action="{{ asset('dashboard/branch') }}" method="post">
            {{ csrf_field() }}
             <input type="hidden" name="role">
             <div class="main-line-header text-center">
                 <h1><span>@lang('translate.management')</span></h1>
             </div>
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                  <label>@lang('translate.manager_name')</label>
                  <input type="text" placeholder="" name="name" value="{{ old('name') }}">
                  @if ($errors->has('name'))
                  <p style="color:red">{{ $errors->first('name') }}</p>
                  @endif
              </div>
              <div class="col-xs-12 col-sm-6">
                  <label>@lang('translate.email')</label>
                  <input type="email" placeholder="" name="email" value="{{ old('email') }}">
                  @if ($errors->has('email'))
                  <p style="color:red">{{ $errors->first('email') }}</p>
                  @endif
              </div>
                <div class="col-xs-12 col-sm-6">
                    <label>@lang('translate.password')</label>
                    <input type="text" placeholder="" name="phone" value="{{ old('phone') }}">
                    @if ($errors->has('phone'))
                    <p style="color:red">{{ $errors->first('phone') }}</p>
                    @endif
                </div>
                <div class="col-xs-12 col-sm-6">
                    <label>@lang('translate.')</label>
                    <input type="password" placeholder="" name="password" value="{{ old('password') }}">
                    @if ($errors->has('password'))
                    <p style="color:red">{{ $errors->first('password') }}</p>
                    @endif
                </div>
                <div class="col-xs-12 col-sm-6">
                    <label>@lang('translate.city')</label>
                    <select title="المدينة" id="city" value="{{ old('city') }}" name="city">
                      <option disabled selected>اختر المدينة </option>
                      @foreach ($cities as $key => $city)
                        <option value="{{ $city->id }}">{{ $city->ar_name }}</option>
                      @endforeach
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <label>@lang('translate.area')</label>
                  <select title="الحى" id="area" name="area_id" value="{{ old('area_id') }}">
                    <option id="area_child" disabled selected>اختر الحي </option>
                  </select>
                  @if ($errors->has('area_id'))
                  <p style="color:red">{{ $errors->first('area_id') }}</p>
                  @endif
                </div>
                <div class="main-line-header text-center">
                    <h1><span>@lang('translate.data')</span></h1>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <label>@lang('translate.commercial_registry')</label>
                    <input type="text" placeholder="" name="commercial_registry" value="{{ old('commercial_registry') }}">
                    @if ($errors->has('commercial_registry'))
                    <p style="color:red">{{ $errors->first('commercial_registry') }}</p>
                    @endif
                </div>
                <div class="col-xs-12 col-sm-6">
                    <label>@lang('translate.civil_registry')</label>
                    <input type="text" placeholder="" name="civil_registry" value="{{ old('civil_registry') }}">
                    @if ($errors->has('civil_registry'))
                    <p style="color:red">{{ $errors->first('civil_registry') }}</p>
                    @endif
                </div>
                <div class="col-xs-12 col-sm-6">
                    <label>@lang('translate.address_')</label>
                    <input type="text" placeholder="" name="address" value="{{ old('address') }}">
                    @if ($errors->has('address'))
                    <p style="color:red">{{ $errors->first('address') }}</p>
                    @endif
                </div>
                <div class="col-xs-12 col-sm-6">
                    <label>@lang('translate.delivery_price')</label>
                    <input type="text" placeholder="" name="delivery" value="{{ old('delivery') }}">
                    @if ($errors->has('delivery'))
                    <p style="color:red">{{ $errors->first('delivery') }}</p>
                    @endif
                </div>
            </div>

            <div id="map" class="google-map"></div>
            <input type="hidden" name="latLng" id="latLng" value="{{ old('latLng') }}">
            @if ($errors->has('latLng'))
            <p style="color:red">{{ $errors->first('latLng') }}</p>
            @endif
            <div class="text-center m-20">
                <button type="submit" class="btn btn-red">@lang('translate.add')</button>
            </div>
            <script>
              var map;
              var markersArray = [];
              function initMap(){
                var saudi = new google.maps.LatLng(23.8859, 45.0792);
                var myOptions = {
                  zoom: 6,
                  center: saudi,
                };
                map = new google.maps.Map(document.getElementById("map"), myOptions);
                var marker = new google.maps.Marker({map: map, position: saudi});
                // add a click event handler to the map object
                google.maps.event.addListener(map, "click", function(event){
                  marker.setMap(null);
                  // place a marker
                  placeMarker(event.latLng);
                  // display the lat/lng in your form's lat/lng fields
                  console.log(event.latLng.lat() + '-'+ event.latLng.lng());
                  $('#latLng').val(event.latLng.lat() + '-'+ event.latLng.lng());
                  // document.getElementById("latLng").value =  ;
                });
              }
              function placeMarker(location){
                // first remove all markers if there are any
                deleteOverlays();
                var marker = new google.maps.Marker({
                  position: location,
                  map: map
                });
                // add marker in markers array
                markersArray.push(marker);
                //map.setCenter(location);
              }
              // Deletes all markers in the array by removing references to them
              function deleteOverlays(){
                if (markersArray) {
                  for (i in markersArray) {
                    markersArray[i].setMap(null);
                  }
                  markersArray.length = 0;
                }
              }
            </script>

        </form>
    </div>
    <script async defer src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBGERiio3BH9dTWcAghYLpldQKnWFr9-OE&callback=initMap"></script>

    <script src="{{asset('site/js/filter.js')}}"></script>
  @endsection
