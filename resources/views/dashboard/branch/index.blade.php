@extends('dashboard.layout')
  @section('content')

            <div class="container div-padding">

                <div class="table-responsive m-20">
                    <table class="table view-table">
                        <thead>
                        <tr>
                            <th>@lang('translate.city')</th>
                            <th>@lang('translate.area')</th>
                            <th>@lang('translate.commercial_registry')</th>
                            <th>@lang('translate.civil_registry')</th>
                            <th>@lang('translate.additions_')</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach ($branches as $key => $branch)
                            <tr>
                              <td><span>{{ $branch->city_ar_name }}</span></td>
                              <td><span>{{ $branch->area_ar_name }}</span></td>
                              <td><span>{{ $branch->commercial_registry }}</span></td>
                              <td><span>{{ $branch->civil_registry }}</span></td>
                              <td><div class="table-view text-left">
                                <div class="table-cell"><button class="success"><i class="icofont icofont-pencil"></i></button></div>
                                <div class="table-cell"><button class="danger"><i class="icofont icofont-close"></i></button></div>
                              </div></td>
                            </tr>
                          @endforeach
                        </tbody>
                    </table>
                    <ul class="pagination">
                        {{ $branches->links() }}
                    </ul>
                </div>

                <div id="map" class="google-map"></div>

                <script>
                    function initMap() {
                        var loc = {lat: 30.048178, lng: 31.236323};
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 13,
                            center: loc
                        });
                        var marker = new google.maps.Marker({
                            position: loc,
                            map: map,
                            animation: google.maps.Animation.DROP
                        });
                    }
                </script>
                <script async defer src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBGERiio3BH9dTWcAghYLpldQKnWFr9-OE&callback=initMap"></script>

            </div>

  @endsection
