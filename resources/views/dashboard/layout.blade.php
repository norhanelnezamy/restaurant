<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="robots" content="index/follow">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="HandheldFriendly" content="true">

    <title>@lang('translate.admin_panel')</title>
    <link rel="shortcut icon" type="image/png" href="{{asset('public/dashboard/images/fav-icon.png')}}">

    <meta name="theme-color" content="#454545">
    <meta name="msapplication-navbutton-color" content="#454545">
    <meta name="apple-mobile-web-app-status-bar-style" content="#454545">

    <link rel="stylesheet" type="text/css" href="{{asset('public/dashboard/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/dashboard/css/icofont.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/dashboard/css/style.css')}}">
    @if (App::isLocale('en'))
      <link rel="stylesheet" type="text/css" href="{{asset('public/dashboard/css/style-en.css')}}">
    @else
      <link rel="stylesheet" type="text/css" href="{{asset('public/dashboard/css/style-ar.css')}}">
    @endif

    <script src="{{asset('public/dashboard/js/jquery-1.11.1.min.js')}}"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->

</head>

<body>

<!--===============================
    LOADER
===================================-->

<div class="pre-load">
    <div class="loader"></div>
</div>



<div class="win-height">


    <!--===============================
        HEADER
    ===================================-->

    <header>
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <ul class="list-inline list-inline">
                        <li><a href="{{ asset('lang/switch/ar') }}">عربي</a></li>
                        <li>|</li>
                        <li><a href="{{ asset('lang/switch/en') }}">English</a></li>
                    </ul>
                </div>
                <div class="col-xs-6 text-left">
                    <ul class="list-inline list-inline">
                        <li><a style="color: #ff0000;" href="{{ asset('logout') }}" >@lang('translate.log_out') </a></li>
                        <li>|</li>
                        <li><i class="icofont icofont-user-alt-3 right-fa"></i> <span class="pull-left">{{Auth::guard('web')->user()->name}}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    {{--<header>
        <div class="container text-left">
          <a style="color: #ff0000;" href="{{ asset('logout') }}" title="خروج ">خروج </a>
          <i class="icofont icofont-user-alt-3 right-fa"></i> <span class="pull-left">{{Auth::guard('web')->user()->name}}</span>
        </div>
    </header>--}}



    <div class="main-div-bg">


        <!--===============================
            NAV
        ===================================-->

        <nav class="navbar text-center">
            <div class="container">
                <a href="{{ asset('/') }}" class="div-small-padding"><img src="{{asset('public/dashboard/images/logo.png')}}" alt="LOGO"></a>
                <div class="row">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                            <span class="icofont icofont-navigation-menu"></span>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <ul class="nav navbar-nav">
                          @if (Auth::guard('web')->user()->role == 'restaurant'|| Auth::guard('web')->user()->role == 'smart_restaurant')
                            <li><a href="{{ asset('dashboard/index') }}">@lang('translate.info')  </a></li>
                            <li><a href="{{ asset('dashboard/branch') }}">@lang('translate.show_branch')  </a></li>
                            <li><a href="{{ asset('dashboard/branch/create') }}">@lang('translate.add_branch') </a></li>
                            <li><a href="{{ asset('dashboard/feedback') }}">@lang('translate.feedback') </a></li>
                            <li class="dropdown">
                              <a class="dropdown-toggle" data-toggle="dropdown">@lang('translate.ad') <span class="icofont icofont-simple-down left-fa text-red"></span></a>
                              <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{ asset('dashboard/b/ad') }}">@lang('translate.show_ad')</a></li>
                                <li><a href="{{ asset('dashboard/ad/create') }}">@lang('translate.add_ad')</a></li>
                              </ul>
                            </li>
                          @else
                            @if (Auth::guard('web')->user()->role == 'productive_family')
                              <li><a href="{{ asset('dashboard/index') }}">@lang('translate.info') </a></li>
                              <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown">@lang('translate.ad') <span class="icofont icofont-simple-down left-fa text-red"></span></a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                  <li><a href="{{ asset('dashboard/b/ad') }}">@lang('translate.show_ad')</a></li>
                                  <li><a href="{{ asset('dashboard/ad/create') }}">@lang('translate.add_ad')</a></li>
                                </ul>
                              </li>
                            @endif
                            <li class="dropdown">
                              <a class="dropdown-toggle" data-toggle="dropdown">@lang('translate.food_menu') <span class="icofont icofont-simple-down left-fa text-red"></span></a>
                              <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{ asset('dashboard/b/menu') }}">@lang('translate._menu')</a></li>
                                <li><a href="{{ asset('dashboard/b/food/create') }}">@lang('translate.add_meals')</a></li>
                              </ul>
                            </li>
                            <li><a href="{{ asset('dashboard/b/additional') }}">@lang('translate.additions_')</a></li>
                            <li class="dropdown">
                              <a class="dropdown-toggle" data-toggle="dropdown">@lang('translate.Offers_')<span class="icofont icofont-simple-down left-fa text-red"></span></a>
                              <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{ asset('dashboard/b/offer') }}">@lang('translate.Browse_Offers') </a></li>
                                <li><a href="{{ asset('dashboard/b/offer/create') }}">@lang('translate.Add_Offers') </a></li>
                              </ul>
                            </li>
                            <li><a href="{{ asset('dashboard/b/order/index/delivery') }}">@lang('translate.deliveries')</a></li>
                            <li><a href="{{ asset('dashboard/b/order/index/prepare') }}">@lang('translate.processing_order')</a></li>
                            <li><a href="{{ asset('dashboard/b/order/r/reservation') }}">@lang('translate.table_reservation')</a></li>
                            <li><a href="{{ asset('dashboard/b/photo') }}">@lang('translate.photo')</a></li>
                            <li><a href="{{ asset('dashboard/b/feedback') }}">@lang('translate.rating')</a></li>
                          @endif

                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <!--===============================
            CONTENT
        ===================================-->

        @yield('content')

        @if (session()->has('add_message') || session()->has('update_message') || session()->has('delete_message'))
          <div class="modal fade" id="message" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                  <div class="modal-content">
                      <div class="panel-body">
                        @if (session()->has('add_message'))
                          <p style="color:green">@lang('translate.add_message')</p>
                        @elseif (session()->has('update_message'))
                          <p style="color:orange">@lang('translate.update_message')</p>
                        @else
                          <p style="color:red">@lang('translate.delete_message')</p>
                        @endif
                      </div>
                  </div>
              </div>
          </div>
          <script>
          $(function(){
            $('#message').modal('show');
          });
          </script>

        @endif

    </div>

</div>

<!--===============================
    FOOTER
===================================-->

<footer>
    <div class="container text-center">
        <p>@lang('translate.footer_2') <a href="#">@lang('translate.company')</a> 2017 </p>
    </div>
</footer>



<!--===============================
    SCRIPT
===================================-->

<script src="{{asset('public/dashboard/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/script.js')}}"></script>
</body>
</html>
