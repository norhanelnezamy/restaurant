@extends('layout')
@section('content')
<div class="div-padding bg-gray p-b-80 login-bg">
    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-pull-3 text-center main-header">
                <h1 class="text-red animate-box">@lang('translate.register_title')</h1>
            </div>
        </div>
        <div class="animate-box">
            <div class="head-icon">
                <img src="{{asset('public/site/images/login-icon.png')}}">
            </div>
        </div>

        <div class="panel panel-br0">
            <div class="panel-body p-t-b-20">
                <div class="m-t-50">
                    <h1 class="f-s-20 text-center">@lang('translate.register_title')</h1>

                    <div class="div-small-padding text-center">
                        <div class="clearfix choose-type">
                            <div class="col-xs-12 col-sm-6 right-item">
                                <label class="animate-box right-in">
                                    <input type="radio" name="type" value="order" class="choose-form user">
                                    <i class="circle"></i>
                                    <span>@lang('translate.applicant')</span>
                                </label>
                            </div>

                            <div class="col-xs-12 col-sm-6 left-item">
                                <label class="animate-box left-in">
                                    <input type="radio" name="type" value="owner" class="choose-form restaurant">
                                    <i class="circle"></i>
                                    <span>@lang('translate.rest_owner')</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="input-of-form user" id="order">
                        <form action="{{ asset('/register') }}" method="post">
                          {{ csrf_field() }}
                            <div class="clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <input type="text" placeholder="@lang('translate.name_')" name="name">
                                    @if ($errors->has('name'))
                                    <p style="color:red">{{ $errors->first('name') }}</p>
                                    @endif
                                    <input type="email" placeholder="@lang('translate.email_')" name="email">
                                    @if ($errors->has('email'))
                                    <p style="color:red">{{ $errors->first('email') }}</p>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <input type="tel" placeholder="@lang('translate.phone_')" name="phone">
                                    @if ($errors->has('phone'))
                                    <p style="color:red">{{ $errors->first('phone') }}</p>
                                    @endif
                                    <input type="password" placeholder="@lang('translate.password_')" name="password">
                                    @if ($errors->has('password'))
                                    <p style="color:red">{{ $errors->first('password') }}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="text-center">
                                <a href="{{asset('terms')}}" class="text-black">@lang('translate.terms_') <span class="text-red">؟</span></a>
                                <br>
                                <label class="checkbox-holder">
                                    <input type="checkbox" name="terms">
                                    <span class="checkbox-icon"><i class="icofont icofont-check"></i></span>
                                    <span>@lang('translate.agreement_')</span>
                                    @if ($errors->has('terms'))
                                    <p style="color:red">{{ $errors->first('terms') }}</p>
                                    @endif
                                </label>
                                <br>
                                <button type="submit" class="btn btn-red btn-br-20 m-t-25">@lang('translate.sin_up')</button>
                            </div>

                        </form>
                    </div>

                    <div class="input-of-form restaurant" id="owner">
                        <form action="{{ asset('restaurant/register') }}" method="post">
                          {{ csrf_field() }}
                            <div class="clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <input name="name" type="text" placeholder="@lang('translate.name_')" value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                    <p style="color:red">{{ $errors->first('name') }}</p>
                                    @endif
                                    <input name="email" type="email" placeholder="@lang('translate.email_')" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                    <p style="color:red">{{ $errors->first('email') }}</p>
                                    @endif
                                    <select title="النوع " name="role">
                                        <option value="restaurant" selected>@lang('translate._restaurant')</option>
                                        <option value="smart_restaurant">@lang('translate.five_stars')</option>
                                        <option value="productive_family">@lang('translate.families_prroduced')</option>
                                    </select>
                                    @if ($errors->has('role'))
                                    <p style="color:red">{{ $errors->first('role') }}</p>
                                    @endif
                                    <?php
                                        use App\Model\Country;
                                        use App\Model\Category;
                                        $countries = Country::all();
                                        $categories = Category::all();
                                        ?>
                                    <select title="الدولة" id="country" name="country_id">
                                        @foreach ($countries as $key => $country)
                                        <option value="{{ $country->id }}">{{ $country->ar_name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('country_id'))
                                    <p style="color:red">{{ $errors->first('country_id') }}</p>
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <input name="phone" type="tel" placeholder="@lang('translate.phone_')" value="{{ old('phone') }}">
                                    @if ($errors->has('phone'))
                                    <p style="color:red">{{ $errors->first('phone') }}</p>
                                    @endif
                                    <input name="password" type="password" placeholder="@lang('translate.password_')" value="{{ old('password') }}">
                                    @if ($errors->has('password'))
                                    <p style="color:red">{{ $errors->first('password') }}</p>
                                    @endif
                                    <input name="restaurant_name" type="text" placeholder="@lang('translate.rest_name')" value="{{ old('restaurant_name') }}">
                                    @if ($errors->has('restaurant_name'))
                                    <p style="color:red">{{ $errors->first('restaurant_name') }}</p>
                                    @endif
                                    <select title="نوع المطبخ" id="category" name="category_id">
                                      @foreach ($categories as $key => $category)
                                        <option value="{{ $category->id }}">{{$category->ar_name}}</option>
                                      @endforeach
                                   </select>
                                </div>
                            </div>

                            <div class="text-center">
                                <a href="{{asset('terms')}}" class="text-black">@lang('translate.terms_') <span class="text-red">؟</span></a>
                                <br>
                                <label class="checkbox-holder">
                                    <input type="checkbox" name="terms">
                                    <span class="checkbox-icon"><i class="icofont icofont-check"></i></span>
                                    <span>@lang('translate.agreement_')</span>
                                    @if ($errors->has('terms'))
                                    <p style="color:red">{{ $errors->first('terms') }}</p>
                                    @endif
                                </label>
                                <br>
                                <button type="submit" class="btn btn-red btn-br-20 m-t-25">@lang('translate.sin_up')</button>
                            </div>

                        </form>
                    </div>


                </div>
            </div>
        </div>

    </div>
</div>

@endsection
