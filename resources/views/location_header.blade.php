
<script src="{{asset('public/site/js/filter.js')}}"></script>

<div class="container text-center">

  <div class="text-white">
    <h1 class="animate-box">@lang('translate.header_title_1')</h1>
    <p class="animate-box">@lang('translate.header_title_2')</p>
  </div>

  <div class="animate-box right-in">
    <a data-toggle="modal" data-target="#mapModal" class="btn btn-white half-btn red-half">
      <span>@lang('translate.discover_location')</span>
      <div class="red-icon"><i class="icofont icofont-globe-alt"></i></div>
    </a>
  </div>

  <form method="get" class="search-box clearfix">
    <div class="col-xs-12 col-sm-6 col-20 p-0">
      <i class="icofont icofont-social-google-map text-red"></i>
      <?php
      $countries = country();
      $categories = category();
      ?>
      <select id="country">
        <option>@lang('translate.country')</option>
        @if (App::isLocale('en'))
          @foreach ($countries as $key => $country)
            <option value="{{ $country->id }}">{{ $country->en_name }}</option>
          @endforeach
        @else
          @foreach ($countries as $key => $country)
            <option value="{{ $country->id }}">{{ $country->ar_name }}</option>
          @endforeach
        @endif
      </select>
    </div>
    <div class="col-xs-12 col-sm-6 col-20 p-0">
      <i class="icofont icofont-social-google-map text-red"></i>
      <select id="city">
        <option id="city_child">@lang('translate.city')</option>
      </select>
    </div>
    <div class="col-xs-12 col-sm-6 col-20 p-0">
      <i class="icofont icofont-social-google-map text-red"></i>
      <select id="area" name="area">
        <option id="area_child">@lang('translate.district')</option>
      </select>
    </div>
    <div class="col-xs-12 col-sm-6 col-20 p-0">
      <i class="icofont icofont-social-google-map text-red"></i>
      <select>
        <option>@lang('translate.rest_type')</option>
        @if (App::isLocale('en'))
          @foreach ($categories as $key => $category)
            <option value="{{ $category->id }}">{{$category->en_name}}</option>
          @endforeach
        @else
          @foreach ($categories as $key => $category)
            <option value="{{ $category->id }}">{{$category->ar_name}}</option>
          @endforeach
        @endif
      </select>
    </div>

    <div class="col-xs-12 col-20 p-0">
      <button type="submit" class="btn btn-red btn-block">@lang('translate.discover_food')</button>
    </div>
  </form>

<?php $setting = setting();?>

  <div class="social">
    <ul class="list-unstyled">
      <li class="animate-box left-in"><a href="{{ $setting->facebook }}" data-toggle="tooltip" data-placement="right" title="Facebook"><i class="icofont icofont-social-facebook"></i></a></li>
      <li class="animate-box left-in"><a href="{{ $setting->twitter }}" data-toggle="tooltip" data-placement="right" title="Twitter"><i class="icofont icofont-social-twitter"></i></a></li>
      <li class="animate-box left-in"><a href="{{ $setting->google }}" data-toggle="tooltip" data-placement="right" title="Google+"><i class="icofont icofont-social-google-plus"></i></a></li>
      <li class="animate-box left-in"><a href="{{ $setting->instgram }}" data-toggle="tooltip" data-placement="right" title="Instagram"><i class="icofont icofont-social-instagram"></i></a></li>
    </ul>
  </div>

</div>


<!--===============================
    MAP MODAL
===================================-->

<div class="modal fade" id="mapModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div id="map" class="google-map"></div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGERiio3BH9dTWcAghYLpldQKnWFr9-OE"></script>
<script type="text/javascript">
window.onload = getLocation;
function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    alert("Geolocation is not supported by browser.");
  }
}
function showPosition(position) {
  var lat = position.coords.latitude;
  var lng = position.coords.longitude;
  var latLng = new google.maps.LatLng(lat, lng);
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: latLng,
  });
  var marker = new google.maps.Marker({
    position: latLng,
    map: map,
    draggable: true
  });
}
</script>
