@extends('layout')
 @section('content')
   <div class="div-padding bg-gray p-b-80 login-bg">
       <div class="container">

           <div class="row">
               <div class="col-xs-12 col-sm-6 col-sm-pull-3 text-center main-header">
                   <h1 class="text-red animate-box">@lang('translate.login_title')</h1>
               </div>
           </div>

           <div class="animate-box">
               <div class="head-icon">
                   <img src="{{asset('public/site/images/login-icon.png')}}">
               </div>
           </div>

           <div class="panel panel-br0">
               <div class="panel-body p-t-b-20">
                   <div class="m-t-50">
                       <h1 class="f-s-20 text-center">@lang('translate.register_type')</h1>
                       <form action="{{ asset('/login') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="site" value="1">
                           <div class="clearfix m-t-25">
                               <div class="col-xs-12 col-sm-6">
                                   <input name="phone" type="tel"  placeholder="@lang('translate.phone_')" >
                                   @if ($errors->has('phone'))
                                   <p style="color:red">{{ $errors->first('phone') }}</p>
                                   @endif
                               </div>
                               <div class="col-xs-12 col-sm-6">
                                   <input name="password" type="password" placeholder="@lang('translate.password_')">
                                   @if ($errors->has('password'))
                                   <p style="color:red">{{ $errors->first('password') }}</p>
                                   @endif
                               </div>
                           </div>
                           @if (session()->has('authorized'))
                             <p style="color:red">{{ session()->get('authorized') }}</p>
                           @endif
                           <div class="text-center">
                               <button type="submit" class="btn btn-red btn-br-20 m-t-15">@lang('translate.sin_in')</button>
                           </div>

                       </form>
                   </div>
               </div>
           </div>

       </div>
   </div>
 @endsection
