<!DOCTYPE html>
<html lang="ar">
<head>
  <head>
      <meta charset="UTF-8">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="">
      <meta name="robots" content="index/follow">

      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <meta name="HandheldFriendly" content="true">

      <title>fast food</title>
      <link rel="shortcut icon" type="image/png" href="{{asset('public/dashboard/images/fav-icon.png')}}">

      <meta name="theme-color" content="#454545">
      <meta name="msapplication-navbutton-color" content="#454545">
      <meta name="apple-mobile-web-app-status-bar-style" content="#454545">

      <link rel="stylesheet" type="text/css" href="{{asset('public/dashboard/css/bootstrap.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('public/dashboard/css/icofont.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('public/dashboard/css/style.css')}}">

      <script src="{{asset('public/dashboard/js/jquery-1.11.1.min.js')}}"></script>

      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <![endif]-->

  </head>

<body>

<!--===============================
    LOADER
===================================-->

<div class="pre-load">
    <div class="loader"></div>
</div>



<div class="win-height">


    <!--===============================
        HEADER
    ===================================-->

    {{-- <header>
        <div class="container text-left">
            <i class="icofont icofont-user-alt-3 right-fa"></i> <span class="pull-left"></span>
        </div>
    </header> --}}

    <div class="main-div-bg">

        <!--===============================
            CONTENT
        ===================================-->

        <div class="not-found-div">
            <div class="container text-center">
                <div class="above-number">
                    <h1>404</h1>
                    <h2>Not found</h2>
                </div>
                <h1>WE ARE SORRY</h1>
                <p>We seem to have lost this page, try one of these instead</p>
                <a href="{{ asset('/') }}" class="btn btn-orange">take me home</a>
            </div>
        </div>

    </div>

</div>

<!--===============================
    FOOTER
===================================-->

{{-- <footer>
    <div class="container text-center">
        <p>تصميم وبرمجة شركة <a href="#">خليج البرمجة</a> 2017</p>
    </div>
</footer> --}}

<!--===============================
    SCRIPT
===================================-->

<script src="{{asset('public/dashboard/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/dashboard/js/script.js')}}"></script>
</body>
</html>
