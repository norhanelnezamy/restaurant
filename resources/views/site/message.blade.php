@extends('layout')
@section('content')
  <div class="div-padding bg-gray p-b-80 login-bg">
      <div class="container">

          <div class="row">
              <div class="col-xs-12 col-sm-6 col-sm-pull-3 text-center main-header">
                  <h1 class="text-red animate-box">@lang('translate.messages')</h1>
              </div>
          </div>

          <div class="panel panel-br0">
              <div class="panel-body p-t-b-20">
                  <div class="m-t-50">

                      <div class="div-small-padding">

                        @foreach ($messages as $key => $message)
                          <div class="panel bg-gray panel-br0">
                            <div class="panel-body">
                              <div class="table-view message-box">
                                <div class="table-cell block"><i class="icofont icofont-mail text-red"></i></div>
                                <div class="table-cell block">
                                  <p>
                                    <span class="text-red">
                                      @if (App::isLocale('en'))
                                        {{ $message->brand_en_name }}
                                      @else
                                        {{ $message->brand_ar_name }}
                                      @endif
                                    </span>
                                    {{ $message->content }}
                                  </p>
                                  <span class="text-muted">{{ $message->created_at }}</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        @endforeach

                      </div>

                      {{ $messages->links() }}

                  </div>
              </div>
          </div>

      </div>
  </div>
@endsection
