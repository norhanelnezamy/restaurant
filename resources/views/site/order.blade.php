@extends('layout')
 @section('content')
   <div class="div-padding bg-gray p-b-80 login-bg">
       <div class="container">

           <div class="row">
               <div class="col-xs-12 col-sm-6 col-sm-pull-3 text-center main-header">
                   <h1 class="text-red animate-box">@lang('translate.my_order')</h1>
               </div>
           </div>
           <div class="animate-box">
               <div class="head-icon">
                   <img src="{{asset('public/site/images/orders-icon.png')}}">
               </div>
           </div>

           <div class="panel panel-br0">
               <div class="panel-body p-t-b-20">
                   <div class="m-t-50">
                       <h1 class="f-s-20 text-center">@lang('translate.wait_orders')</h1>

                       <div class="clearfix">

                         @foreach ($wait_orders as $key => $order)
                           <?php $products = $order->products;?>
                           @foreach ($products as $key => $product)
                             <?php $product_data = getProductData($product);?>
                             <div class="col-xs-12 col-md-6">
                               <div class="table-view order-box">

                                 <div class="table-cell block">
                                   <div class="order-img">
                                     @if (isset($product_data->photo))
                                       <img src="{{asset('public/uploads/'.$product_data->photo)}}">
                                     @endif
                                   </div>
                                 </div>

                                 <div class="table-cell block">
                                   <div class="table-view">
                                     <div class="table-cell v-top">
                                       <h3 class="f-s-20">
                                         <span>
                                           @if (App::isLocale('en'))
                                             {{ $product_data->en_name }}
                                           @else
                                             {{ $product_data->ar_name }}
                                           @endif
                                         </span>
                                       </h3>
                                       <p class="text-muted din-font">
                                         @if (isset($product_data->en_describe) && isset($product_data->ar_describe))
                                           @if (App::isLocale('en'))
                                             {{ $product_data->en_describe }}
                                           @else
                                             {{ $product_data->ar_describe }}
                                           @endif
                                         @endif
                                       </p>
                                     </div>

                                     <div class="table-cell text-left v-top">
                                       <span class="f-s-20 din-font">{{ $product->price }}</span>
                                     </div>
                                   </div>
                                 </div>

                               </div>
                             </div>
                           @endforeach
                         @endforeach

                       </div>

                       <div class="text-center">
                           <span class="btn btn-br-20 btn-gray btn-border"><span class="text-red">ملحوظة</span> : لايمكن الغاء الطلب بعد مرور 10 دقائق</span>
                       </div>

                   </div>
               </div>
           </div>

           <div class="panel panel-br0">
               <div class="panel-body p-t-b-20">
                   <div class="m-t-50">
                       <h1 class="f-s-20 text-center">@lang('translate.prev_orders')</h1>

                       <div class="clearfix">

                          @foreach ($prev_orders as $key => $order)
                            <?php $products = $order->products;?>
                            @foreach ($products as $key => $product)
                              <?php $product_data = getProductData($product);?>
                              <div class="col-xs-12 col-md-6">
                                <div class="table-view order-box">

                                  @if (isset($product_data->photo))
                                    <div class="table-cell block">
                                      <div class="order-img">
                                        <img src="{{asset('public/uploads/'.$product_data->photo)}}">
                                      </div>
                                    </div>
                                  @endif

                                  <div class="table-cell block">
                                    <div class="table-view">
                                      <div class="table-cell v-top">
                                        <h3 class="f-s-20">
                                          <span>
                                            @if (App::isLocale('en'))
                                              {{ $product_data->en_name }}
                                            @else
                                              {{ $product_data->ar_name }}
                                            @endif
                                          </span>
                                        </h3>
                                        <p class="text-muted din-font">
                                          @if (isset($product_data->en_describe) && isset($product_data->ar_describe))
                                            @if (App::isLocale('en'))
                                              {{ $product_data->en_describe }}
                                            @else
                                              {{ $product_data->ar_describe }}
                                            @endif
                                          @endif
                                        </p>
                                      </div>

                                      <div class="table-cell text-left v-top">
                                        <span class="f-s-20 din-font">{{ $product->price }}</span>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            @endforeach
                          @endforeach

                       </div>

                   </div>
               </div>
           </div>

       </div>
   </div>

 @endsection
