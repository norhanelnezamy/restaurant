@extends('layout')
 @section('content')

   <div class="div-padding bg-gray p-b-80 login-bg">
       <div class="container">

           <div class="row">
               <div class="col-xs-12 col-sm-6 col-sm-pull-3 text-center main-header">
                   <h1 class="text-red animate-box">@lang('translate.account')</h1>
               </div>
           </div>


           <div class="animate-box">
               <div class="head-icon">
                   <img src="{{asset('public/site/images/login-icon.png')}}">
               </div>
           </div>

           <div class="panel panel-br0">
               <div class="panel-body p-t-b-20">
                   <div class="m-t-50">
                       <h1 class="f-s-20 text-center">@lang('translate.user_account')</h1>


                       <div class="div-small-padding text-center">
                           <div class="clearfix choose-type">
                               <div class="col-xs-12 col-sm-6 right-item">
                                   <label class="animate-box right-in">
                                       <i class="circle"></i>
                                       <span>@lang('translate.user_account')</span>
                                   </label>
                               </div>

                               <div class="col-xs-12 col-sm-6 left-item">
                                   <label class="animate-box left-in">
                                       <i class="circle"></i>
                                       <span>@lang('translate.change_password')</span>
                                   </label>
                               </div>
                           </div>
                       </div>


                       <div class="clearfix">
                           <div class="col-xs-12 col-sm-6">
                               <form action="{{ asset('user/data/update') }}" method="post" class="animate-box right-in">
                                 {{ csrf_field() }}
                                   <label>@lang('translate.name')</label>
                                   <input type="text" class="full-border bg-gray" value="{{ Auth::user()->name }}" name="name">
                                   @if ($errors->has('name'))
                                   <p style="color:red">{{ $errors->first('name') }}</p>
                                   @endif
                                   <label>@lang('translate.email')</label>
                                   <input type="email" class="full-border bg-gray" value="{{ Auth::user()->email }}" name="email">
                                   @if ($errors->has('email'))
                                   <p style="color:red">{{ $errors->first('email') }}</p>
                                   @endif
                                   <label>@lang('translate.phone')</label>
                                   <input type="text" class="full-border bg-gray" value="{{ Auth::user()->phone }}" name="phone">
                                   @if ($errors->has('phone'))
                                   <p style="color:red">{{ $errors->first('phone') }}</p>
                                   @endif
                                   <div class="text-left">
                                       <button type="submit" class="btn btn-red">تحديث</button>
                                   </div>
                               </form>
                           </div>

                           <div class="col-xs-12 col-sm-6">
                               <form action="{{ asset('user/password/update') }}" method="post" class="animate-box left-in">
                                 {{ csrf_field() }}
                                   <label>@lang('translate.new_password')</label>
                                   <input type="password" placeholder="كلمة المرور الجديدة" class="full-border bg-gray" name="password">
                                   @if ($errors->has('password'))
                                   <p style="color:red">{{ $errors->first('password') }}</p>
                                   @endif
                                   <label>@lang('translate.confirm_password')</label>
                                   <input type="password" placeholder="تأكيد كلمة المرور" class="full-border bg-gray" name="password_confirmation">
                                   @if ($errors->has('password_confirmation'))
                                   <p style="color:red">{{ $errors->first('password_confirmation') }}</p>
                                   @endif
                                   <div class="text-left">
                                       <button type="submit" class="btn btn-red">تغيير</button>
                                   </div>
                               </form>
                           </div>
                       </div>

                   </div>
               </div>
           </div>

       </div>
   </div>
 @endsection
