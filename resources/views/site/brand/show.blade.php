@extends('layout')
@section('content')
  <?php
  $brand = brand($id);
  $feedback_food = rate('food', $id);
  $feedback_service = rate('services', $id);
  $feedback_delivery = rate('delivery', $id);
  $total = 0
  ?>
  <div class="relative-parent animate-box left-in">
      <div class="rest-logo-holder">
          <img src="{{ asset('public/uploads/'.$brand->logo) }}" alt="logo">
      </div>
  </div>
  <div class="div-padding">
      <div class="container">
          <div class="row">
              <div class="clearfix">
                  <div class="col-xs-12 col-sm-8 col-sm-pull-2 col-lg-6 col-lg-pull-3">
                      <div class="panel panel-br0 light-gray m-t-50 animate-box">
                          <div class="panel-body">
                              <h3 class="panel-title text-center m-b-15">
                                @if (App::isLocale('en'))
                                  {{ $brand->en_name }}
                                @else
                                  {{ $brand->ar_name }}
                                @endif
                                <span class="left-fa text-red"><i class="icofont icofont-social-google-map"></i></span></h3>
                              <div class="clearfix">
                                  <div class="col-xs-12 col-sm-6 p-0 m-b-5">
                                      <small class="right-fa text-muted">@lang('translate.delivery')</small>
                                      @for ($i=1; $i <= $feedback_delivery ; $i++)
                                        <i class="icofont icofont-star text-yellow"></i>
                                      @endfor
                                      @for ($i=5; $i > $feedback_delivery ; $i--)
                                        <i class="icofont icofont-star text-muted"></i>
                                      @endfor
                                  </div>
                                  <div class="col-xs-12 col-sm-6 p-0 m-b-5">
                                      <small class="right-fa text-muted">@lang('translate.food_feedback')</small>
                                      @for ($i=1; $i <= $feedback_food ; $i++)
                                        <i class="icofont icofont-star text-yellow"></i>
                                      @endfor
                                      @for ($i=5; $i > $feedback_food ; $i--)
                                        <i class="icofont icofont-star text-muted"></i>
                                      @endfor
                                  </div>
                                  <div class="col-xs-12 col-sm-6 p-0 m-b-5">
                                      <small class="right-fa text-muted">@lang('translate.services')</small>
                                      @for ($i=1; $i <= $feedback_service ; $i++)
                                        <i class="icofont icofont-star text-yellow"></i>
                                      @endfor
                                      @for ($i=5; $i > $feedback_service ; $i--)
                                        <i class="icofont icofont-star text-muted"></i>
                                      @endfor
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

              @if ($errors->any())
                @if (App::isLocale('en'))
                  <p style="color:red">something wnt wrong !</p>
                @else
                  <p style="color:red">حدث خطأ ما .</p>
                @endif
              @endif

              @if (isset($checkout))
                @yield('checkout')
              @else
                <div class="col-xs-12 col-sm-8 animate-box">
                  <ul class="nav nav-justified tabs" role="tablist">
                    <li role="presentation" class="about"><a href="{{ asset('brand/show/'.$id) }}" ></a>@lang('translate.information')</li>
                    <li role="presentation" class="feedback"><a href="{{ asset('brand/feedback/'.$id) }}" ></a>@lang('translate.feedbacks')</li>
                    <li role="presentation" class="category"><a href="{{ asset('brand/p/category/'.$id) }}" ></a>@lang('translate.menu')</li>
                  </ul>
                  <div class="tab-content m-t-25">
                    @yield('about')

                    @yield('feedback')

                    @yield('menu')
                  </div>
                </div>

                <!--===============================
                CART
                ===================================-->

                <div class="col-xs-12 col-sm-4" id="cart">

                  <div class="panel panel-br0 light-gray animate-box left-in din-font">
                    <div class="panel-heading gray">@lang('translate.cart')</div>
                    <div class="panel-body">
                      @if (Cart::content()->count() != 0 && isset($branch))
                        @foreach (Cart::content() as $key => $product)
                          {{-- {{ $product->options->branch_id. '======' .$branch->id }} --}}
                          @if ($product->options->branch_id == $branch->id)
                            <?php $total += $product->price*$product->qty?>
                            <div class="table-view border-bottom">
                              <div class="table-cell">
                                <span class="label btn-border round btn-white text-red right-fa">{{$product->qty}}</span>
                                <span>{{ $product->name }}</span>
                                <span class="text-muted left-fa">{{ $product->price }}</span>
                              </div>
                              <div class="table-cell text-left">
                                <form action="{{ asset('cart/'.$product->rowId) }}" method="post">
                                  {{ csrf_field() }}
                                  {{ method_field("DELETE") }}
                                  <button class="bg-transparent text-red"><i class="icofont icofont-close-circled"></i></button>
                                </form>
                              </div>
                            </div>
                          @endif
                        @endforeach

                        @if ($total > 0)
                          <div class="clearfix m-b-15">
                            <div class="col-xs-6 p-0 m-t-5">
                              @if (isset($branch))
                                <p>@lang('translate.delivery') <span class="text-muted left-fa">{{ $branch->delivery }}</span></p>
                              @endif
                            </div>
                            <div class="col-xs-6 p-0">
                              <div class="btn btn-sm clearfix btn-block btn-border bg-white text-center">
                                <div class="col-xs-6 p-0 border-left">@lang('translate.sum')</div>
                                <div class="col-xs-6 p-0">{{ $total }}</div>
                              </div>
                            </div>
                          </div>
                          <div class="text-center">
                            <a href="{{ asset('checkout/delivery/'.$id) }}" class="btn btn-red btn-br-20 btn-block">@lang('translate.delivery') </a>
                            <a id="checkout_prepare" href="#" class="btn btn-red btn-br-20 btn-block pop-it-up">@lang('translate.prepare')</a>
                            <div class="notes-txtarea">
                            <form method="post" action="{{ asset('order') }}">
                              {{ csrf_field() }}
                              <input type="hidden" name="branch_id" value="{{ $branch->id }}">
                              <input type="hidden" name="total" value="{{ $total }}">
                              <input type="hidden" name="type" value="prepare">
                              <textarea placeholder="@lang('translate.add_notes')" name="notes"></textarea>
                              <button type="submit" class="btn btn-red btn-br-20 btn-block">@lang('translate.send') </button>
                            </form>
                            </div>
                          </div>
                        @else
                          <div class="empty-cart text-center">
                            <div class="cart-icon">
                              <img src="{{ asset('site/images/cart-icon.png') }}" alt="cart">
                            </div>

                            <h3 class="f-s-20">@lang('translate.empty_cart')</h3>
                          </div>
                        @endif
                      @else
                        <div class="empty-cart text-center">
                          <div class="cart-icon">
                            <img src="{{ asset('site/images/cart-icon.png') }}" alt="cart">
                          </div>
                          <h3 class="f-s-20">@lang('translate.empty_cart')</h3>
                        </div>
                      @endif
                    </div>
                  </div>
                </div>
              @endif

          </div>
      </div>
  </div>

  @if (session()->has('tab'))
    <?php $tab = session()->get('tab');?>
    <script>
    $(function(){
      $('li.{{ $tab }}').addClass('active');
    });
    </script>
@endif
@endsection
