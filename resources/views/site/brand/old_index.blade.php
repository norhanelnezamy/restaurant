@extends('layout')
@section('content')

<script src="{{asset('public/site/js/filter.js')}}"></script>

  <div class="div-padding bg-gray p-b-80">
      <div class="container">
          <div class="row">
              <div class="col-xs-12 col-sm-6 col-sm-pull-3 text-center main-header">
                  <h1 class="text-red animate-box">@lang('translate.'.$type)</h1>
              </div>
          </div>
          <div class="row">
            <?php
             $countries = country();
             $categories = category();
             ?>
              <div class="col-xs-12 col-sm-3 search-box">
                  <a class="btn-red open-search-btn"><i class="icofont icofont-search"></i></a>
                  <div class="search-form">
                      <div class="panel gray panel-br0">
                          <div class="panel-body text-muted text-center din-font">ابحث عن مطعمك</div>
                      </div>
                      <form action="{{ asset('brand/filter/restaurant') }}" method="post">
                        {{ csrf_field() }}
                          <div class="panel gray panel-br0">
                              <div class="panel-body">
                                  <select class="full-border" id="country">
                                    <option>@lang('translate.country')</option>
                                    @if (App::isLocale('en'))
                                      @foreach ($countries as $key => $country)
                                        <option value="{{ $country->id }}">{{ $country->en_name }}</option>
                                      @endforeach
                                    @else
                                      @foreach ($countries as $key => $country)
                                        <option value="{{ $country->id }}">{{ $country->ar_name }}</option>
                                      @endforeach
                                    @endif
                                  </select>
                                  <select class="full-border" id="city">
                                      <option id="city_child">@lang('translate.city')</option>
                                  </select>
                                  <select class="full-border" id="area" name="area_id">
                                      <option id="area_child">@lang('translate.district')</option>
                                  </select>
                              </div>
                          </div>

                          <div class="panel gray panel-br0">
                              <div class="panel-body">
                                  <select class="full-border" name="category_id">
                                    <option>@lang('translate.rest_type')</option>
                                    @if (App::isLocale('en'))
                                      @foreach ($categories as $key => $category)
                                        <option value="{{ $category->id }}">{{$category->en_name}}</option>
                                      @endforeach
                                    @else
                                      @foreach ($categories as $key => $category)
                                        <option value="{{ $category->id }}">{{$category->ar_name}}</option>
                                      @endforeach
                                    @endif
                                  </select>
                              </div>
                          </div>
                          <div class="panel gray panel-br0">
                              <div class="panel-body">
                                <button class="btn-red" style="width:100%;height:35px">بحث</button>
                              </div>
                          </div>
                      </form>

                  </div>
              </div>

              <div class="col-xs-12 col-sm-9 search-data">
                  <div class="row">
                    @foreach ($brands as $key => $brand)
                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 animate-box">
                        <div class="panel offers-panel text-center">
                          <div class="panel-body offer-bg">
                            <img src="{{ asset('public/uploads/'.$brand->logo) }}">
                          </div>

                          <div class="panel-body p-5">
                            <a href="#" class="btn btn-white text-red btn-border basket-btn" >
                              <i class="icofont icofont-globe-alt"></i>
                            </a>
                            <h1>
                              @if (App::isLocale('en'))
                                {{ $brand->en_name }}
                              @else
                                {{ $brand->ar_name }}
                              @endif
                            </h1>
                            <p class="text-muted din-font">
                              @if (App::isLocale('en'))
                                {{ $brand->category_en_name }}
                              @else
                                {{ $brand->category_ar_name }}
                              @endif
                            </p>
                          </div>

                          <div class="p-5 clearfix text-center din-font">
                            <div class="col-xs-6 p-0 border-left">
                              <div class="btn btn-block">
                                <?php
                                $feedback_food=rate('food', $brand->id);
                                $feedback_service=rate('services', $brand->id);
                                $feedback_delivery=rate('delivery', $brand->id);
                                if ($feedback_food==0&&$feedback_service==0&&$feedback_delivery==0) {
                                  $total_average = 0;
                                }
                                else {
                                  $total_average = ($feedback_food+$feedback_service+$feedback_delivery)/3;
                                }
                                ?>
                                @for ($i=1; $i <= $total_average ; $i++)
                                  <i class="icofont icofont-star text-yellow"></i>
                                @endfor
                                @for ($i=5; $i > $total_average ; $i--)
                                  <i class="icofont icofont-star text-muted"></i>
                                @endfor
                              </div>
                            </div>
                            <div class="col-xs-6 p-0">
                              <a href="{{ asset('/brand/show/'.$brand->id) }}" class="btn btn-block text-black" >@lang('translate.more')</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforeach
                  </div>

                  <ul class="pagination din-font">
                    {{ $brands->links() }}
                  </ul>

              </div>

          </div>

      </div>
  </div>
@endsection
