@extends('site.brand.show')
@section('about')
  <div role="tabpanel" class="tab-pane fade active in" id="info">
    <h4>@lang('translate.about_brand')</h4>
    <p class="text-muted din-font">
      @if (App::isLocale('en'))
        {{ $brand->en_details }}
      @else
        {{ $brand->ar_details }}
      @endif
    </p>
    {{-- <div class="panel panel-br0 light-gray m-t-25">
      <div class="panel-body">
        <div class="row">
          <div class="col-xs-6 col-sm-4 col-lg-3 m-b-5"> <img src="images/menu-1.png" class="img-responsive center-block"> </div>
          <div class="col-xs-6 col-sm-4 col-lg-3 m-b-5"> <img src="images/menu-2.png" class="img-responsive center-block"> </div>
          <div class="col-xs-6 col-sm-4 col-lg-3 m-b-5"> <img src="images/menu-3.png" class="img-responsive center-block"> </div>
          <div class="col-xs-6 col-sm-4 col-lg-3 m-b-5"> <img src="images/menu-4.png" class="img-responsive center-block"> </div>
        </div>
      </div>
    </div> --}}
    <div class="panel panel-br0 light-gray">
      <div class="panel-body">
        <div class="clearfix">
          <div class="col-xs-6 p-0">@lang('translate.min_charge')</div>
          <div class="col-xs-6 p-0">{{ $brand->min_charge }}</div>
        </div>
      </div>
      <hr class="m-0">
      <div class="panel-body">
        <div class="clearfix">
          <div class="col-xs-6 p-0">@lang('translate.work_time')</div>
          <div class="col-xs-6 p-0">
            <?php //$time = explode(':', $brand->work_time);?>
            <span class="label bg-white btn-border btn-sm">{{ $brand->work_time }}</span>
            {{-- <span class="label bg-white btn-border btn-sm">{{ $time[0] }}</span> --}}
            {{-- <span class="left-fa right-fa text-muted">@lang('translate.to')</span>
            <span class="label bg-white btn-border btn-sm">{{ $time[1] }}</span> --}}
          </div>
        </div>
      </div>
      <hr class="m-0">
      <div class="panel-body">
        <div class="clearfix">
          <div class="col-xs-6 p-0">@lang('translate.brand_type')</div>
          <div class="col-xs-6 p-0">
            @if (App::isLocale('en'))
              {{ $brand->category_en_name }}
            @else
              {{ $brand->category_ar_name }}
            @endif
          </div>
        </div>
      </div>
      <hr class="m-0">
      @if (isset($brand->address))
        <div class="panel-body">
          <div class="clearfix">
            <div class="col-xs-6 p-0">@lang('translate.address')</div>
            <div class="col-xs-6 p-0">{{ $brand->address }}</div>
          </div>
        </div>
      @endif
    </div>
    <div class="panel panel-br0 light-gray">
      <div class="panel-body">
        <div class="table-view text-center item-b-left">
          <div class="table-cell block"><i class="icofont icofont-social-google-map right-fa"></i>@lang('translate.reservation')</div>
          <div class="table-cell block"><i class="icofont icofont-money-bag right-fa"></i>@lang('translate.prepare')</div>
          <div class="table-cell block"><i class="icofont icofont-motor-bike right-fa"></i>@lang('translate.delivery')</div>
        </div>
      </div>
    </div>
  </div>

@endsection
