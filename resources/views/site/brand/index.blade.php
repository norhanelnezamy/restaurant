@extends('layout')
@section('content')

  <!--===============================
      OFFERS SECTION
  ===================================-->

  <div class="div-padding">
      <div class="container">

          <div class="row">

              <div class="col-xs-12 col-sm-6 col-sm-pull-3 text-center main-header">
                  <h1 class="text-red animate-box">@lang('translate.'.$type)</h1>
              </div>

              <div class="col-xs-6 col-sm-3 col-sm-push-6"></div>

              <div class="col-xs-6 col-sm-3 text-left">
                  <button class="owl-btn btn-white text-red btn-border offers-first-prev"><i class="icofont icofont-arrow-right"></i></button>
                  <button class="owl-btn btn-white text-red btn-border offers-first-next"><i class="icofont icofont-arrow-left"></i></button>
              </div>

          </div>


          <div class="animate-box">
              <div class="owl-carousel owl-offers m-t-25" id="first">
                  @foreach ($brands as $key => $brand)
                    <div class="item">
                        <div class="panel offers-panel text-center">
                            <div class="panel-body offer-bg">
                                <img src="{{ asset('public/uploads/'.$brand->logo) }}">
                            </div>
                            <div class="panel-body p-5">
                                <a href="{{ asset('/brand/show/'.$brand->id) }}" class="btn btn-white text-red btn-border basket-btn" title="اضافة للمفضلة">
                                    <i class="icofont icofont-basket"></i>
                                </a>
                                <h1>
                                  @if (App::isLocale('en'))
                                    {{ $brand->en_name }}
                                  @else
                                    {{ $brand->ar_name }}
                                  @endif
                                </h1>
                                <p class="text-muted din-font">
                                  @if (App::isLocale('en'))
                                    {{ $brand->category_en_name }}
                                  @else
                                    {{ $brand->category_ar_name }}
                                  @endif
                                </p>
                            </div>

                            <div class="p-5 clearfix text-center din-font">
                                <div class="col-xs-6 p-0 border-left">
                                    <div class="btn btn-block">
                                      <?php
                                      $feedback_food=rate('food', $brand->id);
                                      $feedback_service=rate('services', $brand->id);
                                      $feedback_delivery=rate('delivery', $brand->id);
                                      if ($feedback_food==0&&$feedback_service==0&&$feedback_delivery==0) {
                                        $total_average = 0;
                                      }
                                      else {
                                        $total_average = ($feedback_food+$feedback_service+$feedback_delivery)/3;
                                      }
                                      ?>
                                      @for ($i=1; $i <= $total_average ; $i++)
                                        <i class="icofont icofont-star text-yellow"></i>
                                      @endfor
                                      @for ($i=5; $i > $total_average ; $i--)
                                        <i class="icofont icofont-star text-muted"></i>
                                      @endfor
                                    </div>
                                </div>
                                <div class="col-xs-6 p-0">
                                    <a href="{{ asset('/brand/show/'.$brand->id) }}" class="btn btn-block text-black" >@lang('translate.more')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                  @endforeach
              </div>
          </div>


          <div class="text-center animate-box">
            <ul class="pagination din-font">
              {{ $brands->links() }}
            </ul>
          </div>

      </div>
  </div>



  <!--===============================
      TESTIMONIAL SECTION
  ===================================-->

  <div class="div-padding bg-gray p-b-80">
      <div class="container">
          <div class="row">

              <div class="col-xs-12 col-sm-6 col-sm-pull-3 text-center main-header">
                  <h1 class="text-red animate-box">@lang('translate.galary')</h1>
              </div>

          </div>
          <div class="row">

              <div class="col-xs-12 col-sm-6 col-md-4 animate-box">
                  <a class="image-holder half-height" data-fancybox="group" href="{{asset('public/uploads/'.$photos[0]['photo'])}}">
                      <img src="{{asset('public/uploads/'.$photos[0]['photo'])}}">
                      <div class="overlay"><i class="icofont icofont-resize"></i></div>
                  </a>

                  <a class="image-holder half-height" data-fancybox="group" href="{{asset('public/uploads/'.$photos[1]['photo'])}}">
                      <img src="{{asset('public/uploads/'.$photos[1]['photo'])}}">
                      <div class="overlay"><i class="icofont icofont-resize"></i></div>
                  </a>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 animate-box">
                  <a class="image-holder full-height" data-fancybox="group" href="{{asset('public/uploads/'.$photos[2]['photo'])}}">
                      <img src="{{asset('public/uploads/'.$photos[2]['photo'])}}">
                      <div class="overlay"><i class="icofont icofont-resize"></i></div>
                  </a>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 animate-box">
                  <a class="image-holder half-height" data-fancybox="group" href="{{asset('public/uploads/'.$photos[3]['photo'])}}">
                      <img src="{{asset('public/uploads/'.$photos[3]['photo'])}}">
                      <div class="overlay"><i class="icofont icofont-resize"></i></div>
                  </a>

                  <a class="image-holder half-height" data-fancybox="group" href="{{asset('public/uploads/'.$photos[4]['photo'])}}">
                      <img src="{{asset('public/uploads/'.$photos[4]['photo'])}}">
                      <div class="overlay"><i class="icofont icofont-resize"></i></div>
                  </a>
              </div>

          </div>

      </div>
  </div>
@endsection
