@extends('site.brand.show')
@section('checkout')
  <div id="map" class="big-map"></div>
  @if ($errors->has('latLng'))
  <p style="color:red">{{ $errors->first('latLng') }}</p>
  @endif
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYhk7qaAlI-HzYkvA0G5Asw58Ev8VpWqo&callback=initMap" async defer></script>
  <script>
  var map;
  var markersArray = [];
  function initMap(){
    var saudi = new google.maps.LatLng(23.8859, 45.0792);
    var myOptions = {
        zoom: 6,
        center: saudi,
    };
    map = new google.maps.Map(document.getElementById("map"), myOptions);
    var marker = new google.maps.Marker({map: map, position: saudi});
    // add a click event handler to the map object
    google.maps.event.addListener(map, "click", function(event){
    marker.setMap(null);
      // place a marker
      placeMarker(event.latLng);
      // display the lat/lng in your form's lat/lng fields
      console.log(event.latLng.lat() + '-'+ event.latLng.lng());
      $('#latLng').val(event.latLng.lat() + '-'+ event.latLng.lng());
      // document.getElementById("latLng").value =  ;
    });
  }
  function placeMarker(location){
    // first remove all markers if there are any
    deleteOverlays();
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
    // add marker in markers array
    markersArray.push(marker);
    //map.setCenter(location);
  }
  // Deletes all markers in the array by removing references to them
  function deleteOverlays(){
    if (markersArray) {
        for (i in markersArray) {
            markersArray[i].setMap(null);
        }
    markersArray.length = 0;
    }
  }
  </script>

  <form method="post"  action="{{ asset('order') }}">
    {{ csrf_field() }}
    <input type="hidden" name="branch_id" value="{{ $branch->id }}">
    <input type="hidden" name="latLng" id="latLng" value="">
    <input type="hidden" name="type" value="delivery">
    <input type="hidden" name="brand_id" value="{{ $id }}">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <label>@lang('translate.floor')</label>
            <input type="text" class="full-border btn-br-20 bg-gray" placeholder="@lang('translate.floor')" name="floor">
            @if ($errors->has('floor'))
            <p style="color:red">{{ $errors->first('floor') }}</p>
            @endif
        </div>

        <div class="col-xs-12 col-sm-6">
            <label>@lang('translate.flat_number')</label>
            <input type="text" class="full-border btn-br-20 bg-gray" placeholder="@lang('translate.flat_number')"  name="flat_number">
            @if ($errors->has('flat_number'))
            <p style="color:red">{{ $errors->first('flat_number') }}</p>
            @endif
        </div>
    </div>

    <label>@lang('translate.details')</label>
    <textarea class="full-border btn-br-20 bg-gray" placeholder="@lang('translate.details')"  name="details"></textarea>

    @if ($errors->has('details'))
    <p style="color:red">{{ $errors->first('details') }}</p>
    @endif

    <div class="div-padding bg-gray">
      <div class="container">

        <div class="panel">
          <div class="panel-body p-5">
            <?php $total=0; ?>
            @foreach (Cart::content() as $key => $product)
              @if ($product->options->branch_id == $branch->id)
                <?php $total += $product->price*$product->qty?>
                <div class="table-view p-5">
                  <div class="table-cell">{{ $product->qty }}</div>
                  <div class="table-cell">{{ $product->name }}</div>
                  {{-- <div class="table-cell">
                  <div class="btn btn-border full-width bg-gray">{{$product->}}</div>
                </div> --}}
                <div class="table-cell text-left">
                  <div class="btn btn-border bg-gray">{{ $product->price }}</div>
                </div>
              </div>
            @endif
          @endforeach


        </div>
      </div>

      <label>@lang('translate.notes')</label>
      <textarea class="full-border btn-br-20" placeholder="@lang('translate.notes')"  name="notes"></textarea>
      @if ($errors->has('notes'))
        <p style="color:red">{{ $errors->first('notes') }}</p>
      @endif
    </div>
  </div>


  <div class="div-padding p-b-80">
      <div class="container">
          <div class="table-view text-center">
              <div class="table-cell block">@lang('translate.delivery')
                  <span class="btn btn-border bg-gray left-fa">{{ $branch->delivery }}</span>
              </div>
              <div class="table-cell block">@lang('translate.sum')
                  <span class="btn btn-red left-fa">{{ $total+$branch->delivery }}</span>
              </div>
          </div>
          <input type="hidden" name="total" value="{{ $total+$branch->delivery }}">
          <div class="panel-body bg-gray m-t-25 m-b-15 btn-br-20">
              <div class="text-left">
                  <a href="{{ asset('cart/destroy/branch/'.$branch->id) }}" class="btn btn-white btn-border border-red">الغاء</a>
                  <button type="submit" class="btn btn-red" >@lang('translate.submit_order')</button>
              </div>
          </div>
      </div>
  </div>

</form>

@endsection
