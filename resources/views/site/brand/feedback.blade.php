@extends('site.brand.show')
@section('feedback')
   @foreach ($feedbacks as $key => $feedback)
     <div class="panel panel-br0 light-gray din-font">
       <div class="panel-body">
         <div class="table-view">
           <div class="table-cell"><h1 class="f-s-20 m-t-0">{{ $feedback->name }}</h1></div>
           <div class="table-cell text-muted text-left">{{ $feedback->created_at }}</div>
         </div>
         <p class="text-muted">{{ $feedback->content }}</p>
         <div class="row text-align-right">
           <div class="col-xs-12 col-sm-4">
             <span class="label label-default bg-white">@lang('translate.delivery')</span>
             <br>
             @for ($i=1; $i <= $feedback->delivery ; $i++)
               <i class="icofont icofont-star text-yellow"></i>
             @endfor
             @for ($i=5; $i > $feedback->delivery ; $i--)
               <i class="icofont icofont-star text-muted"></i>
             @endfor
           </div>
           <div class="col-xs-12 col-sm-4">
             <span class="label label-default bg-white">@lang('translate.services')</span>
             <br>
             @for ($i=1; $i <= $feedback->services ; $i++)
               <i class="icofont icofont-star text-yellow"></i>
             @endfor
             @for ($i=5; $i > $feedback->services ; $i--)
               <i class="icofont icofont-star text-muted"></i>
             @endfor
           </div>
           <div class="col-xs-12 col-sm-4">
             <span class="label label-default bg-white">@lang('translate.food_feedback')</span>
             <br>
             @for ($i=1; $i <= $feedback->food ; $i++)
               <i class="icofont icofont-star text-yellow"></i>
             @endfor
             @for ($i=5; $i > $feedback->food ; $i--)
               <i class="icofont icofont-star text-muted"></i>
             @endfor
           </div>
         </div>
       </div>
     </div>
   @endforeach

   <ul class="pagination din-font">
     {{  $feedbacks->links()}}
  </ul>

  @if (Auth::guard('web')->check())
    <div class="text-center"><button class="btn btn-red btn-br-20" data-toggle="modal" data-target="#rateModal">اضف تقييمك</button></div>
  @else
    <?php session()->put('page', '/brand/feedback/'.$id)?>
    <div class="text-center"><a href="{{ asset('/login') }}" class="btn btn-red btn-br-20">اضف تقييمك</a></div>
  @endif


  <!--===============================
  RATE MODAL
  ===================================-->

  <div class="modal fade" id="rateModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content bg-gray">
        <div class="modal-header">
          <div class="modal-title">@lang('translate.add_feedback')</div>
        </div>

        <form action="{{ asset('brand/feedback/'.$id) }}" method="post" class="modal-body p-0 din-font">
          {{ csrf_field() }}
          <div class="panel-body">
            <div class="table-view text-center">

              <div class="table-cell block">
                <p>@lang('translate.delivery')</p>

                <div class="rating-holder">
                  <input type="radio" id="rate-1-5" name="delivery" value="5">
                  <label for="rate-1-5"><i class="icofont icofont-star"></i></label>
                  <input type="radio" id="rate-1-4" name="delivery" value="4">
                  <label for="rate-1-4"><i class="icofont icofont-star"></i></label>
                  <input type="radio" id="rate-1-3" name="delivery" value="3">
                  <label for="rate-1-3"><i class="icofont icofont-star"></i></label>
                  <input type="radio" id="rate-1-2" name="delivery" value="2">
                  <label for="rate-1-2"><i class="icofont icofont-star"></i></label>
                  <input type="radio" id="rate-1-1" name="delivery" value="1">
                  <label for="rate-1-1"><i class="icofont icofont-star"></i></label>
                </div>
                @if ($errors->has('delivery'))
                <p style="color:red">{{ $errors->first('delivery') }}</p>
                @endif
              </div>

              <div class="table-cell block">
                <p>@lang('translate.services')</p>

                <div class="rating-holder">
                  <input type="radio" id="rate-2-5" name="services" value="5">
                  <label for="rate-2-5"><i class="icofont icofont-star"></i></label>
                  <input type="radio" id="rate-2-4" name="services" value="4">
                  <label for="rate-2-4"><i class="icofont icofont-star"></i></label>
                  <input type="radio" id="rate-2-3" name="services" value="3">
                  <label for="rate-2-3"><i class="icofont icofont-star"></i></label>
                  <input type="radio" id="rate-2-2" name="services" value="2">
                  <label for="rate-2-2"><i class="icofont icofont-star"></i></label>
                  <input type="radio" id="rate-2-1" name="services" value="1">
                  <label for="rate-2-1"><i class="icofont icofont-star"></i></label>
                </div>
                @if ($errors->has('services'))
                <p style="color:red">{{ $errors->first('services') }}</p>
                @endif
              </div>

              <div class="table-cell block">
                <p>@lang('translate.food_feedback')</p>

                <div class="rating-holder">
                  <input type="radio" id="rate-3-5" name="food" value="5">
                  <label for="rate-3-5"><i class="icofont icofont-star"></i></label>
                  <input type="radio" id="rate-3-4" name="food" value="4">
                  <label for="rate-3-4"><i class="icofont icofont-star"></i></label>
                  <input type="radio" id="rate-3-3" name="food" value="3">
                  <label for="rate-3-3"><i class="icofont icofont-star"></i></label>
                  <input type="radio" id="rate-3-2" name="food" value="2">
                  <label for="rate-3-2"><i class="icofont icofont-star"></i></label>
                  <input type="radio" id="rate-3-1" name="food" value="1">
                  <label for="rate-3-1"><i class="icofont icofont-star"></i></label>
                </div>
                @if ($errors->has('food'))
                <p style="color:red">{{ $errors->first('food') }}</p>
                @endif
              </div>

            </div>
          </div>

          <hr class="m-0">

          <div class="panel-body">
            <p class="text-muted">@lang('translate.feedback_notes')</p>
            <div class="boxed-form bg-white">
              <textarea placeholder="@lang('translate.write_comment')" name="content"></textarea>
              <div class="text-left"><button type="submit" class="btn btn-red">@lang('translate.send')</button></div>
            </div>
            @if ($errors->has('content'))
            <p style="color:red">{{ $errors->first('content') }}</p>
            @endif
          </div>

        </form>
      </div>
    </div>
  </div>
@endsection
