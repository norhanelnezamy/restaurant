@extends('site.brand.show')
@section('menu')
  <script src="{{asset('public/site/js/filter.js')}}"></script>
  <div>
    <?php $countries = country();?>
    <div class="row m-t-25">
      <div class="col-xs-12 col-sm-4">
        <select class="full-border" id="country">
          <option>@lang('translate.country')</option>
          @if (App::isLocale('en'))
            @foreach ($countries as $key => $country)
              <option value="{{ $country->id }}">{{ $country->en_name }}</option>
            @endforeach
          @else
            @foreach ($countries as $key => $country)
              <option value="{{ $country->id }}">{{ $country->ar_name }}</option>
            @endforeach
          @endif
        </select>
      </div>

      <div class="col-xs-12 col-sm-4">
        <select class="full-border" id="city">
          <option id="city_select">@lang('translate.city')</option>
        </select>
      </div>

      <div class="col-xs-12 col-sm-4">
        <select class="full-border" id="area" name="area">
          <option id="area_select">@lang('translate.district')</option>
        </select>
      </div>
    </div>

    @if (session()->has('area'))
      <h3 class="text-center m-t-25">@lang('translate.choose_order')</h3>
      <div class="row">
        <div class="col-xs-12 col-md-8 col-md-pull-2">
          <ul class="nav nav-justified inside-tabs" role="tablist">
            <li role="presentation" class="active reserve border-left"><a href="#takeAway" aria-controls="takeAway" role="tab" data-toggle="tab">@lang('translate.reservation')</a></li>
            <li role="presentation" class="order"><a href="#makeOrder" aria-controls="makeOrder" role="tab" data-toggle="tab">@lang('translate.order')</a></li>

          </ul>

        </div>
      </div>

      <div class="tab-content m-t-25">

        <div role="tabpanel" class="tab-pane fade active in" id="takeAway">
          @if (isset($branch))
            <form action="{{ asset('brand/'.$id.'/branch/'.$branch->id.'/reservation') }}" method="post">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <input type="number" placeholder="@lang('translate.numder')" class="full-border btn-br-20 bg-gray" name="numder" >
                  @if ($errors->has('numder'))
                  <p style="color:red">{{ $errors->first('numder') }}</p>
                  @endif
                </div>
                <div class="col-xs-12 col-sm-6">
                  <input type="date" placeholder="@lang('translate.date')" class="full-border btn-br-20 bg-gray" name="date">
                  @if ($errors->has('date'))
                  <p style="color:red">{{ $errors->first('date') }}</p>
                  @endif
                </div>
                <div class="col-xs-12 col-sm-6">
                  <input type="time" placeholder="@lang('translate.time')" class="full-border btn-br-20 bg-gray" name="time">
                  @if ($errors->has('time'))
                  <p style="color:red">{{ $errors->first('time') }}</p>
                  @endif
                </div>
                <div class="col-xs-12 col-sm-6">
                  <input type="text" placeholder="@lang('translate.occasion')" class="full-border btn-br-20 bg-gray" name="occasion">
                  @if ($errors->has('occasion'))
                  <p style="color:red">{{ $errors->first('occasion') }}</p>
                  @endif
                </div>
                <div class="col-xs-12 col-sm-6">
                  <input type="tel" placeholder="@lang('translate.other_phone')" class="full-border btn-br-20 bg-gray" name="other_phone">
                  @if ($errors->has('other_phone'))
                  <p style="color:red">{{ $errors->first('other_phone') }}</p>
                  @endif
                </div>
                <div class="col-xs-12 col-sm-6">
                  <button type="submit" class="btn btn-block btn-red btn-br-20">@lang('translate.reservation_submit')</button>
                </div>
              </div>
            </form>
          @endif
        </div>


        <div role="tabpanel" class="tab-pane fade" id="makeOrder">







      <div class="row m-t-25">

        <div class="col-xs-12 col-md-4">
          <form action="#">
            <div class="search-with-btn">
              <input type="search" placeholder="@lang('translate.search_categories')" class="din-font full-border">
              <button type="submit" class="search-btn"><i class="icofont icofont-search"></i></button>
            </div>
          </form>


          <div class="panel panel-br0 light-gray">
            <div class="panel-heading gray">@lang('translate.menu_catgory')</div>
            <div class="panel-body p-0">
              <ul class="list-unstyled menu-list m-0">
                @if (isset($menus))
                  <li><a href="{{ asset('branch/offer') }}">@lang('translate.Offers_')</a></li>
                  @if (App::isLocale('en'))
                    @foreach ($menus as $key => $menu)
                      <li class="{{$menu->id}}"> <a href="{{ asset('branch/'.$id.'/menu/'.$menu->id.'/food') }}">{{ $menu->en_name }}</a></li>
                    @endforeach
                  @else
                    @foreach ($menus as $key => $menu)
                      <li class="{{$menu->id}}"><a href="{{ asset('branch/'.$id.'/menu/'.$menu->id.'/food') }}">{{ $menu->ar_name }}</a></li>
                    @endforeach
                  @endif
                @endif

                @if (session()->has('menu'))
                  <?php $menu = session()->get('menu');?>
                  <script>
                  $(function(){
                    $('li.{{ $menu }}').addClass('active');
                  });
                  </script>
                @endif

              </ul>
            </div>
          </div>

          @if (isset($photos) && sizeof($photos)>0)
            <a class="btn btn-block btn-red btn-br-20" href="{{asset('public/uploads/'.$photos[0]['photo'])}}" data-fancybox="group">@lang('translate.restaurant_photo')</a>
            <div class="hidden">
              @foreach ($photos as $key => $photo)
                <a href="{{asset('public/uploads/'.$photo->photo)}}" data-fancybox="group"></a>
              @endforeach
            </div>
          @endif

        </div>


        <div class="col-xs-12 col-md-8">
          <div class="panel panel-br0 light-gray">
            <div class="panel-body p-5">@lang('translate.super_food')</div>
          </div>

          @if (isset($foods))
            @foreach ($foods as $key => $food)
              <div class="table-view">
                <div class="table-cell block order-img-holder">
                  <div class="order-img">
                    <img src="{{ asset('public/uploads/'.$food->photo) }}">
                  </div>
                </div>
                <div class="table-cell block">
                  <div class="table-view">
                    <div class="table-cell v-top">
                      <h3 class="f-s-20 m-t-0">
                        <span>
                          @if (App::isLocale('en'))
                            {{ $food->en_name }}
                          @else
                            {{ $food->ar_name }}
                          @endif
                        </span>
                      </h3>
                    </div>
                    <div class="table-cell text-left v-top">
                      <a data-toggle="modal" data-target="#{{ 'food_'.$food->id }}" class="text-red left-fa"><i class="icofont icofont-plus-circle"></i></a>
                    </div>
                  </div>
                  <p class="text-muted din-font">
                    @if (App::isLocale('en'))
                      {{ $food->en_additional }}
                    @else
                      {{ $food->ar_additional }}
                    @endif
                  </p>
                </div>

              </div>

              <hr class="m-t-0">

              <!--===============================
              ADDITIONAL MODAL
              ===================================-->
              <input type="hidden" value="{{ $model }}" name="model">

              <div class="modal fade" id="{{ 'food_'.$food->id }}" tabindex="-1" role="dialog">
                <div class="to-right- modal-dialog" role="document" style="right:-65px">
                  <div class="modal-content">
                    <div class="modal-header">
                      <div class="table-view">
                        <div class="table-cell">
                          <div class="modal-title text-red" >
                            @if (App::isLocale('en'))
                              {{ $food->en_name }}
                            @else
                              {{ $food->ar_name }}
                            @endif
                          </div>
                        </div>
                        <div class="table-cell text-left"><button class="bg-transparent" data-dismiss="modal"><i class="icofont icofont-close-circled"></i></button></div>
                      </div>
                    </div>

                    <div class="modal-body">

                      <div class="table-view">
                        <div class="table-cell block">
                          <div class="order-img">
                            <img src="{{ asset('public/uploads/'.$food->photo) }}">
                          </div>
                        </div>

                        <div class="table-cell block">
                          <h3 class="f-s-20 m-t-0">
                            <span>
                              @if (App::isLocale('en'))
                                {{ $food->en_name }}
                              @else
                                {{ $food->ar_name }}
                              @endif
                            </span>
                          </h3>
                          <p class="text-muted din-font">
                            @if (App::isLocale('en'))
                              {{ $food->en_additional }}
                            @else
                              {{ $food->ar_additional }}
                            @endif
                          </p>
                        </div>
                      </div>

                      <form method="post" action="{{ asset('/cart') }}">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ $model }}" name="model">
                        <input type="hidden" value="{{ $food->id }}" name="food_id">
                        <input type="hidden" value="{{ $branch->id }}" name="branch_id">
                        @if (App::isLocale('en'))
                          <input type="hidden" value="{{ $food->en_name }}" name="food_name">
                        @else
                          <input type="hidden" value="{{ $food->ar_name }}" name="food_name">
                        @endif
                        <div class="clearfix">
                          <div class="col-xs-12 col-sm-9 p-0">
                            @if (isset($food->size))
                              <h5 class="m-t-0">
                                <small class="icofont icofont-spinner-alt-4 text-red right-fa"></small> اختر حجم الوجبة <small class="text-muted left-fa">@lang('translate.multiple_additonal')</small>
                              </h5>
                              <?php $size = json_decode($food->size, true);?>
                              @foreach ($size as $key => $one)
                                <input type="hidden" name="price[{{ $key }}]" value="{{$one}}">
                                <label class="radio-holder">
                                  <input type="radio" name="size" value="{{$key}}">
                                  <span class="radio-icon"><i class="icofont icofont-check"></i></span>
                                  <span>@lang('translate.'.$key) <i class="left-fa right-fa">-</i> <span class="text-red">{{$one}}</span></span>
                                </label>
                              @endforeach
                            @else
                              <input type="hidden" name="price[one_size]" value="{{$food->price}}">
                              <input type="hidden" name="size" value="one_size">
                            @endif
                            <?php
                            $drink=$salad=$appetizer=$other=array();
                            foreach ($additionals as $key => $additional){
                              if ($additional->type == 'drink') {
                                array_push($drink, $additional);
                              }
                              if ($additional->type == 'salad') {
                                array_push($salad, $additional);
                              }
                              if ($additional->type == 'appetizer') {
                                array_push($appetizer, $additional);
                              }
                              if ($additional->type == 'other') {
                                array_push($other, $additional);
                              }
                            }
                            ?>

                            @if (sizeof($drink)>0)
                              <h5 class="m-t-25">
                                <small class="icofont icofont-spinner-alt-4 text-red right-fa"></small> @lang('translate.drink')<small class="text-muted left-fa">@lang('translate.multiple_additonal')</small>
                              </h5>
                              @foreach ($drink as $key => $one)
                                <label class="checkbox-holder">
                                  <input type="checkbox" name="additional[]" value="{{ $one->id }}">
                                  <span class="checkbox-icon"><i class="icofont icofont-check"></i></span>
                                  <span>
                                    @if (App::isLocale('en'))
                                      {{ $one->en_name}} <i class="left-fa right-fa">-</i> <span class="text-red">{{ $one->price }}</span>
                                    @else
                                      {{ $one->ar_name }} <i class="right-fa left-fa">-</i> <span class="text-red">{{ $one->price }}</span>
                                    @endif
                                  </span>
                                </label>
                              @endforeach
                            @endif

                            @if (sizeof($salad)>0)
                              <h5 class="m-t-25">
                                <small class="icofont icofont-spinner-alt-4 text-red right-fa"></small> @lang('translate.salad')<small class="text-muted left-fa">@lang('translate.multiple_additonal')</small>
                              </h5>
                              @foreach ($salad as $key => $one)
                                <label class="checkbox-holder">
                                  <input type="checkbox" name="additional[]" value="{{ $one->id }}">
                                  <span class="checkbox-icon"><i class="icofont icofont-check"></i></span>
                                  <span>
                                    @if (App::isLocale('en'))
                                      {{ $one->en_name}} <i class="left-fa right-fa">-</i> <span class="text-red">{{ $one->price }}</span>
                                    @else
                                      {{ $one->ar_name }} <i class="right-fa left-fa">-</i> <span class="text-red">{{ $one->price }}</span>
                                    @endif
                                  </span>
                                </label>
                              @endforeach
                            @endif

                            @if (sizeof($appetizer)>0)
                              <h5 class="m-t-25">
                                <small class="icofont icofont-spinner-alt-4 text-red right-fa"></small> @lang('translate.appetizer')<small class="text-muted left-fa">@lang('translate.multiple_additonal')</small>
                              </h5>
                              @foreach ($appetizer as $key => $one)
                                <label class="checkbox-holder">
                                  <input type="checkbox" name="additional[]" value="{{ $one->id }}">
                                  <span class="checkbox-icon"><i class="icofont icofont-check"></i></span>
                                  <span>
                                    @if (App::isLocale('en'))
                                      {{ $one->en_name}} <i class="left-fa right-fa">-</i> <span class="text-red">{{ $one->price }}</span>
                                    @else
                                      {{ $one->ar_name }} <i class="right-fa left-fa">-</i> <span class="text-red">{{ $one->price }}</span>
                                    @endif
                                  </span>
                                </label>
                              @endforeach
                            @endif

                            @if (sizeof($other)>0)
                              <h5 class="m-t-25">
                                <small class="icofont icofont-spinner-alt-4 text-red right-fa"></small> @lang('translate.other') <small class="text-muted left-fa">@lang('translate.multiple_additonal')</small>
                              </h5>
                              @foreach ($other as $key => $one)
                                <label class="checkbox-holder">
                                  <input type="checkbox" name="additional[]" value="{{ $one->id }}">
                                  <span class="checkbox-icon"><i class="icofont icofont-check"></i></span>
                                  <span>
                                    @if (App::isLocale('en'))
                                      {{ $one->en_name}} <i class="left-fa right-fa">-</i> <span class="text-red">{{ $one->price }}</span>
                                    @else
                                      {{ $one->ar_name }} <i class="right-fa left-fa">-</i> <span class="text-red">{{ $one->price }}</span>
                                    @endif
                                  </span>
                                </label>
                              @endforeach
                            @endif
                          </div>

                          <div class="hidden-xs col-sm-3 p-0">
                            <img src="{{asset('public/site/images/modal-bg.png')}}" class="img-responsive center-block">
                          </div>

                          <div class="text-left m-t-25">
                            <div class="inline-block">
                              <div class="table-view counter">
                                <div class="table-cell"><button class="plus btn btn-sm btn-white btn-border f-s-20">+</button></div>
                                <div class="table-cell"><input name="quantity" type="number" class="no-border" value="1" readonly></div>
                                <div class="table-cell"><button class="minus btn btn-sm btn-white btn-border f-s-20">-</button></div>
                              </div>
                            </div>
                            <button type="submit" class="btn btn-red left-fa">@lang('translate.add')</button>
                          </div>

                        </div>
                      </form>

                    </div>
                  </div>
                </div>
              </div>

            @endforeach
          @endif

        </div>

      </div>
    @endif

</div></div>

  </div>

  @endsection
