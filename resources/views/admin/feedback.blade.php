
@extends('admin.layout')
@section('content')
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
        <h5> الاعلانات</h5>
      </div>
      <div class="card_body card_table">
        <table id="example" class="cell-border display" cellspacing="0" width="100%;margin-top: 20px;">
          <thead>
            <tr>
              <th>#</th>
              <th>اسم المستخدم</th>
              <th>رقم الجوال </th>
              <th>المحتوى</th>
              <th>تاريخ التعليق </th>
              <th>حذف</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($feedbacks as $key => $feedback)
              <tr>
                <td>{{ $feedback->id }}</td>
                <td>{{ $feedback->name }}</td>
                <td>{{ $feedback->phone }}</td>
                <td>{{ $feedback->content }}</td>
                <td>{{ $feedback->created_at }}</td>
                <td>
                  <form action="{{ url('admin/site/feedback/'. $feedback->id ) }}" method="post"style="Display:inline-block;">
                    {{ csrf_field() }}
                    {{ method_field("DELETE") }}
                    <button class="fa fa-remove remov_btn" title="مسح"></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {{ $feedbacks->links() }}
@endsection
