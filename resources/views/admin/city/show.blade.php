@extends('admin.layout')
@section('content')
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
        <h5>احياء مدينة {{ $city->ar_name }}</h5>
        <a href="{{ asset('admin/area/'.$city->id.'/create') }}"> اضافة حي</a>
      </div>
      <div class="card_body">
        <table id="example" class="cell-border display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>اسم الحي بالعربي </th>
              <th>اسم الحي بالانجليزي </th>
              <th>التحكم </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($areas as $key => $area)
              <tr>
                <td>{{ $area->id }}</td>
                <td>{{ $area->ar_name }}</td>
                <td>{{ $area->en_name }}</td>
                <td>
                  <form action="{{ url('admin/area/'. $area->id.'/edit') }}" method="get"style="Display:inline-block;">
                    <button class="fa fa-pencil edit_btn" title="تعديل"></button>
                  </form>
                  <form action="{{ url('admin/area/'. $area->id ) }}" method="post"style="Display:inline-block;">
                    {{ csrf_field() }}
                    {{ method_field("DELETE") }}
                    <button class="fa fa-remove remov_btn" title="مسح"></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {{ $areas->links() }}
@endsection
