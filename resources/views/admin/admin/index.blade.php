
@extends('admin.layout')
@section('content')
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
        <h5> الادارة</h5>
      </div>
      <div class="card_body card_table">
        <table id="example" class="cell-border display" cellspacing="0" width="100%;margin-top: 20px;">
          <thead>
            <tr>
              <th>#</th>
              <th>الاسم</th>
              <th>تاريخ التسجيل </th>
              <th>التحكم </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($admins as $key => $admin)
              <tr>
                <td>{{ $admin->id }}</td>
                <td>{{ $admin->name}}</td>
                <td>{{ $admin->created_at }}</td>
                <td>
                  <form action="{{ url('admin/admin/'. $admin->id.'/edit') }}" method="get" style="Display:inline-block;">
                    <button class="fa fa-pencil edit_btn" title="تعديل"></button>
                  </form>
                  <form action="{{ url('admin/admin/'. $admin->id ) }}" method="post"style="Display:inline-block;">
                    {{ csrf_field() }}
                    {{ method_field("DELETE") }}
                    <button class="fa fa-remove remov_btn" title="مسح"></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {{ $admins->links() }}
@endsection
