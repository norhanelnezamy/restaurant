@extends('admin.layout')
  @section('content')
        <div class=" col-xs-12 colmn_card">
          <div class="card card_ card_form white lighten-2">
            <div class="card-header">
              <h5>لماذا اسرع طعام ؟ </h5>
            </div>
            <div class="card_body">
              <div class="row">
                <!-- form start -->
                <?php $setting = setting();?>
                <form class="col s12" action="{{asset('admin/setting/why')}}" method="post">
                  {{ csrf_field() }}
                  <input type="hidden" name="id" value="{{ $setting->id }}">
                  <div class="row">
                    <div class="input-field col s12">
                      <label for="name">المحتوى العربي </label>
                      <textarea name="ar_content"  class="materialize-textarea">{{old('ar_content', $setting->ar_why)}}</textarea>
                      @if ($errors->has('ar_content'))
                        <p style="color:red">{{ $errors->first('ar_content') }}</p>
                      @endif
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col s12">
                      <label for="name">المحتوى بالانجليزي </label>
                      <textarea name="en_content"  class="materialize-textarea">{{old('en_content', $setting->en_why)}}</textarea>
                      @if ($errors->has('en_content'))
                        <p style="color:red">{{ $errors->first('en_content') }}</p>
                      @endif
                    </div>
                  </div>

                  <button class="btn btn_submit" type="submit" name="action">حفظ
                    <i class="material-icons right">send</i>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div><!-- COLMN FOR FIRST CARD END-->
  @endsection
