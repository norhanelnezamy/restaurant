N

<!DOCTYPE html>
<html >
<head>
  <!--Let browser know website is optimized for mobile-->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>fast food </title>

  <link rel="shortcut icon" href="{{asset('public/dashboard/images/logo.png')}}" type="image" />
  <link rel="stylesheet" href="{{asset('public/admin_asset/bootstrap/css/bootstrap.css')}}">
  <!--font Awesome-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

  <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" />
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="{{asset('public/admin_asset/css/materialize.min.css')}}"  media="screen,projection"/>
  <link rel="stylesheet" href="{{asset('public/admin_asset/bootstrap/css/stylesheet.css')}}">
  <link rel="stylesheet" href="{{asset('public/admin_asset/bootstrap/css/responsive.css')}}">
  <link rel="stylesheet" href="{{asset('public/admin_asset/css/style.css')}}">
  <script type="text/javascript" src="{{asset('public/admin_asset/bootstrap/js/jquery-3.1.1.min.js')}}" ></script>
</head>
<body>
  <!--sid_nav start-->
  <div class="sid_nav scrollbar" id="ex3">
    <!--sid_nav_list start-->
    <ul class="sid_nav_list">
      <!--normal li-->
      <li class="li"><a href="{{ asset('/admin') }}"class="index waves-effect waves-light link_icon"><i class="material-icons">home</i></a><a href="{{ asset('/admin') }}" class="link_">الرئيسية </a></li>
      <!--collapsible li start-->
      <li class="no-padding">
        <ul class="collapsible collapsible_ collapsible-accordion">
          <li class="active_">
            <a href="{{ asset('/admin/admin') }}" class="admin waves-effect waves-light link_icon"><i class="material-icons">vpn_key</i></a>
            <a  class="li collapsible-header collapsible-header_ link_" style="text-decoration: none;">الادارة </a>
            <div class="collapsible-body collapsible-body_">
              <ul class="">
                <li class="li0 waves-effect waves-light "><a href="{{ asset('admin/admin/create') }}"><i class="fa fa-link" aria-hidden="true"></i> اضافة جديدة</a></li>
                <li class="li0 waves-effect waves-light"><a href="{{ asset('admin/admin') }}"><i class="fa fa-link" aria-hidden="true"></i> تصفح</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li><!--collapsible li end-->

      <li class="no-padding">
        <ul class="collapsible collapsible_ collapsible-accordion">
          <li class="active_">
            <a href="{{ asset('/admin/category') }}" class="category waves-effect waves-light link_icon"><i class="material-icons">location_on</i></a>
            <a  class="li collapsible-header collapsible-header_ link_" style="text-decoration: none;">انواع المطابخ</a>
            <div class="collapsible-body collapsible-body_">
              <ul class="">
                <li class="li0 waves-effect waves-light "><a href="{{ asset('admin/category/create') }}"><i class="fa fa-link" aria-hidden="true"></i> اضافة جديدة</a></li>
                <li class="li0 waves-effect waves-light"><a href="{{ asset('admin/category') }}"><i class="fa fa-link" aria-hidden="true"></i> تصفح</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li><!--collapsible li end-->

      <!--collapsible li start-->
      <li class="no-padding">
        <ul class="collapsible collapsible_ collapsible-accordion">
          <li class="active_">
            <a href="{{ asset('/admin/country') }}" class="country waves-effect waves-light link_icon"><i class="material-icons">location_on</i></a>
            <a  class="li collapsible-header collapsible-header_ link_" style="text-decoration: none;">البلاد</a>
            <div class="collapsible-body collapsible-body_">
              <ul class="">
                <li class="li0 waves-effect waves-light "><a href="{{ asset('admin/country/create') }}"><i class="fa fa-link" aria-hidden="true"></i> اضافة جديدة</a></li>
                <li class="li0 waves-effect waves-light"><a href="{{ asset('admin/country') }}"><i class="fa fa-link" aria-hidden="true"></i> تصفح</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li><!--collapsible li end-->

      <li class="li"><a href="{{ asset('/admin/b/brand/restaurant') }}"class="restaurant waves-effect waves-light link_icon"><i class="material-icons">restaurant_menu</i></a><a href="{{ asset('/admin/b/brand/restaurant') }}" class="link_">كل المطاعم </a></li>
      <li class="li"><a href="{{ asset('/admin/b/brand/smart_restaurant') }}"class="smart_restaurant waves-effect waves-light link_icon"><i class="material-icons">restaurant</i></a><a href="{{ asset('/admin/b/brand/smart_restaurant') }}" class="link_">مطاعم راقية </a></li>
      <li class="li"><a href="{{ asset('/admin/b/brand/productive_family') }}"class="productive_family waves-effect waves-light link_icon"><i class="material-icons">store_mall_directory</i></a><a href="{{ asset('/admin/b/brand/productive_family') }}" class="link_"> الاسر المنتجة </a></li>

      <li class="no-padding">
        <ul class="collapsible collapsible_ collapsible-accordion">
          <li class="active_">
            <a href="{{ asset('/admin/ad') }}" class="ad waves-effect waves-light link_icon"><i class="material-icons">assistant</i></a>
            <a  class="li collapsible-header collapsible-header_ link_" style="text-decoration: none;">اعلانات المطاعم</a>
            <div class="collapsible-body collapsible-body_">
              <ul class="">
                <li class="li0 waves-effect waves-light "><a href="{{ asset('admin/ad/create') }}"><i class="fa fa-link" aria-hidden="true"></i> اضافة جديدة</a></li>
                <li class="li0 waves-effect waves-light"><a href="{{ asset('admin/ad') }}"><i class="fa fa-link" aria-hidden="true"></i> تصفح</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li><!--collapsible li end-->
      <li class="no-padding">
        <ul class="collapsible collapsible_ collapsible-accordion">
          <li class="active_">
            <a id="#" href="{{ asset('/admin/user') }}" class="user waves-effect waves-light link_icon"><i class="material-icons">account_circle</i></a>
            <a  class="li collapsible-header collapsible-header_ link_" style="text-decoration: none;">المستخدمين </a>
            <div class="collapsible-body collapsible-body_">
              <ul class="">
                <li class="li0 waves-effect waves-light "><a href="{{ asset('admin/user/create') }}"><i class="fa fa-link" aria-hidden="true"></i> اضافة جديدة</a></li>
                <li class="li0 waves-effect waves-light"><a href="{{ asset('admin/user') }}"><i class="fa fa-link" aria-hidden="true"></i> تصفح</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li><!--collapsible li end-->

      <li class="li"><a href="{{ asset('/admin/site/feedback') }}"class="feedback waves-effect waves-light link_icon"><i class="material-icons">create</i></a><a href="{{ asset('/admin/site/feedback')}}" class="link_">اراء المستخدمين </a></li>
      <li class="li"><a href="{{ asset('/admin/setting/social') }}"class="social waves-effect waves-light link_icon"><i class="material-icons">timeline</i></a><a href="{{ asset('/admin/setting/social')}}" class="link_">التواصل الاجتماعي </a></li>

      <li class="no-padding">
        <ul class="collapsible collapsible_ collapsible-accordion">
          <li class="active_">
            <a href="{{ asset('/admin/setting/about') }}" class="setting waves-effect waves-light link_icon"><i class="material-icons">settings</i></a>
            <a  class="li collapsible-header collapsible-header_ link_" style="text-decoration: none;">الاعدادات</a>
            <div class="collapsible-body collapsible-body_">
              <ul class="">
                <li class="li0 waves-effect waves-light "><a href="{{ asset('/admin/setting/about') }}"><i class="fa fa-link" aria-hidden="true"></i> عن اسرع طعام</a></li>
                <li class="li0 waves-effect waves-light"><a href="{{ asset('/admin/setting/why') }}"><i class="fa fa-link" aria-hidden="true"></i> لماذا اسرع طعام</a></li>
                <li class="li0 waves-effect waves-light"><a href="{{ asset('/admin/setting/what') }}"><i class="fa fa-link" aria-hidden="true"></i> ماهو اسرع طعام</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li><!--collapsible li end-->

      @if (session()->has('side_bar'))
        <?php $side_bar = session()->get('side_bar');?>
        <script>
        console.log('a.{{ $side_bar }}');
        $(function(){
          $('a.{{ $side_bar }}').addClass('active_icon');
        });
        </script>
      @endif
    </ul><!--sid_nav_list end-->
  </div><!--sid_nav end-->

  <div class="container-fluid">
    <div class="main_content" onclick="hid()">
      <div class="row">
        <div class="col-xs-12"  style="margin:80px auto;margin-bottom:10px;">
          <!-- statistics here -->
          @if(Session::has('message'))
            <p class="alert alert-info"><i class="fa fa-check"></i>{{ Session::get('message') }}</p>
          @endif
          @yield('content')
        </div><!-- mian row end-->
        <div class="row footer">
          <div class="col-xs-12">
            <p class="copyright">جميع الحقوق محفوظه <a href="http://khalijalbarmaja.com.sa/" style="text-decoration:none;color: teal;"> خليج البرمجة </a> 2017 </p>
          </div>
        </div>

      </div>
    </div>
  </div>
    <!--tool_bar -->
    <div class="container-fluid layout_toolbar teal lighten-2">
      <a class="waves-effect waves-light btn teal lighten-2 togle_opened">  <i class="material-icons">menu</i></a>
      <a class="waves-effect waves-light btn teal lighten-2 togle_closed">  <i class="material-icons"> close</i></a>
      <a href="dashbourd.html" class="logo_text" >لوحة التحكم </a>

      <!-- Dropdown Trigger -->
      <img src="{{asset('public/dashboard/images/logo.png')}}" alt="" class="">
      <a class='dropdown-button personal' href='' data-activates='dropdown1'>
      {{ Auth::guard('admin')->user()->name }}
      </a>

      <!-- Dropdown Structure -->
      <ul id='dropdown1' class='dropdown-content'>
        <li><a href="{{ asset('/') }}" class="personal personal_"> <img src="{{asset('public/admin_asset/logo.png')}}" alt="" class="">
          {{ Auth::guard('admin')->user()->name }}</a>
        </li>
        <li class="divider"></li>
        <li><a href="{{ asset('admin/logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i>تسجيل خروج</a></li>
      </ul>
  </div>
  <!-- / tool_bar -->

  <!--Import jQuery before materialize.js-->

  <script type="text/javascript" src="{{asset('public/admin_asset/bootstrap/js/bootstrap.min.js')}}" ></script>
  <script type="text/javascript" src="{{asset('public/admin_asset/js/materialize.min.js')}}"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="{{asset('public/admin_asset/bootstrap/js/javascript.js')}}" ></script>
  <script type="text/javascript" src="{{asset('public/admin_asset/js/script.js')}}" ></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
      "createdRow": function ( row, data, index ) {
        if ( data[4].replace(/[\$,]/g, '') * 1 > 150000 ) {
          $('td', row).eq(4).addClass('highlight');
        }
      }
    } );
  });

  </script>
</body>
</html>
