<!DOCTYPE html>
<html>
<head>
  <!--Let browser know website is optimized for mobile-->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>fast food</title>

  <link rel="shortcut icon" href="{{asset('dashboard/images/logo.png')}}" type="image" />
  <link rel="stylesheet" href="{{asset('public/admin_asset/bootstrap/css/bootstrap.css')}}">
  <!--font Awesome-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="{{asset('public/admin_asset/css/materialize.min.css')}}"  media="screen,projection"/>
  <link rel="stylesheet" href="{{asset('public/admin_asset/bootstrap/css/stylesheet.css')}}">
  <link rel="stylesheet" href="{{asset('public/admin_asset/bootstrap/css/responsive.css')}}">
</head>
<body>
  <div class="container-fluid login">
    <div class="pro_pic">
      <img src="{{asset('dashboard/images/logo.png')}}" alt="" class="">
    </div>
    <h4 class="login_h4">تسجيل دخول</h4>
    <div class="row login_ card">
      <form class="col s12" method="post" action="{{ asset('admin/login') }}">
        {{ csrf_field() }}
        <div class="row">
          <div class="input-field col s12">
            <label for="first_name"> الاسم </label>
            <input name="name"  id="name" type="text" class="validate" value="{{ old('name') }}">
            @if ($errors->has('name'))
              <p style="color:red">{{ $errors->first('name') }}</p>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <label for="password">كلمة السر </label>
            <input name="password" id="password" type="password" class="validate" value="{{ old('password') }}">
            @if ($errors->has('password'))
              <p style="color:red">{{ $errors->first('password') }}</p>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            <button class="btn waves-effect waves-light" type="submit" name="action">
              <i class="material-icons right">send</i> تسجيل
            </button>
          </div>
        </div>
        @if (session()->has('authorize'))
          <p style="color:red">{{ session('authorize') }}</p>
        @endif
      </form>
    </div>
  </div>
  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="{{asset('public/admin_asset/bootstrap/js/jquery-3.1.1.min.js')}}" ></script>
  <script type="text/javascript" src="{{asset('public/admin_asset/bootstrap/js/bootstrap.min.js')}}" ></script>
  <script type="text/javascript" src="{{asset('public/admin_asset/js/materialize.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('public/admin_asset/bootstrap/js/javascript.js')}}" ></script>
</body>
</html>
