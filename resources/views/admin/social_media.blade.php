@extends('admin.layout')
  @section('content')
        <div class=" col-xs-12 colmn_card">
          <div class="card card_ card_form white lighten-2">
            <div class="card-header">
              <h5>روابط حسابات التواصل الاجتماعي </h5>
            </div>
            <div class="card_body">
              <div class="row">
                <!-- form start -->
                <?php $setting = setting();?>
                <form class="col s12" action="{{asset('admin/setting/social')}}" method="post">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="input-field col s12">
                      <label for="name">رابط الفيسبوك </label>
                      <input value="{{old('facebook', $setting->facebook)}}" name="facebook" type="text" class="validate">
                      @if ($errors->has('facebook'))
                        <p style="color:red">{{ $errors->first('facebook') }}</p>
                      @endif
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col s12">
                      <label for="name">رابط تويتر </label>
                      <input value="{{old('twitter', $setting->twitter)}}" name="twitter" type="text" class="validate">
                      @if ($errors->has('twitter'))
                        <p style="color:red">{{ $errors->first('twitter') }}</p>
                      @endif
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col s12">
                      <label for="name">رابط جوجل بلس</label>
                      <input value="{{old('google', $setting->google)}}" name="google" type="text" class="validate">
                      @if ($errors->has('google'))
                        <p style="color:red">{{ $errors->first('google') }}</p>
                      @endif
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col s12">
                      <label for="name">رابط انستجرام</label>
                      <input value="{{old('instgram', $setting->instgram)}}" name="instgram" type="text" class="validate">
                      @if ($errors->has('instgram'))
                        <p style="color:red">{{ $errors->first('instgram') }}</p>
                      @endif
                    </div>
                  </div>

                  <button class="btn btn_submit" type="submit" name="action">حفظ
                    <i class="material-icons right">send</i>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div><!-- COLMN FOR FIRST CARD END-->
  @endsection
