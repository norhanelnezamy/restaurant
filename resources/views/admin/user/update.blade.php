@extends('admin.layout')
  @section('content')
        <div class=" col-xs-12 colmn_card">
          <div class="card card_ card_form white lighten-2">
            <div class="card-header">
              <h5>تعديل بيانات مستخدم</h5>
            </div>
            <div class="card_body">
              <div class="row">
                <!-- form start -->
                <form class="col s12" action="{{asset('admin/user/'.$user->id)}}" method="post">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="input-field col s12">
                      <label for="name">الاسم </label>
                      <input value="{{old('name',$user->name)}}" name="name" type="text" class="validate">
                      @if ($errors->has('name'))
                        <p style="color:red">{{ $errors->first('name') }}</p>
                      @endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <label for="name">رقم الجوال </label>
                      <input value="{{old('phone',$user->phone)}}" name="phone" type="text" class="validate">
                      @if ($errors->has('phone'))
                        <p style="color:red">{{ $errors->first('phone') }}</p>
                      @endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col m6 s12">
                      <label for="password">كلمة المرور </label>
                      <input value="{{old('password')}}" name="password" type="password"  id="first_name" type="text" class="validate">
                      @if ($errors->has('password'))
                        <p style="color:red">{{ $errors->first('password') }}</p>
                      @endif
                    </div>
                    <div class="input-field col m6 s12">
                      <label for="password_confirmation">تأكيد كلمة المرور </label>
                      <input value="{{old('password_confirmation')}}" name="password_confirmation" type="password" id="last_name" type="text" class="validate">
                      @if ($errors->has('password_confirmation'))
                        <p style="color:red">{{ $errors->first('password_confirmation') }}</p>
                      @endif
                    </div>
                  </div>
                  <button class="btn btn_submit" type="submit" name="action">تسجيل
                    <i class="material-icons right">send</i>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div><!-- COLMN FOR FIRST CARD END-->
  @endsection
