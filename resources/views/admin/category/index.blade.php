@extends('admin.layout')

@section('content')
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
        <h5> انواع المطابخ</h5>
      </div>
      <div class="card_body">
        <table id="example" class="cell-border display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>الاسم</th>
              <th>التحكم </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($categories as $key => $category)
              <tr>
                <td>{{ $category->id }}</td>
                <td>{{ $category->ar_name }} -- {{ $category->en_name }}</td>
                <td>
                  <form action="{{ url('admin/category/'. $category->id.'/edit') }}" method="get"style="Display:inline-block;">
                    <button class="fa fa-pencil edit_btn" title="تعديل"></button>
                  </form>
                  <form action="{{ url('admin/category/'. $category->id ) }}" method="post"style="Display:inline-block;">
                    {{ csrf_field() }}
                    {{ method_field("DELETE") }}
                    <button class="fa fa-remove remov_btn" title="مسح"></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {{ $categories->links() }}
@endsection
