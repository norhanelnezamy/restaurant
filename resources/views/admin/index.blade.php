@extends('admin.layout')
   @section('content')
     <!-- statistics here -->
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="card statistics_3">
         <div class="">
           <strong class="text-xl">7895255<i class="material-icons">restaurant</i></strong><br/>
           <span class="opacity-50">المطاعم</span>
         </div>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="card statistics_3">
         <div class="">
           <strong class="text-xl">788515<i class="material-icons">store_mall_directory</i></strong><br/>
           <span class="opacity-50">الاسر المنتجة </span>
         </div>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="card statistics_3">
         <div class="">
           <strong class="text-xl">8795455 <i class="material-icons">restaurant_menu</i> </strong><br/>
           <span class="opacity-50"> المطاعم الراقية</span>
         </div>
       </div>
     </div>
     <div class="col-md-3 col-sm-6 col-xs-12">
       <div class="card statistics_3">
         <div class="">
           <strong class="text-xl">9985525 <i class="material-icons">account_circle</i> </strong><br/>
           <span class="opacity-50">المستخدمين</span>
         </div>
       </div>
     </div>
     <!-- statistics end -->

     <div class="col-xs-12 col-md-6">
       <div class="card card_  white lighten-2">
         <div class="card-header">
           <h5>خريطه جوجل  </h5>
         </div>
         <div class="card_body">
           <div class="map-responsive">
          <div id="map"  class="map_"></div>
           </div>
         </div>
       </div>
     </div>
     <div class="col-xs-12 col-md-6">
       <div class="card card_  white lighten-2">
         <div class="card-header">
           <h5>ارسال رسالة بريد الاكتروني </h5>
         </div>
         <div class="card_body">
           <form role="form" class="form-horizontal" action="{{ asset('admin/send/email') }}" method="post">
             {{ csrf_field() }}
             <div class="form-group">
               <label class="col-sm-3" for="inputTo"><i class="material-icons">account_circle</i>الى </label>
               <div class="col-sm-9"><input type="text" class="form-control" id="inputTo" placeholder="admin@fastfood.com" name="email"></div>
             </div>
             @if ($errors->has('email'))
               <p style="color:red">{{ $errors->first('email') }}</p>
             @endif
             <div class="form-group">
               <label class="col-sm-3" for="inputSubject"><i class="material-icons">subject</i></span>العنوان </label>
               <div class="col-sm-9"><input type="text" class="form-control" id="inputSubject" placeholder="subject" name="subject"></div>
             </div>
             @if ($errors->has('subject'))
               <p style="color:red">{{ $errors->first('subject') }}</p>
             @endif
             <div class="form-group">
               <label class="col-sm-12" for="inputBody"><i class="material-icons">email</i></span>نص الرسالة </label>
               <div class="col-sm-12"><textarea class="form-control" id="inputBody" rows="8" name="content"></textarea></div>
             </div>
             @if ($errors->has('content'))
               <p style="color:red">{{ $errors->first('content') }}</p>
             @endif
             <button class="btn btn_submit" type="submit" name="action">ارسال </button>
           </form>
         </div>
       </div>
     </div>

     <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBGERiio3BH9dTWcAghYLpldQKnWFr9-OE"></script>
     <script type="text/javascript">
     window.onload = getLocation;
     function getLocation() {
       if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(showPosition);
       } else {
         alert("Geolocation is not supported by browser.");
       }
     }
     function showPosition(position) {
       var lat = position.coords.latitude;
       var lng = position.coords.longitude;
       var latLng = new google.maps.LatLng(lat, lng);
       var map = new google.maps.Map(document.getElementById('map'), {
         zoom: 16,
         center: latLng,
       });
       var marker = new google.maps.Marker({
         position: latLng,
         map: map,
         draggable: true
       });
     }
     </script>
   @endsection
