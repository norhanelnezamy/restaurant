@extends('admin.layout')
  @section('content')
    <div class=" col-xs-12 colmn_card">
      <div class="card card_ card_form white lighten-2">
        <div class="card-header">
          <h5>اضافة دولة</h5>
        </div>
        <div class="card_body">
          <div class="row">
            <!-- form start -->
            <form class="col s12" action="{{asset('admin/country')}}" method="post">
              {{ csrf_field() }}
              <div class="row">
                <div class="input-field col s12">
                  <label for="name">اسم الدولة بالعربي </label>
                  <input value="{{old('ar_name')}}" name="ar_name"  id="first_name" type="text" class="validate">
                  @if ($errors->has('ar_name'))
                    <p style="color:red">{{ $errors->first('ar_name') }}</p>
                  @endif
                </div>
                <div class="input-field col s12">
                  <label for="name">اسم الدولة بالانجليزي </label>
                  <input value="{{old('en_name')}}" name="en_name"  id="first_name" type="text" class="validate">
                  @if ($errors->has('en_name'))
                    <p style="color:red">{{ $errors->first('en_name') }}</p>
                  @endif
                </div>
                <button class="btn btn_submit" type="submit" name="action">حفظ
                  <i class="material-icons right">send</i>
                </button>
              </form>
            </div>
          </div>
        </div>
      </div><!-- COLMN FOR FIRST CARD END-->
    </div>
  @endsection
