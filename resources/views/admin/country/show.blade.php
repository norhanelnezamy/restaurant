@extends('admin.layout')
@section('content')
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
        <h5> مدن {{ $country->ar_name }}</h5>
        <a href="{{ asset('admin/city/'.$country->id.'/create') }}"> اضافة مدينة</a>
      </div>
      <div class="card_body">
        <table id="example" class="cell-border display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>اسم المدينة بالعربي </th>
              <th>اسم المدينة بالانجليزي </th>
              <th>التحكم </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($cities as $key => $city)
              <tr>
                <td>{{ $city->id }}</td>
                <td>{{ $city->ar_name }}</td>
                <td>{{ $city->en_name }}</td>
                <td>
                  <form action="{{ url('admin/city/'. $city->id) }}" method="get"style="Display:inline-block;">
                    <button class="fa fa-eye edit_btn" title="المدن "></button>
                  </form>
                  <form action="{{ url('admin/city/'. $city->id.'/edit') }}" method="get"style="Display:inline-block;">
                    <button class="fa fa-pencil edit_btn" title="تعديل"></button>
                  </form>
                  <form action="{{ url('admin/city/'. $city->id ) }}" method="post"style="Display:inline-block;">
                    {{ csrf_field() }}
                    {{ method_field("DELETE") }}
                    <button class="fa fa-remove remov_btn" title="مسح"></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {{ $cities->links() }}
@endsection
