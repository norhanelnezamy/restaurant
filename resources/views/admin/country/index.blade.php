@extends('admin.layout')
@section('content')
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
        <h5> الدول</h5>
      </div>
      <div class="card_body">
        <table id="example" class="cell-border display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>اسم الدولة بالعربي </th>
              <th>اسم الدولة بالانجليزي </th>
              <th>التحكم </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($countries as $key => $country)
              <tr>
                <td>{{ $country->id }}</td>
                <td>{{ $country->ar_name }}</td>
                <td>{{ $country->en_name }}</td>
                <td>
                  <form action="{{ url('admin/country/'. $country->id) }}" method="get"style="Display:inline-block;">
                    <button class="fa fa-eye edit_btn" title="المدن "></button>
                  </form>
                  <form action="{{ url('admin/country/'. $country->id.'/edit') }}" method="get"style="Display:inline-block;">
                    <button class="fa fa-pencil edit_btn" title="تعديل"></button>
                  </form>
                  <form action="{{ url('admin/country/'. $country->id ) }}" method="post"style="Display:inline-block;">
                    {{ csrf_field() }}
                    {{ method_field("DELETE") }}
                    <button class="fa fa-remove remov_btn" title="مسح"></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {{ $countries->links() }}
@endsection
