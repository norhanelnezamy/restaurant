
@extends('admin.layout')
@section('content')
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
        <h5> الاعلانات</h5>
      </div>
      <div class="card_body card_table">
        <table id="example" class="cell-border display" cellspacing="0" width="100%;margin-top: 20px;">
          <thead>
            <tr>
              <th>#</th>
              <th>اسم المالك </th>
              <th>رقم الجوال </th>
              <th> البريد الالكتروني</th>
              <th>الاسم</th>
              @if ($type != 'productive_family')
                <th>عدد الفروع </th>
              @endif
              <th>تاريخ التسجيل </th>
              <th>التفعيل </th>
              <th>التحكم </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($brands as $key => $brand)
              <tr>
                <td>{{ $brand->id }}</td>
                <td>{{ $brand->name }}</td>
                <td>{{ $brand->phone }}</td>
                <td>{{ $brand->email }}</td>
                <td>{{ $brand->ar_name .'-' . $brand->en_name }}</td>
                @if ($type != 'productive_family')
                  <td>{{ $brand->branches->count() }}</td>
                @endif
                <td>{{ $brand->created_at }}</td>
                <td>
                  <form action="{{ url('admin/brand/'. $brand->id) }}" method="post" style="Display:inline-block;">
                    {{ method_field("PUT") }}
                    {{ csrf_field() }}
                    @if ($brand->admin_active == 0)
                      <input type="hidden" name="status" value="1">
                      <button type="submit" title="التفعيل"><i class="material-icons">mood_bad</i></button>
                    @else
                      <input type="hidden" name="status" value="0">
                      <button type="submit" class="active_icon" title="الغاء التفعيل"><i class="material-icons">mood</i></button>
                    @endif
                  </form>
                </td>
                <td>
                  @if ($type == 'productive_family')
                    <form action="{{ url('admin/branch/'. $brand->id) }}" method="get"style="Display:inline-block;">
                      <button class="fa fa-eye edit_btn" title="مشاهدة"></button>
                    </form>
                  @else
                    <form action="{{ url('admin/brand/'. $brand->id) }}" method="get"style="Display:inline-block;">
                      <button class="fa fa-eye edit_btn" title="مشاهدة"></button>
                    </form>
                  @endif
                  <form action="{{ url('admin/brand/'. $brand->id ) }}" method="post"style="Display:inline-block;">
                    {{ csrf_field() }}
                    {{ method_field("DELETE") }}
                    <button class="fa fa-remove remov_btn" title="مسح"></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {{ $brands->links() }}
@endsection
