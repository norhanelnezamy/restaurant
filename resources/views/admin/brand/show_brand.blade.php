
@extends('admin.layout')
@section('content')
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
        <h5> الفروع</h5>
      </div>
      <div class="card_body card_table">
        <table id="example" class="cell-border display" cellspacing="0" width="100%;margin-top: 20px;">
          <thead>
            <tr>
              <th>#</th>
              <th>اسم المدير </th>
              <th>رقم الجوال </th>
              <th> البريد الالكتروني</th>
              <th>اسم المطعم</th>
              <th>العنوان</th>
              <th>تاريخ الاضافة </th>
              <th>التحكم </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($branches as $key => $branch)
              <tr>
                <td>{{ $branch->id }}</td>
                <td>{{ $branch->name }}</td>
                <td>{{ $branch->phone }}</td>
                <td>{{ $branch->email }}</td>
                <td>{{ $branch->ar_name .'-' . $branch->en_name }}</td>
                <td>{{ $branch->country.'-'.$branch->city.'-'.$branch->area }}</td>
                <td>{{ $branch->created_at }}</td>
                <td>
                  <form action="{{ url('admin/branch/'. $branch->id) }}" method="get"style="Display:inline-block;">
                    <button class="fa fa-eye edit_btn" title="مشاهدة"></button>
                  </form>
                  <form action="{{ url('admin/branch/'. $branch->id ) }}" method="post"style="Display:inline-block;">
                    {{ csrf_field() }}
                    {{ method_field("DELETE") }}
                    <button class="fa fa-remove remov_btn" title="مسح"></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
