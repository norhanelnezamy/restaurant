
@extends('admin.layout')
@section('content')
  <div class="col-xs-12 colmn_card">
    <div class="card card_ white lighten-2">
      <div class="card-header">
        <h5> المطاعم</h5>
      </div>
      <div class="card_body card_table">
        <table id="example" class="cell-border display" cellspacing="0" width="100%;margin-top: 20px;">
          <thead>
            <tr>
              <th>#</th>
              <th>اسم المالك </th>
              <th>رقم الجوال </th>
              <th>صورة الاعلان</th>
              <th>العنوان</th>
              <th>تاريخ التسجيل </th>
              <th>التفعيل </th>
              <th>التحكم </th>
            </tr>
          </thead>
          <tbody>
            @foreach ($ads as $key => $ad)
              <tr>
                <td>{{ $ad->id }}</td>
                <td>{{ $ad->name }}</td>
                <td>{{ $ad->phone }}</td>
                <td><img src="{{asset('public/uploads/'.$ad->photo)}}" style="width:100px;height:100px;"></td>
                <td>{{ $ad->ar_title .'-' . $ad->en_title }}</td>
                <td>{{ $ad->created_at }}</td>
                <td>
                  <form action="{{ url('admin/ad/active/'. $ad->id) }}" method="post" style="Display:inline-block;">
                    {{ csrf_field() }}
                    @if ($ad->active == 0)
                      <input type="hidden" name="status" value="1">
                      <button type="submit" title="التفعيل"><i class="material-icons">mood_bad</i></button>
                    @else
                      <input type="hidden" name="status" value="0">
                      <button type="submit" class="active_icon" title="الغاء التفعيل"><i class="material-icons">mood</i></button>
                    @endif
                  </form>
                </td>
                <td>
                  <form action="#" style="Display:inline-block;">
                    <button class="fa fa-eye edit_btn" title="مشاهدة"></button>
                  </form>
                  <form action="{{ url('admin/ad/'. $ad->id ) }}" method="post"style="Display:inline-block;">
                    {{ csrf_field() }}
                    {{ method_field("DELETE") }}
                    <button class="fa fa-remove remov_btn" title="مسح"></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {{ $ads->links() }}
@endsection
