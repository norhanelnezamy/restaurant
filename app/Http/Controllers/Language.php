<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class Language extends Controller
{
  public function switchLanguage($lang)
  {
    session()->put('Lang', $lang);
    App::setLocale(session()->get('Lang'));
    return redirect()->back();
  }
}
