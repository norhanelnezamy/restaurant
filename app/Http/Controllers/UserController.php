<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\UserRequest;
use App\Model\User;
use App\Model\Message;
use App\Model\Order;
use Auth;

class UserController extends Controller
{

    public function profile()
    {
      session()->forget('nav');
      return view('site.profile');
    }

    public function updateData(UserRequest $request)
    {
      User::where('id', Auth::user()->id)->update(['email' => $request->email, 'name' => $request->name, 'phone' => $request->phone]);
      return redirect()->back()->with('msg', 'Your Data updated .');
    }
    public function updatePassword(PasswordRequest $request)
    {
      User::where('id', Auth::user()->id)->update(['password'=> bcrypt($request->password)]);
      return redirect()->back()->with('msg', 'Your password updated .');
    }

    public function message()
    {
      session()->forget('nav');
      $messages = Message::orderBy('created_at', 'DESC')->where('messages.user_id', Auth::user()->id)
                                                        ->join('branches', 'messages.branch_id', 'branches.id')
                                                        ->join('brands', 'branches.brand_id', 'brands.id')
                                                        ->select('messages.*', 'brands.ar_name as brand_ar_name', 'brands.en_name as brand_en_name')
                                                        ->paginate(15);
      return view('site.message', compact('messages'));
    }

    public function order()
    {
      session()->forget('nav');
      $wait_orders = Order::where('user_id', Auth::user()->id)->where('status', 1)->get();
      $prev_orders = Order::where('user_id', Auth::user()->id)->where('status', 2)->paginate(5);
      // return $wait_orders;
      return view('site.order', compact('wait_orders', 'prev_orders'));
    }
}
