<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Model\User;
use Auth;

class AuthController extends Controller
{
    public function getLogin()
    {
      if (Auth::check()) {
        return redirect('/');
      }
      session()->forget('nav');
      return view('login');
    }

    public function postLogin(LoginRequest $request)
    {
      if (Auth::guard('web')->attempt([ 'phone' => $request->phone , 'password' => $request->password ])) {
        $user_role = Auth::guard('web')->user()->role;
        if (session()->has('page')) {
          return redirect(session()->get('page'));
        }
        if ($user_role == 'user') {
          return redirect('/');
        }
        if ($user_role == 'restaurant' || $user_role == 'smart_restaurant') {
          return redirect('/dashboard/index');
        }
        else {
          return redirect('/dashboard/b/menu');
        }
      }
      return redirect()->back()->with('authorized', 'Invalide Phone or Password .');
    }

    public function getRegister(){
      if (Auth::check()) {
        return redirect('/');
      }
      session()->forget('nav');
      return view('register');
    }

    public function postRegister(Request $request)
    {
      $request->role = 'user';
      $user = User::add($request);
      // active_email($user);
      return redirect('/')->with('add_account', 'Account is created please, active your acount');
    }

    public function active($id, $token)
    {
      User::where([['id',$id], ['token',$token]])->update(['email_active'=> 1]);
      return "account is actived";
    }

    public function logout()
    {
      Auth::logout();
      session()->forget('page');
      return redirect('/');
    }
}
