<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Model\SiteFeedback;

class SiteFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->put('side_bar', 'feedback');
        $feedbacks = SiteFeedback::join('users', 'site_feedbacks.user_id', 'users.id')->select('site_feedbacks.*', 'users.name', 'users.phone')->paginate(20);
        return view('admin.feedback', compact('feedbacks'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SiteFeedback::findOrFail($id)->delete();
        return redirect()->back();
    }
}
