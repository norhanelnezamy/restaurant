<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\City;
use App\Model\Country;
use App\Http\Requests\NameRequest;

class CountryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->put('side_bar', 'country');
        $countries = Country::paginate(20);
        return view('admin.country.index', compact('countries'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      session()->put('side_bar', 'country');
      return view('admin.country.insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NameRequest $request)
    {
      Country::add($request);
      return redirect('admin/country')->with('message', 'تم اضافة البلد.');
    }

    public function show($id)
    {
      $country = Country::findOrFail($id);
      $cities = City::where('country_id', $id)->paginate(20);
      return view('admin.country.show', compact('cities', 'country'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $country = Country::findOrFail($id);
      return view('admin.country.update', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NameRequest $request, $id)
    {
      Country::edit($request, $id);
      return redirect('admin/country')->with('message', 'تم تعديل اسم البلد .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Country::findOrFail($id)->delete();
      return redirect()->back()->with('message', 'تم مسح البلد .');
    }
}
