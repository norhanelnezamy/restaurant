<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Area;
use App\Http\Requests\NameRequest;

class AreaController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($country_id)
    {
      return view('admin.area.insert', compact('country_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NameRequest $request)
    {
      Area::add($request);
      return redirect('admin/city/'.$request->city_id)->with('message', 'تم اضافة حي .');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $area = Area::findOrFail($id);
      return view('admin.area.update', compact('area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NameRequest $request, $id)
    {
      Area::edit($request, $id);
      return redirect()->back()->with('message', 'تم تعديل اسم الحي .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Area::findOrFail($id)->delete();
      return redirect()->back()->with('message', 'تم مسح الحي .');
    }
}
