<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AboutRequest;
use App\Http\Requests\EmailRequest;
use App\Http\Requests\ContactRequest;
use Email;

class IndexController extends Controller
{

  public function getIndex(){
    session()->put('side_bar', 'index');
    return view('admin.index');
  }

  public function postSendEmail(EmailRequest $request){
    send_email($request);
    return redirect()->back();
  }

}
