<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\NameRequest;
use App\Model\Category;

class CategoryController extends Controller
{
    public function index()
    {
        session()->put('side_bar', 'category');
        $categories = Category::paginate(20);
        return view('admin.category.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.category.insert');
    }

    public function store(NameRequest $request)
    {
      Category::create($request->all());
      return redirect('admin/category')->with('message', 'تم أضافة فئة جديدة .');
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.category.update', compact('category'));
    }

    public function update(NameRequest $request,$id)
    {
        Category::where('id',$id)->update(['ar_name'=>$request->ar_name, 'en_name'=>$request->en_name]);
        return redirect('admin/category')->with('message', 'تم تعديل الفئة  .');
    }

    public function destroy($id)
    {
        Category::findOrFail($id)->delete();
        return redirect('admin/category')->with('message', 'تم حذف الفئة  .');
    }
}
