<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\City;
use App\Model\Area;
use App\Http\Requests\NameRequest;

class CityController extends Controller
{

    public function __construct()
    {
        session()->put('side_bar', 'country');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($country_id)
    {
      return view('admin.city.insert', compact('country_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NameRequest $request)
    {
      City::add($request);
      return redirect('admin/country/'.$request->country_id)->with('message', 'تم اضافة مدينة ');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $city = City::findOrFail($id);
      $icon = 'country';
      return view('admin.city.update', compact('city', 'icon'));
    }

    public function show($id)
    {
      $city = City::findOrFail($id);
      $areas = Area::where('city_id', $id)->paginate(20);
      $icon = 'country';
      return view('admin.city.show', compact('areas', 'icon', 'city'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NameRequest $request, $id)
    {
      City::edit($request, $id);
      return redirect()->back()->with('message', 'تم تعديل اسم المدينة .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      City::findOrFail($id)->delete();
      return redirect()->back()->with('message', 'تم مسح المدينة .');
    }
}
