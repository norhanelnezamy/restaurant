<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Branch;

class BranchController extends Controller
{
    public function show($id)
    {
      $branch = Branch::where('branches.brand_id',$id)->join('brands', 'branches.brand_id', 'brands.id')
                                                      ->join('categories', 'brands.category_id', 'categories.id')
                                                      ->join('users', 'branches.user_id', 'users.id')
                                                      ->join('areas', 'branches.area_id', 'areas.id')
                                                      ->join('cities', 'areas.city_id', 'cities.id')
                                                      ->join('countries', 'cities.country_id', 'countries.id')
                                                      ->select('branches.*', 'brands.*', 'users.name', 'users.role', 'users.phone','users.email', 'countries.ar_name as country', 'cities.ar_name as city', 'areas.ar_name as area')
                                                      ->first();
      return view('admin.brand.show_branch', compact('branch'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Branch::findOrFail($id)->delete();
        return redirect()->back();
    }
}
