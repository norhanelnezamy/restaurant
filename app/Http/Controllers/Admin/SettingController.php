<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SocialRequest;
use App\Http\Requests\ContentRequest;
use App\Model\Setting;

class SettingController extends Controller
{
    public function getScoial(){
      session()->put('side_bar', 'social');
      return view('admin.social_media');
    }

    public function postScoial(SocialRequest $request){
      Setting::where('id',$id)->update(['facebook'=>$request->facebook, 'twitter'=>$request->twitter, 'google'=>$request->google, 'instgram'=>$request->instgram]);
      return redirect()->back();
    }

    public function getAbout(){
      session()->put('side_bar', 'setting');
      return view('admin.about');
    }

    public function postAbout(ContentRequest $request){
      Setting::where('id', $request->id)->update(['ar_about'=>$request->ar_content, 'en_about'=>$request->en_content]);
      return redirect()->back();
    }

    public function getWhat(){
      session()->put('side_bar', 'setting');
      return view('admin.what');
    }

    public function postWhat(ContentRequest $request){
      Setting::where('id', $request->id)->update(['ar_what'=>$request->ar_content, 'en_what'=>$request->en_content]);
      return redirect()->back();
    }

    public function getWhy(){
      session()->put('side_bar', 'setting');
      return view('admin.why');
    }

    public function postWhy(ContentRequest $request){
      Setting::where('id', $request->id)->update(['ar_why'=>$request->ar_content, 'en_why'=>$request->en_content]);
      return redirect()->back();
    }
}
