<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\User;
use App\Model\Admin;
use App\Model\Brand;
use App\Model\Branch;

class BrandController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
      session()->put('side_bar', $type);
      $brands = Brand::join('users', 'brands.user_id', 'users.id')->where('users.role', $type)
                                                                  ->select('brands.*', 'users.name','users.email', 'users.phone')
                                                                  ->paginate(12);
      return view('admin.brand.index', compact('brands', 'type'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $branches = Branch::where('branches.brand_id',$id)->join('brands', 'branches.brand_id', 'brands.id')
                                                        ->join('users', 'branches.user_id', 'users.id')
                                                        ->join('areas', 'branches.area_id', 'areas.id')
                                                        ->join('cities', 'areas.city_id', 'cities.id')
                                                        ->join('countries', 'cities.country_id', 'countries.id')
                                                        ->select('branches.*', 'brands.ar_name', 'brands.en_name', 'users.name', 'users.role', 'users.phone','users.email', 'countries.ar_name as country', 'cities.ar_name as city', 'areas.ar_name as area')
                                                        ->get();
      return view('admin.brand.show_brand', compact('branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      Brand::where('id', $id)->update(['admin_active'=> $request->status]);
      return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Brand::findOrFail($id)->delete();
      return redirect()->back()->with('message', 'تم مسح المطعم ');
    }
}
