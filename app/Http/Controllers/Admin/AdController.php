<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AdRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Ad;
use Auth;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->put('side_bar', 'ad');
        $ads = Ad::join('brands', 'ads.brand_id', 'brands.id')->join('users', 'brands.user_id', 'users.id')->select('ads.*', 'users.name', 'users.phone')->paginate(20);
        return view('admin.ad.index', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ad.insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdRequest $request)
    {
        Ad::add($request, Auth::user()->brand->id);
        return redirect('admin/ad');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdRequest $request, $id)
    {
        Ad::edit($request, $id);
        return redirect()->back();
    }

    public function active(Request $request, $id)
    {
        Ad::where('id',$id)->update(['active' => $request->status]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ad::findOrFail($id)->delete();
        return redirect()->back();
    }
}
