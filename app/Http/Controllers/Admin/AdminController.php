<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AdminRequest;
use App\Model\Admin;
use Auth;

class AdminController extends Controller
{

  public function index()
  {
    session()->put('side_bar', 'admin');
    $admins = Admin::paginate(20);
    return view('admin.admin.index', compact('admins'));
  }

  public function create()
  {
    session()->put('side_bar', 'admin');
    return view('admin.admin.insert');
  }

  public function store(AdminRequest $request)
  {
      session()->put('side_bar', 'admin');
      Admin::add($request);
      return redirect('admin/admins')->with('message', 'تم اضافة عضو الى الادارة .');
  }

  public function destroy($id)
  {
    session()->put('side_bar', 'admin');
    Admin::findOrFail($id)->delete();
    return redirect()->back()->with('message', 'تم مسح عضو من الادارة .');
  }
}
