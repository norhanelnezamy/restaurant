<?php

namespace App\Http\Controllers\Restaurant;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Feedback;
use Auth;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feedbacks = Feedback::orderBy('created_at', 'DESC')->join('users', 'feedbacks.user_id', 'users.id')
                                                            ->join('branches', 'feedbacks.branch_id', 'branches.id')
                                                            ->join('areas', 'branches.area_id', 'areas.id')
                                                            ->join('cities', 'areas.city_id', 'cities.id')
                                                            ->join('countries', 'cities.country_id', 'countries.id')
                                                            ->where('branches.brand_id', Auth::guard('web')->user()->id)
                                                            ->select('feedbacks.*', 'users.*', 'areas.ar_name as area_ar_name', 'areas.en_name as area_en_name', 'cities.ar_name as city_ar_name', 'cities.en_name as city_en_name', 'countries.ar_name as country_ar_name', 'countries.en_name as country_en_name')
                                                            ->paginate(20);
       return view('dashboard.feedback', compact('feedbacks'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Feedback::findOrFail($id)->delete();
       return redirect()->back()->with('message', 'تم مسح رأي العميل .');
    }
}
