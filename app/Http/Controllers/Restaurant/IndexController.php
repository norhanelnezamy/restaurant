<?php

namespace App\Http\Controllers\Restaurant;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Brand;
use Auth;

class IndexController extends Controller
{

    public function index()
    {
      // return  Auth::guard('web')->user();
      $brand = Brand::findOrFail(Auth::guard('web')->user()->brand->id);
      return view('dashboard.data', compact('brand'));
    }

    public function msg()
    {
      $user = Auth::user();
      return $user;
      if ($user->admin_active == 1) {
        if ($user->role == 'smart_restaurant' || $user->role == 'restaurant' || $user->role == 'productive_family' ) {
          return redirect('/dashboard/index');
        }
        return redirect('/dashboard/b/menu');
      }
      return "Your account not active till now from admin .!";
    }

}
