<?php

namespace App\Http\Controllers\Restaurant;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RestaurantRequest;
use App\Model\User;
use App\Model\Brand;
use App\Model\Branch;

class BrandController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RestaurantRequest $request)
    {
      $user = User::add($request);
      // active_email($user);
      $brand = Brand::add($request, $user->id);
      if ($user->role == 'productive_family') {
        $brand = Branch::family($user->id , $brand->id);
      }
      return redirect()->back()->with('add_account', 'Account is created please, active your acount');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if (sizeof($request->menu_photos) > 0) {
        foreach ($request->menu_photos as $key => $photo) {
          $photos[$key] = upload_file($photo);
        }
        $request->menu_photos = json_encode($photos, true);
      }
      $brand = Brand::edit($request, $id);
      if ($request->has('address')) {
        Branch::where([['brand_id',$brand->id]])->update(['address'=> $request->address]);
      }
      return redirect()->back();
    }

}
