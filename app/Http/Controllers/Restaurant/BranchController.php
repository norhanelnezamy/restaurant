<?php

namespace App\Http\Controllers\Restaurant;

use App\Http\Requests\BranchRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\User;
use App\Model\Brand;
use App\Model\Branch;
use App\Model\City;
use Auth;

class BranchController extends Controller
{

    public function __construct()
    {
        $this->middleware('brand_authorized', ['only' => ['show', 'update', 'destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::join('brands', 'branches.brand_id', 'brands.id')->where('brands.user_id', Auth::guard('web')->user()->id)
                           ->join('areas', 'branches.area_id', 'areas.id')
                           ->join('cities', 'areas.city_id', 'cities.id')
                           ->select('branches.*', 'areas.ar_name as area_ar_name', 'areas.en_name as area_en_name', 'cities.ar_name as city_ar_name', 'cities.en_name as city_en_name')
                           ->paginate(20);
        return view('dashboard.branch.index', compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::where('country_id', Auth::guard('web')->user()->brand->country_id)->get();
        return view('dashboard.branch.insert', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchRequest $request)
    {
        $request->role = 'branch';
        $user = User::add($request);
        // active_email($user);
        $branch = Branch::family($user->id, Auth::guard('web')->user()->brand->id);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $branch = Branch::where('id', $id)->join('brands', 'branches.brand_id', 'brands.id')
                                        ->where('brands.user_id', Auth::guard('web')->user()->id)
                                        ->join('countries', 'brands.country_id', 'countries.id')
                                        ->join('cities', 'countries.id', 'cities.country_id')
                                        ->join('areas', 'cities.id', 'areas.city_id')
                                        ->select('branche.*', 'brands.*', 'countries.ar_name as country', 'cities.ar_name as city', 'areas.ar_name as area')
                                        ->first();
      return view('admin.branche.show', compact('branche'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $branch = Branch::edit($request, $id);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Branch::findOrFail($id)->delete();
        return redirect()->back();
    }
}
