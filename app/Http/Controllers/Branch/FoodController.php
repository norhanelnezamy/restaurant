<?php

namespace App\Http\Controllers\Branch;

use App\Http\Requests\FoodRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Menu;
use App\Model\Food;
use App\Model\FoodDatail;
use Auth;

class FoodController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = Menu::where('branch_id', Auth::guard('web')->user()->branch->id)->get();
        return view('dashboard.food.insert', compact('menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FoodRequest $request)
    {
      $size = array();
      if ($request->has('large')) {
        $size['large'] = $request->large_price;
      }
      elseif ($request->has('medium')) {
        $size['medium'] = $request->medium_price;
      }
      elseif ($request->has('small')) {
        $size['small'] = $request->small_price;
      }
      else {
        $size['one_size'] = $request->one_size_price;
      }
      $request->size = json_encode($size, true);
      $food = Food::add($request);
      return redirect()->back()->with('add_message', ' Item has added successfully .');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $menus = Menu::where('branch_id', Auth::guard('web')->user()->branch->id)->get();
      $food = Food::findOrFail($id);
      return view('dashboard.food.update', compact('food', 'menus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FoodRequest $request, $id)
    {
      $size = array();
      if ($request->has('large')) {
        $size['large'] = $request->large_price;
      }
      elseif ($request->has('medium')) {
        $size['medium'] = $request->medium_price;
      }
      elseif ($request->has('small')) {
        $size['small'] = $request->small_price;
      }
      else {
        $size = array();
        $size['one_size'] = $request->one_size_price;
      }
      $request->size = json_encode($size, true);
      Food::edit($request, $id);
      return redirect()->back()->with('update_message', 'Item has updated successfully .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Food::findOrFail($id)->delete();
      return redirect()->back()->with('delete_message', 'Item has deleted .');
    }
}
