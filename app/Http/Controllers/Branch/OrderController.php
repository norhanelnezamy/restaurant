<?php

namespace App\Http\Controllers\Branch;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Model\Order;
use App\Model\OrderProduct;
use App\Model\Resevation;
use Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {

        $orders = Order::where('branch_id', Auth::user()->branch->id)->where('orders.status', '!=', 0)
                                                                     ->where('orders.type', $type)
                                                                     ->join('users', 'orders.user_id', 'users.id')
                                                                     ->orderBy('orders.created_at', 'DESC')
                                                                     ->select('orders.*', 'users.name')
                                                                     ->paginate(10);
        return view('dashboard.order.index', compact('orders', 'type'));
    }

    public function reservation()
    {

        $reservations = Resevation::where('branch_id', Auth::user()->branch->id)->join('users', 'resevations.user_id', 'users.id')
                                                                                 ->orderBy('resevations.created_at', 'DESC')
                                                                                 ->select('resevations.*', 'users.name')
                                                                                 ->paginate(10);
        return view('dashboard.order.reservation', compact('reservations'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $order = Order::where('orders.branch_id', Auth::user()->branch->id)->where('orders.id', $id)
                                                                         ->join('users', 'orders.user_id', 'users.id')
                                                                         ->select('orders.*', 'users.name', 'users.email', 'users.phone')
                                                                         ->first();
      $products = OrderProduct::where('order_id', $id)->get();
      return view('dashboard.order.show', compact('order', 'products'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::findOrFail($id)->delete();
        return redirect()->back()->with('delete_message', 'Item has deleted .');
    }
}
