<?php

namespace App\Http\Controllers\Branch;
use App\Http\Requests\OfferRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Offer;
use Auth;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = Offer::where('branch_id', Auth::guard('web')->user()->branch->id)->paginate(20);
        return view('dashboard.offer.index', compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.offer.insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OfferRequest $request)
    {
        $offer = Offer::add($request, Auth::guard('web')->user()->branch->id);
        return redirect()->back()->with('add_message', ' Item has added successfully .');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $offer = Offer::findOrFail($id);
      return view('dashboard.offer.update', compact('offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OfferRequest $request, $id)
    {
      $offer = Offer::edit($request, $id);
      return redirect()->back()->with('update_message', 'Item has updated successfully .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Offer::findOrFail($id)->delete();
      return redirect()->back()->with('delete_message', 'Item has deleted .');
    }
}
