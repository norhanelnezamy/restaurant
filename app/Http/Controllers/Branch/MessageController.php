<?php

namespace App\Http\Controllers\Branch;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Message;
use App\Model\Order;
use Auth;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('msg_authorize');
    }

    public function acceptMessage($id)
    {
      $msg = 'تم قبول الطلب من الادارة .';
      $order = Order::findOrFail($id);
      $order->status = 2;
      $order->save();
      Message::add($msg, $order->user_id, Auth::user()->branch->id);
      return redirect()->back()->with('msg', 'You have accept an order .');
    }

    public function refuseMessage($id)
    {
      $msg = 'تم الغاء الطلب من الادارة .';
      $order = Order::findOrFail($id);
      $order->status = 0;
      $order->save();
      Message::add($msg, $order->user_id, Auth::user()->branch->id);
      return redirect()->back()->with('msg', 'You have refused an order .');
    }

    public function getSendMessage($id)
    {
      return view('dashboard.message', compact('id'));
    }


    public function postSendMessage(Request $request, $id)
    {
      $order = Order::findOrFail($id);
      $message = Message::add($request->content, $order->user_id, Auth::user()->branch->id);
      return redirect()->back()->with('msg', 'Your Message has sent .');
    }

}
