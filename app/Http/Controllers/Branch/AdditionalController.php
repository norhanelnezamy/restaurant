<?php

namespace App\Http\Controllers\Branch;

use Illuminate\Http\Request;
use App\Http\Requests\AdditionalRequest;
use App\Http\Controllers\Controller;
use App\Model\Additional;
use Auth;

class AdditionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $additionals = Additional::where('branch_id', Auth::guard('web')->user()->branch->id)->paginate(20);
      return view('dashboard.menu.additional', compact('additionals'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdditionalRequest $request)
    {
      Additional::add($request);
      return redirect()->back()->with('add_message', ' Item has added successfully .');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdditionalRequest $request, $id)
    {
      Additional::edit($request, $id);
      return redirect()->back()->with('update_message', 'Item has updated successfully .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Additional::findOrFail($id)->delete();
      return redirect()->back()->with('delete_message', 'Item has deleted .');
    }
}
