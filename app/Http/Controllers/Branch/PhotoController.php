<?php

namespace App\Http\Controllers\Branch;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Model\BranchPhoto;
use Auth;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photos = BranchPhoto::where('branch_id', Auth::user()->branch->id)->paginate(20);
        return view('dashboard.photo', compact('photos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        BranchPhoto::add($request, Auth::user()->branch->id);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BranchPhoto::where([['id', $id], ['branch_id', Auth::user()->branch->id]])->delete();
        return redirect()->back();
    }
}
