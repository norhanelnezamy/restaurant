<?php

namespace App\Http\Controllers\Branch;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Feedback;
use Auth;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feedbacks = Feedback::orderBy('feedbacks.created_at', 'DESC')->join('users', 'feedbacks.user_id', 'users.id')
                                                                      ->join('branches', 'feedbacks.branch_id', 'branches.id')
                                                                      ->where('feedbacks.branch_id', Auth::user()->branch->id)
                                                                      ->select('feedbacks.*', 'users.name')
                                                                      ->paginate(20);
       return view('dashboard.feedback', compact('feedbacks'));
    }

}
