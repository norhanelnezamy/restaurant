<?php

namespace App\Http\Controllers\Branch;

use App\Http\Requests\NameRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Menu;
use App\Model\Food;
use Auth;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // return Auth::user();
        $menus = Menu::where('branch_id', Auth::guard('web')->user()->branch->id)->paginate(20);
        return view('dashboard.menu.index', compact('menus'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NameRequest $request)
    {
      // return Auth::guard('web')->user();
        $menu = Menu::add($request, Auth::guard('web')->user()->branch->id);
        return redirect()->back()->with('add_message', ' Item has added successfully .');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = Menu::findOrFail($id);
        $foods = Food::where('menu_id', $id)->paginate(20);
        return view('dashboard.menu.show', compact('foods', 'menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NameRequest $request, $id)
    {
        Menu::edit($request, $id);
        return redirect()->back()->with('update_message', 'Item has updated successfully .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Menu::findOrFail($id)->delete();
        return redirect()->back()->with('delete_message', 'Item has deleted .');
    }
}
