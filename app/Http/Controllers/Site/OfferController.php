<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Offer;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = Offer::join('branches','branches.id', 'offers.branch_id')->orderBy('offers.created_at', 'DESC')->paginate(20);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

}
