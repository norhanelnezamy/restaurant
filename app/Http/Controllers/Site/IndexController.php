<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\OrderProduct;
use App\Model\Ad;
use App\Model\SiteFeedback;
use App\Model\Food;

class IndexController extends Controller
{
    public function index()
    {
      session()->put('nav', 'home');
      $products = OrderProduct::where('model_name', 'food_datails')->inRandomOrder()->limit(4)->get();
      $ads = Ad::where('active', 1)->inRandomOrder()->limit(12)->get();
      $products_id = OrderProduct::groupBy('product_id')->where('model_name', 'foods')->orderByRaw('COUNT(*) DESC')->limit(12)->pluck('product_id');
      $brands = Food::join('menus', 'foods.menu_id', 'menus.id')->join('branches', 'menus.branch_id', 'branches.id')
                                                                ->join('brands', 'branches.brand_id', 'brands.id')
                                                                ->join('categories', 'brands.category_id', 'categories.id')
                                                                ->whereIn('foods.id', $products_id)
                                                                ->get(['brands.*','categories.ar_name as category_ar_name', 'categories.en_name as category_en_name']);
      $feedbacks = SiteFeedback::join('users', 'site_feedbacks.user_id', 'users.id')->select('users.name', 'site_feedbacks.content')->inRandomOrder()->limit(12)->get();
      return view('index', compact('feedbacks', 'products', 'ads', 'brands'));
    }

    public function getTerms()
    {
      return view('terms');
    }

}
