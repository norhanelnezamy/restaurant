<?php

namespace App\Http\Controllers\Site;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ReserveRequest;
use App\Http\Requests\OrderRequest;
use App\Model\Order;
use App\Model\OrderProduct;
use App\Model\Branch;
use App\Model\Delivery;
use App\Model\Resevation;
use Auth;
use Cart;

class OrderController extends Controller
{

    public function reservation(ReserveRequest $request,$brand_id ,$branch_id)
    {
      if (Auth::check()) {
        $reservation = Resevation::add($request, Auth::user()->id ,$branch_id);
        return redirect()->back()->with('msg', 'Your reservation request has been sent .');
      }
      session()->put('page', 'brand/p/category/'.$brand_id);
      return redirect('/login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
      if (Auth::check()) {
        if (Cart::content()->count() != 0) {
          $order = Order::add($request, Auth::user()->id);
          foreach (Cart::content() as $key => $product) {
            if ($product->options->branch_id == $request->branch_id) {
              OrderProduct::add($product, $order->id);
              Cart::remove($product->rowId);
            }
          }
          if ($request->type == 'delivery') {
            Delivery::add($request, $order->id);
          }
          return redirect('brand/p/category/'.$request->brand_id)->with('msg', 'Your order has been sent.');
        }
        return redirect()->back();
      }
      session()->put('page', 'checkout/delivery/'.$request->brand_id);
      return redirect('/login');
    }

    public function checkout($id)
    {
      if (Auth::check()) {
        $checkout = 1;
        $branch = Branch::where([['brand_id', $id], ['area_id', session()->get('area')]])->first();
        return view('site.brand.delivery', compact('id', 'branch', 'checkout'));
      }
      session()->put('page', 'checkout/delivery/'.$id);
      return redirect('/login');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $order = Order::findOrFail($id);
      $time_plus_minutes = date("Y-m-d H:i:s", strtotime($order->created_at)+(60*10));
      if ($order->created_at <= $time_plus_minutes) {
        if ($order->user_id == Auth::user()->id) {
          $order->delete();
          return redirect()->back()->with('msg', 'Your order deleted .');
        }
      }
      return redirect()->back()->with('msg', 'Cannot delete Your order .');
    }
}
