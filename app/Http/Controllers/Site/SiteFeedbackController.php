<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests\FeedbackRequest;
use Illuminate\Routing\Controller;
use App\Model\SiteFeedback;
use Auth;

class SiteFeedbackController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeedbackRequest $request)
    {
        SiteFeedback::add($request, Auth::user()->id);
        return redirect()->back();
    }
}
