<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\City;
use App\Model\Country;
use App\Model\Area;
use App;

class CountryController extends Controller
{
    public function index()
    {
        $countries = Country::all();
        return response($countries, 200);
    }

    public function show($id)
    {
      if (App::isLocale('en')){
        $cities = City::where('country_id', $id)->select('id', 'en_name as name')->get();
      }
      else {
        $cities = City::where('country_id', $id)->select('id', 'ar_name as name')->get();
      }
      return response($cities, 200);
    }

    public function area($id)
    {
      if (App::isLocale('en')) {
        $areas = Area::where('city_id', $id)->select('id', 'en_name as name')->get();
      }
      else {
        $areas = Area::where('city_id', $id)->select('id', 'ar_name as name')->get();
      }
      return response($areas, 200);
    }

    public function set_area($id)
    {
      session()->put('area', $id);
      return response('1', 200);
    }

}
