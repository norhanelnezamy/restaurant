<?php

namespace App\Http\Controllers\Site;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\FeedbackRequest;
use App\Http\Requests\FilterRequest;
use App\Model\Brand;
use App\Model\Branch;
use App\Model\Menu;
use App\Model\Feedback;
use App\Model\Additional;
use App\Model\Food;
use App\Model\Area;
use App\Model\Offer;
use App\Model\BranchPhoto;
use Cart;
use Auth;

class BrandController extends Controller
{

    public function index($type)
    {
      session()->put('nav', $type);
      session()->forget('area');
      $brands = Brand::join('users', 'brands.user_id', 'users.id')->join('categories', 'brands.category_id', 'categories.id')
                                                                  ->where('users.role', $type)
                                                                  ->select('brands.*', 'users.role', 'categories.ar_name as category_ar_name', 'categories.en_name as category_en_name')
                                                                  ->paginate(12);
      if ($type == 'restaurant') {
        if (session()->has('area_id') && session()->has('category_id')) {
          $brands = Branch::join('brands', 'branches.brand_id', 'brands.id')->join('users', 'brands.user_id', 'users.id')
                                                                            ->join('categories', 'brands.category_id', 'categories.id')
                                                                            ->where([['users.role', 'restaurant'], ['branches.area_id', session()->get('area_id')], ['category_id', session()->get('category_id')]])
                                                                            ->select('brands.*', 'users.role', 'categories.ar_name as category_ar_name', 'categories.en_name as category_en_name')
                                                                            ->paginate(12);
        }
        return view('site.brand.old_index', compact('brands', 'type'));
      }
      $photos = BranchPhoto::inRandomOrder()->limit(5)->get();
      return view('site.brand.index', compact('brands', 'photos', 'type'));
    }

    public function filterRestaurant(FilterRequest $request){
      session()->put('area_id', $request->area_id);
      session()->put('category_id', $request->category_id);
      return redirect()->back();
    }

    public function show($id)
    {
      session()->put('tab', 'about');
      $brand = Brand::join('categories', 'brands.category_id', 'categories.id')->where('brands.id', $id)
                                                                              ->select('brands.*', 'categories.ar_name as category_ar_name', 'categories.en_name as category_en_name')
                                                                              ->first();
      return view('site.brand.about', compact('brand', 'id'));
    }


    public function getFeedback($id)
    {
      session()->put('tab', 'feedback');
      $feedbacks = Feedback::join('users', 'feedbacks.user_id', 'users.id')->where('feedbacks.branch_id', $id)
                                                                           ->select('feedbacks.*', 'users.name')
                                                                           ->paginate(20);
      return view('site.brand.feedback', compact('feedbacks', 'id'));
    }

    public function postFeedback(FeedbackRequest $request, $id)
    {
      Feedback::add($request, Auth::user()->id, $id);
      return redirect()->back();
    }

    public function category($id)
    {
      // return Cart::content();
      session()->put('tab', 'category');
      if (session()->has('area')) {
        $branch = Branch::where([['brand_id', $id], ['area_id', session()->get('area')]])->first();
        if (sizeof($branch) > 0) {
          $menus = Menu::where('branch_id', $branch->id)->get();
          $photos = BranchPhoto::where('branch_id', $branch->id)->get();
          $additionals = Additional::where('branch_id', $branch->id)->get();
          if (session()->has('filter')) {
            $filter = session()->get('filter');
            if ($filter[0] == 'menu') {
              $foods =Food::join('menus', 'foods.menu_id', 'menus.id')->join('branches', 'menus.branch_id', 'branches.id')
                                                                      ->where('foods.menu_id', $filter[1])
                                                                      ->where('branches.id', $branch->id)
                                                                      ->select('foods.*', 'branches.id as branch_id')
                                                                      ->get();
              $model = 'foods';
            }
            elseif ($filter[0] == 'food_name') {
              $foods =Food::join('menus', 'foods.menu_id', 'menus.id')->join('branches', 'menus.branch_id', 'branches.id')
                                                                      ->where('foods.ar_name',  'like', '%'.$filter[1].'%')
                                                                      ->where('branches.id', $branch->id)
                                                                      ->select('foods.*', 'branches.id as branch_id')
                                                                      ->get();
              $model = 'foods';
            }
            else {
              $foods = Offer::where('branch_id', $branch->id)->get();
              $model = 'offers';
            }
          }
        }
      }
      return view('site.brand.menu', compact('id', 'photos', 'menus', 'additionals', 'foods', 'model', 'branch'));
    }

    public function food($branch_id, $menu_id)
    {
      session()->put('filter', ['menu', $menu_id]);
      return redirect()->back();
    }

    public function search_food(Request $request)
    {
      session()->put('filter', ['food_name', $request->name]);
      return redirect()->back();
    }

    public function offer()
    {
      session()->put('filter', ['offer']);
      return redirect()->back();
    }

}
