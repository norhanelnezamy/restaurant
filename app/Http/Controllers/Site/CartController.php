<?php

namespace App\Http\Controllers\Site;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CartRequest;
use App\Model\Additional;
use Auth;
use Cart;
use App;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CartRequest $request)
    {
      Cart::add([
        'id' => $request->food_id,
        'name' => $request->food_name,
        'qty' => $request->quantity,
        'price' => $request['price'][$request->size],
        'options' => [
           'size' => $request->size,
           'model' => $request->model,
           'branch_id' => $request->branch_id,
        ],
     ]);
      if (!empty($request->additional)) {
        foreach ($request->additional as $key => $one) {
          $one = Additional::findOrFail($one);
          if (App::isLocale('en')) {
            $name = $one->en_name;
          }
          else {
            $name = $one->ar_name;
          }
          Cart::add([
            'id' => $one->id,
            'name' => $name,
            'qty' => $request->quantity,
            'price' => $one->price,
            'options' => [
              'model' => 'additionals',
              'branch_id' => $request->branch_id,
            ],
          ]);
        }
      }
      return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Cart::remove($id);
      return redirect()->back();
    }
}
