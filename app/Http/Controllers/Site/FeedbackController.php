<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests\FeedbackRequest;
use Illuminate\Routing\Controller;
use App\Model\Feedback;
use Auth;

class FeedbackController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeedbackRequest $request)
    {
      Feedback::add($request, Auth::user()->id);
      return redirect('brand/feedback/'.$request->brand_id)->with('msg', 'Your feedback added .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Feedback::where([['id', $id], ['user_id',Auth::user()->id]])->delete();
      return redirect()->back();
    }
}
