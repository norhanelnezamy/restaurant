<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Ad;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ad::join('brands', 'ads.brand_id', 'brands.id')->orderBy('ads.created_at', 'DESC')->limit(4)->paginate(15);
        return response($ads, 200);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ad = Ad::join('brands', 'ads.brand_id', 'brands.id')->where('ads.id', $id)->first();
        return response($ad, 200);
    }
}
