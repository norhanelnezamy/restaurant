<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\City;
use App\Model\Country;
use App\Model\Area;
use App;

class CountryController extends Controller
{
    public function index()
    {
        $countries = Country::all();
        return response($countries, 200);
    }

    public function show($id)
    {
      $cities = City::where('country_id', $id)->get();
      return response($cities, 200);
    }

    public function area($id)
    {
      $areas = Area::where('city_id', $id)->get();
      return response($areas, 200);
    }

    public function set_area($id)
    {
      session()->put('area', $id);
      return response()->json(['status'=>1], 200);
    }

}
