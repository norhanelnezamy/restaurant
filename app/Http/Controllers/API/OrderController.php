<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Order;
use App\Model\OrderProduct;
use App\Model\Food;

class OrderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($token)
    {
      $user = mobileAuth($token);
      $orders = Order::where('user_id', $user->id)->orderBy('created_at', 'DESC')->paginate(15);
      return response($orders, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user = mobileAuth($request->token);
      $order = Order::add($request, $user->id);
      foreach ($request->products as $key => $product) {
        OrderProduct::add($product, $order->id);
      }
      if ($request->type == 'delivery') {
        Delivery::add($request, $order->id);
      }
      return response()->json(['status'=>1, 200]);
    }

    public function reservation(ReserveRequest $request,$branch_id)
    {
      $user = mobileAuth($request->token);
      $reservation = Resevation::add($request, $user->id ,$branch_id);
      return response()->json(['status'=>1, 200]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $token)
    {
      $user = mobileAuth($token);
      $order['details'] = Order::where('id', $id)->where('user_id', $user->id)->first();
      $order['products'] = OrderProduct::where('order_id', $id)->get();
      return response($order, 200);
    }

    public function most_order()
    {
      $products_id = OrderProduct::groupBy('product_id')->where('model_name', 'foods')->orderByRaw('COUNT(*) DESC')->limit(12)->pluck('product_id');
      $branches = Food::join('menus', 'foods.menu_id', 'menus.id')->join('branches', 'menus.branch_id', 'branches.id')
                                                                  ->join('brands', 'branches.brand_id', 'brands.id')
                                                                  ->whereIn('foods.id', $products_id)
                                                                  ->get(['brands.*', 'branches.*']);
      return response($branches, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $token)
    {
      $user = mobileAuth($token);
      Order::findOrFail($id)->where('user_id', $user->id)->delete();
      return response()->json(['status'=>1, 200]);
    }
}
