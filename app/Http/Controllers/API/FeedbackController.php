<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Feedback;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $feedbacks = Feedback::orderBy('created_at', 'DESC')->join('users', 'feedbacks.user_id', 'users.id')
                                                          ->join('branchs', 'feedbacks.branch_id', 'branchs.id')
                                                          ->join('areas', 'branchs.area_id', 'areas.id')
                                                          ->join('cities', 'areas.city_id', 'cities.id')
                                                          ->join('countries', 'cities.country_id', 'country.id')
                                                          ->paginate(15);
     return response($feedbacks, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = mobileAuth($request->token);
        Feedback::add($request, $user->id);
        return response()->json(['status'=>1], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $user = mobileAuth($request->token);
      Feedback::edit($request, $user->id);
      return response()->json(['status'=>1], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $token)
    {
      $user = mobileAuth($token);
      Feedback::where([['id', $id], ['user_id',$user->id]])->delete();
      return response()->json(['status'=>1], 200);
    }
}
