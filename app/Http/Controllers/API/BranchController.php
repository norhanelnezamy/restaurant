<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Branch;
use App\Model\Menu;
use App\Model\Food;
use App\Model\Additional;
use App\Model\BranchPhoto;
use App\Model\Offer;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use AnthonyMartin\GeoLocation\GeoLocation as GeoLocation;

class BranchController extends Controller
{

    public function type_filter($type)
    {
      $branches = Branch::join('brands', 'branches.brand_id', 'brands.id')->join('users', 'brands.user_id', 'users.id')
                                                                          ->where('users.role', $type)
                                                                          ->select('brands.*', 'branches.*')
                                                                          ->paginate(15);
      return response($branches, 200);
    }

    public function area_filter($type, $area_id)
    {
      $branches = Branch::join('brands', 'branches.brand_id', 'brands.id')->join('users', 'brands.user_id', 'users.id')
                                                                          ->where('users.role', $type)
                                                                          ->where('branches.area_id', $area_id)
                                                                          ->select('brands.*', 'branches.*')
                                                                          ->paginate(15);
      return response($branches, 200);
    }

    public function show($id)
    {
      $data['branch'] = Branch::where('branches.id', $id)->join('brands', 'branches.brand_id', 'brands.id')
                                                         ->join('areas', 'branches.area_id', 'areas.id')
                                                         ->join('cities', 'areas.city_id', 'cities.id')
                                                         ->join('countries', 'cities.country_id', 'countries.id')
                                                         ->select('branches.*', 'brands.*', 'countries.ar_name as ar_country', 'countries.en_name as en_country', 'cities.ar_name as ar_city', 'cities.en_name as en_city', 'areas.ar_name as ar_area', 'areas.en_name as en_area')
                                                         ->first();
      $data['photos'] = BranchPhoto::where('branch_id', $id)->pluck('photo');
      $data['service_rate'] = rate('services', $data['branch']['brand_id']);
      $data['food_rate'] = rate('food', $data['branch']['brand_id']);
      $data['delivery_rate'] = rate('delivery', $data['branch']['brand_id']);
      return response($data, 200);
    }

    public function show_menu($id)
    {
      $menu_category = Menu::where('branch_id', $id)->get();
      return response($menu_category, 200);
    }

    public function show_offer($id)
    {
      $offers = Offer::where('branch_id', $id)->orderBy('created_at', 'DESC')->paginate(12);
      return response($offers, 200);
    }

    public function show_menu_food($id)
    {
      try{
          $user = JWTAuth::parseToken()->authenticate();
          $foods = Food::leftJoin('likes', 'foods.id', 'likes.food_id')->where('likes.user_id', $user->id)
                                                                       ->select('foods.*', 'likes.id as like_id')
                                                                       ->paginate(15);

      }catch (JWTException $e){
        $foods = Food::where('menu_id', $id)->paginate(15);
      }
      return response($foods, 200);
    }

    public function show_menu_food_detials($id)
    {
      $data['food'] = Food::join('menus', 'foods.menu_id', 'menus.id')->join('branches', 'menus.branch_id', 'branches.id')
                                                                      ->where('foods.id', $id)
                                                                      ->select('foods.*', 'branches.id as branch_id')
                                                                      ->first();
      $data['food']['size'] = json_decode($data['food']['size'], true);
      $data['size_price'] = array();
      foreach ($data['food']['size'] as $key => $one) {
        array_push($data['size_price'], array($key, $one));
      }
      $data['additionals'] = Additional::where('branch_id', $data['food']['branch_id'])->get();
      return response($data, 200);
    }

    public function nearestBranch($lat, $lng)
    {
      $current = GeoLocation::fromDegrees($lat, $lng);
      $branches = Branch::all();
      $distances = array();
      foreach ($branches as $key => $branch) {
        $branch_location = GeoLocation::fromDegrees($branch->lat, $branch->lng);
        $distances[$branch->id] = $current->distanceTo($branch_location, 'kilometers');
      }
      if (sizeof($distances) > 0) {
        $key = array_search(min($distances), $distances);
        $branch = Branch::join('brands', 'branches.brand_id', 'brands.id')->where('branches.id', $key)->select('branches.id','branches.lat','branches.lng','brands.ar_name', 'brands.en_name', 'brands.logo')->first();
        return response($branch, 200);
      }
      return response()->json(['status' => 0], 200);
    }
}
