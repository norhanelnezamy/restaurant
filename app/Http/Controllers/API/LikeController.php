<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Like;

class LikeController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = mobileAuth($request->token);
        $like = Like::add($request, $user->id);
        return response()->json(['like_id'=>$like->id, 200]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($token)
    {
        $user = mobileAuth($token);
        $likes = Like::where('user_id', $user->id)->join('foods', 'likes.food_id', 'foods.id')
                                                  ->select('likes.id as like_id', 'foods.*')
                                                  ->get();
        return response($likes, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $token)
    {
        $user = mobileAuth($token);
        Like::where([['id', $id],['user_id', $user->id]])->delete();
        return response()->json(['status'=>1, 200]);
    }
}
