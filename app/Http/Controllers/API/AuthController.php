<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use App\Model\User;

class AuthController extends Controller
{
  public function register(Request $request)
  {
    $request->role = "user";
    $user = User::add($request);
    // active_email($user);
    return response($user, 200);
  }

  public function login(Request $request)
  {
    try {
        if (!$token = JWTAuth::attempt($request->only('phone', 'password'))) {
            return response()->json(['status' => 'invalid credentails'], 200);
        }
    }catch (JWTException $e) {
        return response()->json(['status' => "Couldn't create token"], 200);
    }
    if ($request->has('mobile_token') && $request->has('mobile_type')) {
      User::where('phone', $request->phone)->update(['mobile_token' => $request->mobile_token, 'mobile_type' => $request->mobile_type]);
    }
    return response()->json(['user' => JWTAuth::toUser($token), 'token' => $token]);
  }

  public function user($token)
  {
    return response()->json(mobileAuth($token));
  }

}
