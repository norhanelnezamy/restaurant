<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Model\Message;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($token)
    {
        $user = mobileAuth($token);
        $messages =Message::orderBy('created_at', 'DESC')->where('messages.user_id', $user->id)
                                                         ->join('branches', 'messages.branch_id', 'branches.id')
                                                         ->join('brands', 'branches.brand_id', 'brands.id')
                                                         ->select('messages.*', 'brands.ar_name as brand_ar_name', 'brands.en_name as brand_en_name')
                                                         ->paginate(15);
       return response($messages, 200);
    }


}
