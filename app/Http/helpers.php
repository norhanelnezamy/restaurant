
<?php
use Illuminate\Support\Facades\DB;
use App\Model\Country;
use App\Model\Category;
use App\Model\Brand;
use App\Model\Branch;
use App\Model\User;
use App\Model\Offer;
use App\Model\Feedback;
use App\Model\Setting;
use App\Model\Area;
use Tymon\JWTAuth\Exceptions\JWTException;

  function active_email($user){
    Mail::send('active_email',['user' => $user] , function ($msg) use($user){
      $msg->from('khalijalbarmaja@gmail.com', 'Fast Food');
      $msg->to($user->email)->subject('Acount Activation');
    });
  }

  function send_email($request){
    Mail::send('email',['request' => $request] , function ($msg) use($request){
      $msg->from('khalijalbarmaja@gmail.com', 'Fast Food');
      $msg->to($request->email)->subject($request->subject);
    });
  }

  function getProductData($product){
    return DB::table($product->model_name)->where('id', $product->product_id)->first();
  }
  function brand($id){
    return Brand::findOrFail($id);
  }

  function rate($column, $brand_id){
    $feedbacks = Feedback::where('branch_id', $brand_id)->pluck($column);
    $one_count=$two_count=$three_count=$four_count=$five_count=0;
    foreach ($feedbacks as $key => $feedback) {
      if ($feedback == 1) {
        $one_count+=1;
        $one_sum += $feedback;
      }
      if ($feedback == 2) {
        $two_count+=1;
      }
      if ($feedback == 3) {
        $three_count+=1;
      }
      if ($feedback == 4) {
        $four_count+=1;
      }
      if ($feedback == 5) {
        $five_count+=1;
      }
    }
    if ($one_count==0&&$two_count==0&&$three_count==0&&$four_count==0&&$five_count==0) {
      $average = 0;
    }
    else {
      $average = ($one_count*1 + $two_count*2 + $three_count*3 + $four_count*4 + $five_count*5)/($one_count+$two_count+$three_count+$four_count+$five_count);
    }
    return $average;
  }

  function setting(){
    return Setting::first();
  }

  function offer(){
    return Offer::join('branches', 'offers.branch_id', 'branches.id')->inRandomOrder()->limit(8)->select('offers.*', 'branches.brand_id')->get();
  }

  function country(){
    return Country::all();
  }

  function category(){
    return Category::all();
  }

  function productive_family(){
    return User::join('brands', 'users.id', 'brands.user_id')->join('categories', 'brands.category_id', 'categories.id')
    ->select('brands.*', 'categories.ar_name as cate_ar_name', 'categories.en_name as cate_en_name')->where('users.role', 'productive_family')->limit(4)->get();
  }

  function upload_file($file){
    $extension = pathinfo($file->getClientOriginalName() , PATHINFO_EXTENSION);
    $fileName = mt_rand().'.'.$extension ;
    $file->move('public/uploads', $fileName) ;
    return $fileName ;
  }

  function mobileAuth($token){
    try{
      return JWTAuth::toUser($token);
    }catch (JWTException $e){
      try{
          return ['new_token' => JWTAuth::refresh($token)];
      }catch (JWTException $e){
          return ['status' => 'invalid token'];
      }
    }

  }

?>
