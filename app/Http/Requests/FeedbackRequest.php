<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class FeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      if (Auth::check()) {
        return true;
      }
      abort(403);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'food' => 'integer|between:1,5',
          'delivery' => 'integer|between:1,5',
          'services' => 'integer|between:1,5',
          'content' => 'required',
        ];
    }
}
