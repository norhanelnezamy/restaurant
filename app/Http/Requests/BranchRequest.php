<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'required|max:60',
          'phone' => 'required|numeric',
          'email'=>'email|unique:users,email',
          'area_id'=>'required',
          'password'=>'required|alpha_num|between:6,12|',
          // 'commercial_registry'=>'required',
          'civil_registry'=>'required',
          'address'=>'required',
          'delivery'=>'required',
        ];
    }
}
