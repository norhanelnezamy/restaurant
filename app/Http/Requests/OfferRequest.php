<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      if (request()->has('update')) {
        $rules['photo'] = 'image';
      }
      else {
        $rules['photo'] = 'required|image';
      }
      $rules =  [
        'ar_food_name' => 'required',
        'en_food_name' => 'required',
        'ar_food_describe' => 'required',
        'en_food_describe' => 'required',
        'price' => 'required',
      ];
      return $rules;
    }
}
