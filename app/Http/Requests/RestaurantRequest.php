<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RestaurantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      // session()->put('form', 'restaurant');
        return [
          'name' => 'required|max:60',
          'phone' => 'required|numeric|unique:users,phone',
          'email'=>'email|unique:users,email',
          'role'=>'required',
          'country_id'=>'required',
          'password'=>'required|alpha_num|between:6,12|',
          'restaurant_name'=>'required|max:60',
          'terms'=>'required',
          'category_id'=>'required',
        ];
    }
}
