<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      if (request()->has('type')) {
        return [
          'type' => 'required',
        ];
      }
      return [
        'ar_name' => 'required',
        'en_name' => 'required',
      ];
    }
}
