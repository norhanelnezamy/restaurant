<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FoodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      // if (request()->has('update')) {
      //   $rules['photo'] = 'image';
      // }
      // else {
      //   $rules['photo'] = 'required|image';
      // }
      $rules =  [
        'photo' => 'required|image',
        'menu_id' => 'required',
        'ar_food_name' => 'required',
        'en_food_name' => 'required',
        'ar_food_describe' => 'required',
        'en_food_describe' => 'required',
        'one_size' => 'required_without_all:large,medium,small',
        'one_size_price' => 'required_if:one_size,on' ,
        'large' => 'required_without_all:one_size_price,medium,small',
        'large_price' => 'required_if:large,on' ,
        'medium' => 'required_without_all:one_size_price,large,small',
        'medium_price' => 'required_if:medium,on' ,
        'small' => 'required_without_all:one_size_price,large,medium',
        'small_price' => 'required_if:small,on' ,
      ];
      return $rules;
    }
}
