<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
       $rules = [
         'name' => 'required|max:60',
         'phone' => 'required|numeric|unique:users,phone',
       ];
       if (Auth::check()) {
         $rules['email'] = 'email|unique:users,email,'.Auth::user()->id;
         $rules['phone'] = 'required|numeric|unique:users,phone,'.Auth::user()->id;
         $rules['password'] = 'alpha_num|between:6,12|confirmed';
         $rules['password_confirmation'] = 'alpha_num|between:6,12|';
       }
       else {
         $rules['email'] = 'email|unique:users,email';
         $rules['password'] = 'required|alpha_num|between:6,12|confirmed';
         $rules['password_confirmation'] = 'required|alpha_num|between:6,12|';
       }
       return $rules;
     }
}
