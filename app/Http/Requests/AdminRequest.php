<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules(){
       if (request()->has('admin_id')) {
         return [
           'name' => 'required|max:60|unique:admins,name,'.request()->admin_id,
           'password' => 'confirmed',
           'password_confirmation' => '',
         ];
       }
       else {
         return [
           'name' => 'required|max:60|unique:admins,name',
           'password' => 'required|confirmed',
           'password_confirmation' => 'required',
         ];
       }
     }
}
