<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      if (request()->type == 'delivery') {
        return [
          'branch_id' => 'required',
          'latLng' => 'required',
          'floor' => 'required',
          'flat_number' => 'required',
          'details' => 'required|max:250',
        ];
      }
      else {
        return[
          'branch_id' => 'required'
        ];
      }
    }
}
