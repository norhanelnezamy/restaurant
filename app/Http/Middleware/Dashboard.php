<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Model\Brand;

class Dashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
          $user = Auth::user();
          $user->brand = Brand::where('user_id', $user->id)->first();
          if ($user->role == 'smart_restaurant' || $user->role == 'restaurant' || $user->role == 'productive_family' ) {
            return $next($request);
            // if ($user->admin_active == 1) {
            // }
            // return redirect('/msg');
          }
        }
        abort(403);
    }
}
