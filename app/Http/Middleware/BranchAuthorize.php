<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Model\Branch;
use App\Model\Brand;

class BranchAuthorize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->role == 'branch' || $user->role == 'productive_family') {
          $user->branch = Branch::where('user_id', $user->id)->first();
          $user->brand = Brand::where('user_id', $user->branch->brand_id)->first();
          return $next($request);
          // if ($user->admin_active == 1) {
          // }
          // return redirect('/msg');
        }
        abort(403);
    }
}
