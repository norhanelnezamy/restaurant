<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class OrderAuthorize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $user = JWTAuth::parseToken()->authenticate();
            return $next($request);
        }catch (JWTException $e){
            try{
                $token = JWTAuth::getToken();
                $new_token = JWTAuth::refresh($token);
                return response()->json(['status' => 'new token', 'new_token' => $new_token]);
            }catch (JWTException $e){
                return response()->json(['status' => 'invalid token']);
            }
        }
    }
}
