<?php

namespace App\Http\Middleware;

use App\Model\Order;
use Auth;
use Closure;

class MsgAuthorize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $order = Order::where([['branch_id', Auth::user()->branch->id], ['id', $request->route('id')]])->first();
      if (sizeof($order) > 0) {
        return $next($request);
      }
      abort(403);
    }
}
