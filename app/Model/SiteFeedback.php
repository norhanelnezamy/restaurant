<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SiteFeedback extends Model
{
  public static function add($request, $user_id)
  {
    $feedback = new SiteFeedback;
    $feedback->user_id = $user_id ;
    $feedback->content = $request->content ;
    $feedback->save();
  }
}
