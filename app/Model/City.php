<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'ar_name', 'en_name',
    ];

    public static function add($request)
    {
      $city = new City;
      $area->en_name = $request->en_name;
      $area->ar_name = $request->ar_name;
      $city->country_id = $request->country_id;
      $city->save();
    }

    public static function edit($request, $id)
    {
      $city = City::findOrFail($id);
      $area->en_name = $request->en_name;
      $area->ar_name = $request->ar_name;
      $city->save();
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }
}
