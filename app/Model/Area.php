<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $fillable = [
        'ar_name', 'en_name',
    ];

    public static function add($request)
    {
      $area = new Area;
      $area->en_name = $request->en_name;
      $area->ar_name = $request->ar_name;
      $area->city_id = $request->city_id;
      $area->save();
    }

    public static function edit($request, $id)
    {
      $area = Area::findOrFail($id);
      $area->en_name = $request->en_name;
      $area->ar_name = $request->ar_name;
      $area->save();
    }

    public function City()
    {
        return $this->belongsTo('App\City', 'city_id');
    }

}
