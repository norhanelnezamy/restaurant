<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function add($request)
    {
        $user = new User();
        $user->email = str_replace('%40','@',$request->email);
        $user->password = bcrypt($request->password);
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->role = $request->role;
        $user->token = preg_replace('/[^A-Za-z0-9\-\']/', '', bcrypt(mt_rand()));
        if ($request->has('mobile_token')) {
          $user->mobile_token = $request->mobile_token;
          $user->mobile_type = $request->mobile_type;
        }
        $user->email_active = 1;
        $user->save();
        return $user;
    }

    public static function edit($request, $id)
    {
        $user = User::findOrFail($id);
        if (isset($request->password) && !empty($request->password)) {
          $user->password = bcrypt($request->password);
        }
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->save();
        return $user;
    }
}
