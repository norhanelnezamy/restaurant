<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Resevation extends Model
{
  protected $fillable = [
      'numder', 'occasion', 'date', 'other_phone', 'comment'
  ];

  public static function add($request, $user_id, $branch_id)
  {
    $reservation = new Resevation;
    $reservation->user_id = $user_id;
    $reservation->branch_id = $branch_id;
    $reservation->numder = $request->numder;
    $reservation->occasion = $request->occasion;
    $reservation->date = $request->date;
    $reservation->other_phone = $request->other_phone;
    $reservation->comment = $request->comment;
    $reservation->save();
  }

  public static function edit($request, $id)
  {
    $reservation = Resevation::findOrFail($id);
    $reservation->numder = $request->numder;
    $reservation->occasion = $request->occasion;
    $reservation->date = $request->date;
    $reservation->other_phone = $request->other_phone;
    $reservation->comment = $request->comment;
    $reservation->save();
  }

}
