<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
  public static function add($request)
  {
    $admin = new Admin();
    $admin->name = $request->name;
    $admin->password = bcrypt($request->password);
    $admin->save();
  }

  public static function edit($request, $id)
  {
    $admin = Admin::findOrFail($id);
    $admin->name = $request->name;
    if ($request->has('password')) {
      $admin->password = bcrypt($request->password);
    }
    $admin->save();
  }
}
