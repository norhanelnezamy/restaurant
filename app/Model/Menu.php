<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Menu extends Model
{
  protected $fillable = [
      'ar_name', 'en_name'
  ];

  public static function add($request, $branch_id)
  {
    $menu = new Menu;
    $menu->branch_id = $branch_id;
    $menu->ar_name = $request->ar_name;
    $menu->en_name = $request->en_name;
    $menu->save();
  }

  public static function edit($request, $id)
  {
    $menu = Menu::findOrFail($id);
    $menu->ar_name = $request->ar_name;
    $menu->en_name = $request->en_name;
    $menu->save();
  }

}
