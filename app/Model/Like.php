<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
  protected $fillable = [
      'food_id',
  ];

  public static function add($request, $user_id)
  {
    if (empty(Like::where([['user_id', $user_id], ['food_id', $request->food_id]])->first())) {
      $like = new Like;
      $like->user_id = $user_id;
      $like->food_id = $request->food_id;
      $like->save();
    }
  }
}
