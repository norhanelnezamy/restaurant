<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
  protected $fillable = [
      'content',
  ];

  public static function add($msg, $user_id ,$branch_id)
  {
    $message = new Message;
    $message->branch_id = $branch_id;
    $message->user_id = $user_id;
    $message->content = $msg;
    $message->save();
  }

}
