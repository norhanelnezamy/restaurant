<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BranchPhoto extends Model
{
  protected $fillable = ['photo'];

  public static function add($request, $branch_id){
    $photo = new BranchPhoto;
    $photo->branch_id = $branch_id;
    $photo->photo = upload_file($request->photo);
    $photo->save();
  }
}
