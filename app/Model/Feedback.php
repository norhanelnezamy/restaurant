<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
  protected $table = 'feedbacks';

  public static function add($request, $user_id , $brand_id)
  {
    $feedback = new Feedback;
    $feedback->branch_id = $brand_id ;
    $feedback->user_id = $user_id ;
    $feedback->content = $request->content ;
    if (isset($request->delivery)) {
      $feedback->delivery = $request->delivery ;
    }
    if (isset($request->food)) {
      $feedback->food = $request->food ;
    }
    if (isset($request->services)) {
      $feedback->services = $request->services ;
    }
    $feedback->save();
  }

}
