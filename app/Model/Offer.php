<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
  protected $fillable = [
      'ar_name', 'en_name', 'price', 'photo', 'ar_describe', 'en_describe',
  ];

  public static function add($request, $branch_id)
  {
    $offer = new Offer;
    $offer->branch_id = $branch_id;
    $offer->ar_name = $request->ar_food_name;
    $offer->en_name = $request->en_food_name;
    $offer->price = $request->price;
    $offer->photo = upload_file($request->photo);
    $offer->ar_describe = $request->ar_food_describe;
    $offer->en_describe = $request->en_food_describe;
    $offer->save();
  }

  public static function edit($request, $id)
  {
    $offer = Offer::findOrFail($id);
    $offer->ar_name = $request->ar_food_name;
    $offer->en_name = $request->en_food_name;
    $offer->price = $request->price;
    if (!empty($request->photo)) {
      $offer->photo = $request->photo;
    }
    $offer->ar_describe = $request->ar_food_describe;
    $offer->en_describe = $request->en_food_describe;
    $offer->save();
  }

}
