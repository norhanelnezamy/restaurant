<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
  protected $fillable = [
      'ar_name', 'en_name',
  ];

  public static function add($request)
  {
    $country = new Country;
    $area->en_name = $request->en_name;
    $area->ar_name = $request->ar_name;
    $country->save();
  }

  public static function edit($request, $id)
  {
    $country = Country::findOrFail($id);
    $area->en_name = $request->en_name;
    $area->ar_name = $request->ar_name;
    $country->save();
  }
}
