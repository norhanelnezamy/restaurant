<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
  protected $fillable = [
      'ar_name', 'en_name', 'logo', 'ar_details', 'en_details', 'min_charge', 'work_time', 'menus_photos',
  ];

  public static function add($request, $user_id)
  {
    $brand = new Brand;
    $brand->user_id = $user_id;
    $brand->category_id = $request->category_id;
    $brand->country_id = $request->country_id;
    if (session('Lang') == 'ar') {
      $brand->ar_name = $request->restaurant_name;
    }
    else {
      $brand->en_name = $request->restaurant_name;
    }
    $brand->save();
    return $brand;
  }

  public static function edit($request, $id)
  {
    $brand = Brand::findOrFail($id);
    $brand->ar_name = $request->ar_name;
    $brand->en_name = $request->en_name;
    if (sizeof($request->menu_photos) > 0) {
      $brand->menus_photos = $request->menu_photos;
    }
    if (!empty($brand->logo )) {
      $brand->logo = upload_file($request->logo);
    }
    $brand->min_charge = $request->min_charge;
    $brand->work_time = $request->work_time;
    $brand->ar_details = $request->ar_details;
    $brand->en_details = $request->en_details;
    $brand->save();
    return $brand;
  }

  public function branches()
  {
    return $this->hasMany('App\Model\Branch');
  }

}
