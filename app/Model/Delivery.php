<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
  public static function add($request, $order_id)
  {
    $latLng = explode('-', $request->latLng);
    $order_product = new Delivery;
    $order_product->order_id = $order_id;
    $order_product->lat = $latLng[0];
    $order_product->lng = $latLng[1];
    $order_product->floor = $request->floor;
    $order_product->flat_number = $request->flat_number;
    $order_product->details = $request->details;
    $order_product->save();
  }
}
