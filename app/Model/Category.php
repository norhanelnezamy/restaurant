<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'ar_name', 'en_name',
    ];

    public static function add($request)
    {
      $category = new Category;
      $category->ar_name = $request->ar_name;
      $category->en_name = $request->en_name;
      $category->save();
    }

    public static function edit($request, $id)
    {
      $category = Category::findOrFail($id);
      $category->ar_name = $request->ar_name;
      $category->en_name = $request->en_name;
      $category->save();
    }

}
