<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Additional extends Model
{
    public static function add($request){
      $additional = new Additional;
      $additional->branch_id = Auth::guard('web')->user()->branch->id;
      $additional->ar_name = $request->ar_name;
      $additional->en_name = $request->en_name;
      $additional->price = $request->price;
      $additional->type = $request->type;
      $additional->save();
    }

    public static function edit($request, $id){
      $additional = Additional::findOrFail($id);
      $additional->ar_name = $request->ar_name;
      $additional->en_name = $request->en_name;
      $additional->price = $request->price;
      $additional->type = $request->type;
      $additional->save();
    }
}
