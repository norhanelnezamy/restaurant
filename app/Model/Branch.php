<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
  protected $fillable = [
      'area_id', 'lat', 'lng', 'commercial_registry', 'civil_registry', 'address', 'delivery',
  ];

  public static function family($user_id, $brand_id)
  {
    $branch = new Branch;
    $branch->user_id = $user_id;
    $branch->brand_id = $brand_id;
    $branch->save();
  }

  public static function add($request, $user_id, $brand_id)
  {
    $coordinates = explode('-', $request->latLng);
    $branch = new Branch;
    $branch->area_id = $request->area_id;
    $branch->brand_id = $brand_id;
    $branch->user_id = $user_id;
    $branch->lat = $coordinates[0];
    $branch->lng = $coordinates[1];
    $branch->commercial_registry = $request->commercial_registry;
    $branch->civil_registry = $request->civil_registry;
    $branch->address = $request->address;
    $branch->delivery = $request->delivery;
    $branch->save();
  }


  public static function edit($request, $id)
  {
    $coordinates = explode('-', $request->latLng);
    $branch = Branch::findOrFail($id);
    $branch->area_id = $request->area_id;
    $branch->lat = $coordinates[0];
    $branch->lng = $coordinates[1];
    $branch->commercial_registry = $request->commercial_registry;
    $branch->civil_registry = $request->civil_registry;
    $branch->address = $request->address;
    $branch->delivery = $request->delivery;
    $branch->save();
  }
}
