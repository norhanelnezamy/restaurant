<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $fillable = [
      'type', 'total'
  ];

  public static function add($request, $user_id)
  {
    $order = new Order;
    $order->type = $request->type;
    $order->user_id = $user_id;
    $order->branch_id = $request->branch_id;
    $order->total = $request->total;
    $order->save();
    return $order;
  }

  public function products()
  {
      return $this->hasMany('App\Model\OrderProduct');
  }
}
