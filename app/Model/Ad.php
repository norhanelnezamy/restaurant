<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
  protected $fillable = [
      'ar_name', 'en_name', 'photo', 'ar_describe', 'en_describe',
  ];


  public static function add($request, $brand_id)
  {
    $ad = new Ad;
    $ad->brand_id = $brand_id;
    $ad->ar_title = $request->ar_title;
    $ad->en_title = $request->en_title;
    $ad->photo = upload_file($request->photo);
    $ad->ar_describe = $request->ar_describe;
    $ad->en_describe = $request->en_describe;
    $ad->save();
  }

  public static function edit($request, $id)
  {
    $ad = new Ad;
    $ad->ar_name = $request->ar_name;
    $ad->en_title = $request->en_name;
    if (!empty($request->photo)) {
      $ad->photo = $request->photo;
    }
    $ad->ar_describe = $request->ar_describe;
    $ad->en_describe = $request->en_describe;
    $ad->save();
  }
}
