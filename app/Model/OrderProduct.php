<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
  protected $fillable = [
       'quantity',
  ];

  public static function add($product, $order_id)
  {
    $order_product = new OrderProduct;
    $order_product->order_id = $order_id;
    $order_product->quantity = $product->qty;
    $order_product->model_name = $product->options->model;
    $order_product->product_id = $product->id;
    $order_product->size = $product->options->size;
    $order_product->save();
  }

}
