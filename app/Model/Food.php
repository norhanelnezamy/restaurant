<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
  protected $fillable = [
      'menu_id', 'ar_name', 'en_name', 'photo', 'ar_additional', 'en_additional'
  ];

  public static function add($request)
  {
    $food = new Food;
    $food->menu_id = $request->menu_id;
    $food->ar_name = $request->ar_food_name;
    $food->en_name = $request->en_food_name;
    $food->size = $request->size;
    $food->photo = upload_file($request->photo);
    $food->ar_additional = $request->ar_food_describe;
    $food->en_additional = $request->en_food_describe;
    $food->save();
    return $food;
  }


  public static function edit($request, $id)
  {
    $food = Food::findOrFail($id);
    $food->menu_id = $request->menu_id;
    $food->ar_name = $request->ar_name;
    $food->en_name = $request->en_name;
    if (!empty($request->photo)) {
      $food->photo = upload_file($request->photo);
    }
    $food->ar_additional = $request->ar_additional;
    $food->en_additional = $request->en_additional;
    $food->save();
  }
}
