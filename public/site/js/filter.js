jQuery(window).load(function () {

  $('#country').on('change', function() {
    city($(this).val());
      $('select').selectpicker('refresh');
  });

  $('#city').on('change', function() {
    area($(this).val());
    $('select').selectpicker('refresh');
  });

  $('#area').on('change', function() {
    var baseUrl = url();
    $.get(baseUrl+'/set/area/'+ $(this).val(),function(response){
      if (response == 1) {
        location.reload();
      }
      $('select').selectpicker('refresh');
    });
  });

  function city(value){
    var baseUrl = url();
    $.get(baseUrl+'/country/'+ value,function(areas){
      $('#city #city_child').remove();
      for (var i = 0; i < areas.length; i++) {
        $('#city').append($('<option>' , {value:areas[i]['id'] , text:areas[i]['name'] , id: 'city_child'}));
      }
      $('select').selectpicker('refresh');
    });
  }
  function area(value){
    var baseUrl = url();
    $.get(baseUrl+'/area/'+ value,function(areas){
      $('#area #area_child').remove();
      for (var i = 0; i < areas.length; i++) {
        $('#area').append($('<option>' , {value:areas[i]['id'] , text:areas[i]['name'] , id: 'area_child'}));
      }
        $('select').selectpicker('refresh');
    });
  }

  function url(){
    var getUrl = window.location;
    var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    // var baseUrl = getUrl.protocol + "//" + getUrl.host ;
    return baseUrl;
  }

});
