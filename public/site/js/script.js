jQuery(window).load(function () {

    // PAGE LOADER

    $('.pre-load').stop().animate({opacity:0}, 500, function(){$('.pre-load').css({'display':'none'});});
    $('body').css({'overflow-y':'auto'});


    // ANIMATION

    Animate_box();
    $(document).scroll(function (){
        Animate_box();
    });

    function Animate_box() {
        var scroll_var = $(this).scrollTop();

        $('.animate-box').each(function (){
            var val_one = $(this).offset().top - $(window).height() + 80;

            if (scroll_var > val_one){
                if($(this).hasClass('left-in')) {
                    $(this).addClass('animated fadeInLeft');
                }else if($(this).hasClass('right-in')) {
                    $(this).addClass('animated fadeInRight');
                }else {
                    $(this).addClass('animated fadeInUp');
                }
            }
        });
    }

});


$(document).ready(function() {

    $('#checkout_prepare').on('click',function(e){
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            method: 'GET',
            success :function (data) {
                
            }
        });
    });


    // WINDOW HEIGHT

    SliderHeight();
    $(window).resize(function (){
        SliderHeight();
    });

    function SliderHeight() {
        $('.win-height').css({'height': $(window).height()});


        // BOOTSTRAP SELECT

        if($(window).width() > 767) {
            $('select').selectpicker();
        }
    }



    // BURGER

    $(document).scroll(function (){
        var scroll_var = $(this).scrollTop();

        if(scroll_var > $(window).height()/2) {
            $('.fixed-burger').addClass('show');
        }else {
            $('.fixed-burger').removeClass('show');
        }

        if((scroll_var + $(window).height()) > ($(document).height() - 50)) {
            $('.fixed-burger').addClass('stop');
        }else {
            $('.fixed-burger').removeClass('stop');
        }
    });



    // SMOOTH SCROLL

    $('.smooth-a').click(function(){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 1000);
        return false;
    });


    // MENU IN XS

    $('body > *:not(.header), .header > .container').click(function() {
        $('.navbar-collapse').removeClass('in');
        $('.navbar-toggle').addClass('collapsed');
    });


    // SEARCH FORM

    $('.search-form').slideUp(0);
    $('.open-search-btn').click(function () {
        $('.search-form').stop().slideToggle(300);
        setTimeout(function () {
            $('.search-box').toggleClass('show');
        }, 300);
    });


    // COUNTER

    $(".counter button").click(function (a) {
        a.preventDefault();
        var b, c = $(this).parents(".counter").find("input");
        $(this).is(".plus") ? (b = parseInt(c.val()), c.val(b + 1)) :
            $(this).is(".minus") && (b = parseInt(c.val()), b > 1 && c.val(b - 1))
    });


    // UPLOAD IMAGE

    $('.image-uploader').change(function (event){
        $(this).parents('.images-upload-block').append('<div class="uploaded-block"><img src="'+ URL.createObjectURL(event.target.files[0]) +'" alt="..."><button class="close">&times;</button></div>');
        removeUplodedImage();
    });
    removeUplodedImage();
    function removeUplodedImage() {
        $('.uploaded-block .close').click(function (){
            $(this).parents('.uploaded-block').remove();
        });
    }



    // OWL

    var offersOwl = $('.owl-offers');

    var offersOwlFirst = $('#first');
    var offersOwlSecond = $('#second');

    $('.offers-first-next').click(function() {
        offersOwlFirst.trigger('next.owl.carousel');
    });
    $('.offers-first-prev').click(function() {
        offersOwlFirst.trigger('prev.owl.carousel');
    });

    $('.offers-snd-next').click(function() {
        offersOwlSecond.trigger('next.owl.carousel');
    });
    $('.offers-snd-prev').click(function() {
        offersOwlSecond.trigger('prev.owl.carousel');
    });


    var adsOwl = $('.owl-ads');

    $('.ads-prev').click(function() {
        adsOwl.trigger('next.owl.carousel');
    });
    $('.ads-next').click(function() {
        adsOwl.trigger('prev.owl.carousel');
    });

    var singleOwl = $('.owl-single');


    if($('body').css('direction') == 'rtl'){
        console.log('rtl');

        if(offersOwl.length) {
            offersOwl.owlCarousel({
                loop: false,
                margin: 20,
                rtl: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    992: {
                        items: 3
                    },
                    1200: {
                        items: 4
                    }
                }
            });
        }

        if(adsOwl.length) {
            adsOwl.owlCarousel({
                loop: false,
                margin: 20,
                rtl: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    1200: {
                        items: 2
                    }
                }
            });
        }

        if(singleOwl.length) {
            singleOwl.owlCarousel({
                loop: false,
                margin: 10,
                rtl: true,
                dots: true,
                items: 1
            });
        }

    } else {
        console.log('ltr');

        if(offersOwl.length) {
            offersOwl.owlCarousel({
                loop: false,
                margin: 20,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    992: {
                        items: 3
                    },
                    1000: {
                        items: 4
                    }
                }
            });
        }

        if(adsOwl.length) {
            adsOwl.owlCarousel({
                loop: false,
                margin: 20,
                responsive: {
                    0: {
                        items: 1
                    },
                    1200: {
                        items: 2
                    }
                }
            });
        }

        if(singleOwl.length) {
            singleOwl.owlCarousel({
                loop: false,
                margin: 10,
                dots: true,
                items: 1
            });
        }
    }



    // LOGIN FORM CHOOSE

    $('.input-of-form').slideUp(0);

    $('.choose-form').change(function () {
        if($(this).is(':checked')) {
            $('.input-of-form').stop().slideUp();
            $('.input-of-form#' + $(this).val()).stop().slideDown();
        }else {
            $('.input-of-form').stop().slideUp();
        }
    });

    


    // TOOLTIP

    $('[data-toggle="tooltip"]').tooltip();


});


// pop-up

$('.pop-it-up').on('click' , function() {
    $(this).next('.notes-txtarea').toggleClass('popping');
})