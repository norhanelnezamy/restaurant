function printDiv() {
    var w = window.open();
    w.document.write(document.getElementById('print_block').innerHTML);
    w.print();
    w.close();
}


jQuery(window).load(function () {

    // PAGE LOADER

    $('.pre-load').stop().animate({opacity: 0}, 500, function () {
        $('.pre-load').css({'display': 'none'});
    });
    $('body').css({'overflow-y': 'auto'});


    // ANIMATION

    Animate_box();
    $(document).scroll(function () {
        Animate_box();
    });

    function Animate_box() {
        var scroll_var = $(this).scrollTop();

        $('.animate-box').each(function () {
            var val_one = $(this).offset().top - $(window).height() + 80;

            if (scroll_var > val_one) {
                if ($(this).hasClass('left-in')) {
                    $(this).addClass('animated fadeInLeft');
                } else if ($(this).hasClass('right-in')) {
                    $(this).addClass('animated fadeInRight');
                } else {
                    $(this).addClass('animated fadeInUp');
                }
            }
        });
    }

});


$(document).ready(function () {


    // WINDOW HEIGHT

    SliderHeight();
    $(window).resize(function () {
        SliderHeight();
    });

    function SliderHeight() {
        $('.win-height').css({'min-height': $(window).height()});
    }


    // SMOOTH SCROLL

    $('.smooth-a').click(function () {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 1000);
        return false;
    });


    // UPLOAD IMAGE

    $('.image-uploader').change(function (event) {
        $(this).parents('.images-upload-block').append('<div class="uploaded-block"><img src="' + URL.createObjectURL(event.target.files[0]) + '" alt="..."><button class="close">&times;</button></div>');
        removeUplodedImage();
    });
    removeUplodedImage();
    function removeUplodedImage() {
        $('.uploaded-block .close').click(function () {
            $(this).parents('.uploaded-block').remove();
        });
    }


    // DUPLICATE FORM INPUTS

    var newData = $('.new-form-data').html();
    $('.duplicate-btn').click(function (e) {
        e.preventDefault();
        $('.new-rows').append(newData);
    });
    $(document).on('click', '.remove-parent', function (e) {
        e.preventDefault();
        $(this).parents('.div-to-duplicate').remove();
    });



    // CHECKBOX CHOOSE MEAL SIZE

    $('.div-padding').on('click', '.choose-size input[type="checkbox"]', function(){
        if($(this).is(':checked') && $(this).attr('name') == 'one_size'){
            $(this).parent().parent().parent().find('input[type="checkbox"]:not([name="one_size"])').attr('disabled', true);
            $(this).parent().next('input').css({'display':'inline-block'});
        }else if(!$(this).is(':checked') && $(this).attr('name') == 'one_size') {
            $(this).parent().parent().parent().find('input[type="checkbox"]').attr('disabled', false);
            $(this).parent().next('input').css({'display':'none'});
        }else if($(this).is(':checked') && $(this).attr('name') != 'one_size'){
            $(this).parent().parent().parent().find('input[name="one_size"]').attr('disabled', true);
            $(this).parent().next('input').css({'display':'inline-block'});
        }else if(!$(this).is(':checked') && $(this).attr('name') != 'one_size') {
            if(!$('.choose-size input[name="large"]').is(':checked') && !$('.choose-size input[name="medium"]').is(':checked') && !$('.choose-size input[name="small"]').is(':checked')) {
                $(this).parent().parent().parent().find('input[name="one_size"]').attr('disabled', false);
            }
            $(this).parent().next('input').css({'display':'none'});
        }
    });


});