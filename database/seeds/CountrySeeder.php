<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert(['ar_name' => 'المملكة العربية السعودية', 'en_name' => 'Saudi Arabia']);
    }
}
