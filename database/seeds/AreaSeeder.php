<?php

use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('areas')->insert(['ar_name' => 'المصفاة', 'en_name' => 'Misfat', 'city_id' => '1']);
      DB::table('areas')->insert(['ar_name' => 'عريض', 'en_name' => 'Arid', 'city_id' => '1']);
      DB::table('areas')->insert(['ar_name' => 'الهدا', 'en_name' => 'Hada', 'city_id' => '1']);
      DB::table('areas')->insert(['ar_name' => 'الفيحاء', 'en_name' => 'Al Faiha', 'city_id' => '2']);
      DB::table('areas')->insert(['ar_name' => 'التضامن', 'en_name' => 'Al Tadamon', 'city_id' => '2']);
      DB::table('areas')->insert(['ar_name' => 'الفضل', 'en_name' => 'Al Fadel', 'city_id' => '2']);
    }
}
