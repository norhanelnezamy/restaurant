<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('cities')->insert(['ar_name' => 'الرياض', 'en_name' => 'Riyadh', 'country_id' => '1']);
      DB::table('cities')->insert(['ar_name' => 'جدة', 'en_name' => 'Jeddah', 'country_id' => '1']);
    }
}
