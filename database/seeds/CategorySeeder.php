<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->insert(['ar_name' => 'مأكولات ومشروبات', 'en_name' => 'Food and drinkes']);
      DB::table('categories')->insert(['ar_name' => 'سندوتشات', 'en_name' => 'Sandwiches']);
      DB::table('categories')->insert(['ar_name' => 'مأكولات سريعه', 'en_name' => 'Fast food']);
      DB::table('categories')->insert(['ar_name' => 'مشروبات', 'en_name' => 'Drinkes']);
      DB::table('categories')->insert(['ar_name' => 'مأكولات بحرية', 'en_name' => 'Sea food']);
      DB::table('categories')->insert(['ar_name' => 'مأكولات هندية', 'en_name' => 'Indian food']);
      DB::table('categories')->insert(['ar_name' => 'مأكولات صينية', 'en_name' => 'Chinese cuisine']);
      DB::table('categories')->insert(['ar_name' => 'مأكولات امريكية', 'en_name' => 'American food']);
    }
}
