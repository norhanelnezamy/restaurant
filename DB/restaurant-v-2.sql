-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 21, 2017 at 10:36 PM
-- Server version: 5.6.31-0ubuntu0.15.10.1
-- PHP Version: 5.6.11-1ubuntu3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `additionals`
--

CREATE TABLE IF NOT EXISTS `additionals` (
  `id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `additionals`
--

INSERT INTO `additionals` (`id`, `branch_id`, `ar_name`, `en_name`, `price`, `type`, `created_at`, `updated_at`) VALUES
(2, 1, 'سلطة السيزر', 'sesar salad', '30', 'salad', '2017-07-21 18:00:15', '2017-07-21 18:00:15'),
(3, 1, 'بطاطس', 'potatos', '10', 'appetizer', '2017-07-31 14:14:09', '2017-07-31 14:14:09'),
(4, 1, 'بيبسي', 'pepsi', '8', 'drink', '2017-07-31 14:16:54', '2017-07-31 14:16:54'),
(5, 1, 'كاتشب', 'katchep', '11', 'other', '2017-07-31 14:17:34', '2017-07-31 14:17:34'),
(6, 1, 'ميرندا', 'mrinda', '8', 'drink', '2017-07-31 14:20:01', '2017-07-31 14:20:01');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$SSAGRnQGc/ESF8X.kyD/2uaAIGYHSPxAFLLnZPWwTMOV.uAC0os4W', NULL, '2017-07-31 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(10) unsigned NOT NULL,
  `brand_id` int(10) unsigned NOT NULL,
  `ar_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_describe` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_describe` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `brand_id`, `ar_title`, `en_title`, `photo`, `ar_describe`, `en_describe`, `active`, `created_at`, `updated_at`) VALUES
(3, 1, 'هرفي', 'Harfy', '120425216.png', 'يعتبر ماكدونالدز السعودية ضمن قائمة أرقى مطاعم الخدمة السريعة التي تقدم وجبات متنوعة من البرجر بجودة عالية. يمكنكم الآن طلب أي من وجبات ماكدونالدز', 'McDonald''s Saudi Arabia is among the finest fast service restaurants offering high quality Burger meals. You can now order any of McDonald''s meals', 1, '2017-08-11 16:11:09', '2017-08-13 19:00:31'),
(4, 1, 'هرفي', 'Harfy', '678385467.png', 'يعتبر ماكدونالدز السعودية ضمن قائمة أرقى مطاعم الخدمة السريعة التي تقدم وجبات متنوعة من البرجر بجودة عالية. يمكنكم الآن طلب أي من وجبات ماكدونالدز', 'McDonald''s Saudi Arabia is among the finest fast service restaurants offering high quality Burger meals. You can now order any of McDonald''s meals', 1, '2017-08-11 16:12:28', '2017-08-11 16:12:28'),
(5, 1, 'هرفي', 'Harfy', '1284471038.png', 'يعتبر ماكدونالدز السعودية ضمن قائمة أرقى مطاعم الخدمة السريعة التي تقدم وجبات متنوعة من البرجر بجودة عالية. يمكنكم الآن طلب أي من وجبات ماكدونالدز', 'McDonald''s Saudi Arabia is among the finest fast service restaurants offering high quality Burger meals. You can now order any of McDonald''s meals', 1, '2017-08-11 16:13:13', '2017-08-11 16:13:13'),
(6, 1, 'هرفي', 'Harfy', '999296152.png', 'يعتبر ماكدونالدز السعودية ضمن قائمة أرقى مطاعم الخدمة السريعة التي تقدم وجبات متنوعة من البرجر بجودة عالية. يمكنكم الآن طلب أي من وجبات ماكدونالدز', 'McDonald''s Saudi Arabia is among the finest fast service restaurants offering high quality Burger meals. You can now order any of McDonald''s meals', 1, '2017-08-11 16:13:35', '2017-08-11 16:13:35');

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE IF NOT EXISTS `areas` (
  `id` int(10) unsigned NOT NULL,
  `city_id` int(10) unsigned NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `city_id`, `ar_name`, `en_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'المصفاة', 'Misfat', NULL, NULL),
(2, 1, 'عريض', 'Arid', NULL, NULL),
(3, 1, 'الهدا', 'Hada', NULL, NULL),
(4, 2, 'الفيحاء', 'Al Faiha', NULL, NULL),
(5, 2, 'التضامن', 'Al Tadamon', NULL, NULL),
(6, 2, 'الفضل', 'Al Fadel', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE IF NOT EXISTS `branches` (
  `id` int(10) unsigned NOT NULL,
  `area_id` int(10) unsigned DEFAULT NULL,
  `brand_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commercial_registry` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_registry` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `area_id`, `brand_id`, `user_id`, `lat`, `lng`, `commercial_registry`, `civil_registry`, `address`, `delivery`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 13, '24.683945648032207', '46.66992204263806', '9874666', '9874666', '46 شارع الهدي', 20, '2017-07-21 14:22:20', '2017-07-21 14:22:20'),
(3, 1, 8, 21, NULL, NULL, NULL, NULL, NULL, 0, '2017-08-01 08:48:47', '2017-08-01 08:48:47'),
(4, 1, 10, 24, NULL, NULL, NULL, NULL, NULL, 0, '2017-08-01 09:23:36', '2017-08-01 09:23:36'),
(5, 1, 11, 25, NULL, NULL, NULL, NULL, NULL, 0, '2017-08-01 09:29:38', '2017-08-01 09:29:38'),
(6, 1, 12, 27, NULL, NULL, NULL, NULL, NULL, 0, '2017-08-01 09:49:28', '2017-08-01 09:49:28'),
(7, 1, 12, 28, NULL, NULL, NULL, NULL, NULL, 0, '2017-08-01 09:51:23', '2017-08-01 09:51:23'),
(8, 1, 13, 29, NULL, NULL, NULL, NULL, NULL, 0, '2017-08-01 12:24:09', '2017-08-01 12:24:09'),
(9, 1, 14, 30, NULL, NULL, NULL, NULL, NULL, 0, '2017-08-02 07:35:06', '2017-08-02 07:35:06'),
(10, NULL, 15, 32, NULL, NULL, NULL, NULL, NULL, 0, '2017-09-11 14:12:14', '2017-09-11 14:12:14');

-- --------------------------------------------------------

--
-- Table structure for table `branch_photos`
--

CREATE TABLE IF NOT EXISTS `branch_photos` (
  `id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branch_photos`
--

INSERT INTO `branch_photos` (`id`, `branch_id`, `photo`, `created_at`, `updated_at`) VALUES
(1, 1, 'pic1.png', NULL, NULL),
(8, 9, 'pic1.png', NULL, NULL),
(9, 1, 'pic2.png', NULL, NULL),
(16, 9, 'pic2.png', NULL, NULL),
(17, 1, 'pic3.png', NULL, NULL),
(18, 3, 'pic3.png', NULL, NULL),
(19, 4, 'pic3.png', NULL, NULL),
(20, 1, 'pic1.png', NULL, NULL),
(21, 3, 'pic4.png', NULL, NULL),
(22, 4, 'pic4.png', NULL, NULL),
(23, 5, 'pic1.png', NULL, NULL),
(24, 6, 'pic5.png', NULL, NULL),
(25, 7, 'pic1.png', NULL, NULL),
(26, 8, 'pic4.png', NULL, NULL),
(27, 9, 'pic1.png', NULL, NULL),
(28, 1, 'pic5.png', NULL, NULL),
(29, 3, 'pic2.png', NULL, NULL),
(30, 4, 'pic5.png', NULL, NULL),
(31, 5, 'pic2.png', NULL, NULL),
(32, 6, 'pic2.png', NULL, NULL),
(34, 8, 'pic5.png', NULL, NULL),
(35, 9, 'pic5.png', NULL, NULL),
(36, 1, 'pic2.png', NULL, NULL),
(37, 3, 'pic3.png', NULL, NULL),
(38, 4, 'pic3.png', NULL, NULL),
(41, 10, '1603410356.jpg', '2017-09-11 16:27:57', '2017-09-11 16:27:57');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `admin_active` tinyint(1) NOT NULL DEFAULT '0',
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `menus_photos` text COLLATE utf8mb4_unicode_ci,
  `min_charge` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_details` text COLLATE utf8mb4_unicode_ci,
  `en_details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `category_id`, `user_id`, `country_id`, `admin_active`, `ar_name`, `en_name`, `logo`, `menus_photos`, `min_charge`, `work_time`, `ar_details`, `en_details`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 1, 1, 'ماكدوناز', 'Macdonals', 'harfy.jpg', NULL, '20$ ', '10 AM : 11 PM', 'يعتبر المطبخ العربى ضمن قائمة أرقى مطاعم الخدمة السريعة التي تقدم وجبات متنوعة من البرجر بجودة عالية. يمكنكم الآن طلب أي من وجبات ماكدونالدز عبر موقع هنقرستيشن وسنقوم بإيصالها لكم طازجة وخلال مدة زمنية قصيرة', 'The English Angel Espagnole Frances Italiano. Kitchen .. Kitchen. the kitchen. PORTUGZ Pусский Espanyol ITALIANO. Kitchen ... PORTUGALS Pусский ESPANOL CHINA Suppliers Welcome: You can now get orders from McDonald''s meals via Hankerstation and we will enter them fresh in a short time', '2017-07-18 08:38:14', '2017-07-18 14:13:43'),
(8, 1, 21, 1, 1, 'منتجات منزليه', 'Home Productive', 'harfy.jpg', NULL, '20$', '10 AM : 11 PM', 'يعتبر المطبخ العربى ضمن قائمة أرقى مطاعم الخدمة السريعة التي تقدم وجبات متنوعة من البرجر بجودة عالية. يمكنكم الآن طلب أي من وجبات ماكدونالدز عبر موقع هنقرستيشن وسنقوم بإيصالها لكم طازجة وخلال مدة زمنية قصيرة', 'The English Angel Espagnole Frances Italiano. Kitchen .. Kitchen. the kitchen. PORTUGZ Pусский Espanyol ITALIANO. Kitchen ... PORTUGALS Pусский ESPANOL CHINA Suppliers Welcome: You can now get orders from McDonald''s meals via Hankerstation and we will enter them fresh in a short time', '2017-08-01 08:48:46', '2017-08-13 18:57:02'),
(10, 5, 23, 1, 1, 'الشيراتون', 'Sheraton', 'sheraton.jpg', NULL, '80SR', '10 AM : 11 PM', 'يعتبر المطبخ العربى ضمن قائمة أرقى مطاعم الخدمة السريعة التي تقدم وجبات متنوعة من البرجر بجودة عالية. يمكنكم الآن طلب أي من وجبات ماكدونالدز عبر موقع هنقرستيشن وسنقوم بإيصالها لكم طازجة وخلال مدة زمنية قصيرة', 'The English Angel Espagnole Frances Italiano. Kitchen .. Kitchen. the kitchen. PORTUGZ Pусский Espanyol ITALIANO. Kitchen ... PORTUGALS Pусский ESPANOL CHINA Suppliers Welcome: You can now get orders from McDonald''s meals via Hankerstation and we will enter them fresh in a short time', '2017-08-01 09:16:22', '2017-08-13 18:56:15'),
(11, 1, 25, 1, 1, 'سويت هوم', 'Sweet Home', 'default.png', NULL, '25SR', '10 AM : 11 PM', 'يعتبر المطبخ العربى ضمن قائمة أرقى مطاعم الخدمة السريعة التي تقدم وجبات متنوعة من البرجر بجودة عالية. يمكنكم الآن طلب أي من وجبات ماكدونالدز عبر موقع هنقرستيشن وسنقوم بإيصالها لكم طازجة وخلال مدة زمنية قصيرة', 'The English Angel Espagnole Frances Italiano. Kitchen .. Kitchen. the kitchen. PORTUGZ Pусский Espanyol ITALIANO. Kitchen ... PORTUGALS Pусский ESPANOL CHINA Suppliers Welcome: You can now get orders from McDonald''s meals via Hankerstation and we will enter them fresh in a short time', '2017-08-01 09:29:38', '2017-08-01 09:29:38'),
(12, 1, 26, 1, 1, 'ميريديان', 'lemeridien', 'harfy.jpg', NULL, '100SR', '10 AM : 11 PM', 'يعتبر المطبخ العربى ضمن قائمة أرقى مطاعم الخدمة السريعة التي تقدم وجبات متنوعة من البرجر بجودة عالية. يمكنكم الآن طلب أي من وجبات ماكدونالدز عبر موقع هنقرستيشن وسنقوم بإيصالها لكم طازجة وخلال مدة زمنية قصيرة', 'The English Angel Espagnole Frances Italiano. Kitchen .. Kitchen. the kitchen. PORTUGZ Pусский Espanyol ITALIANO. Kitchen ... PORTUGALS Pусский ESPANOL CHINA Suppliers Welcome: You can now get orders from McDonald''s meals via Hankerstation and we will enter them fresh in a short time', '2017-08-01 09:40:42', '2017-08-01 09:48:10'),
(13, 1, 29, 1, 1, 'هوم باستا', 'Home pasta', 'harfy.jpg', NULL, '13$', '10 AM : 11 PM', 'يعتبر المطبخ العربى ضمن قائمة أرقى مطاعم الخدمة السريعة التي تقدم وجبات متنوعة من البرجر بجودة عالية. يمكنكم الآن طلب أي من وجبات ماكدونالدز عبر موقع هنقرستيشن وسنقوم بإيصالها لكم طازجة وخلال مدة زمنية قصيرة', 'The English Angel Espagnole Frances Italiano. Kitchen .. Kitchen. the kitchen. PORTUGZ Pусский Espanyol ITALIANO. Kitchen ... PORTUGALS Pусский ESPANOL CHINA Suppliers Welcome: You can now get orders from McDonald''s meals via Hankerstation and we will enter them fresh in a short time', '2017-08-01 12:24:09', '2017-08-01 12:24:09'),
(14, 2, 30, 1, 1, 'ساندوتش', 'sandwich', 'harfy.jpg', NULL, '14$', '10 AM : 11 PM', 'يعتبر المطبخ العربى ضمن قائمة أرقى مطاعم الخدمة السريعة التي تقدم وجبات متنوعة من البرجر بجودة عالية. يمكنكم الآن طلب أي من وجبات ماكدونالدز عبر موقع هنقرستيشن وسنقوم بإيصالها لكم طازجة وخلال مدة زمنية قصيرة', 'The English Angel Espagnole Frances Italiano. Kitchen .. Kitchen. the kitchen. PORTUGZ Pусский Espanyol ITALIANO. Kitchen ... PORTUGALS Pусский ESPANOL CHINA Suppliers Welcome: You can now get orders from McDonald''s meals via Hankerstation and we will enter them fresh in a short time', '2017-08-02 07:35:06', '2017-08-02 07:35:06'),
(15, 1, 32, 1, 1, 'zxy', NULL, 'default.png', NULL, NULL, NULL, NULL, NULL, '2017-09-11 14:12:14', '2017-09-11 14:12:14');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `ar_name`, `en_name`, `created_at`, `updated_at`) VALUES
(1, 'مأكولات ومشروبات', 'Food and drinkes', NULL, NULL),
(2, 'سندوتشات', 'Sandwiches', NULL, NULL),
(3, 'مأكولات سريعه', 'Fast food', NULL, NULL),
(4, 'مشروبات', 'Drinkes', NULL, NULL),
(5, 'مأكولات بحرية', 'Sea food', NULL, NULL),
(6, 'مأكولات هندية', 'Indian food', NULL, NULL),
(7, 'مأكولات صينية', 'Chinese cuisine', NULL, NULL),
(8, 'مأكولات امريكية', 'American food', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `country_id`, `ar_name`, `en_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'الرياض', 'Riyadh', NULL, NULL),
(2, 1, 'جدة', 'Jeddah', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `ar_name`, `en_name`, `created_at`, `updated_at`) VALUES
(1, 'المملكة العربية السعودية', 'Saudi Arabia', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `deliveries`
--

CREATE TABLE IF NOT EXISTS `deliveries` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `floor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flat_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `deliveries`
--

INSERT INTO `deliveries` (`id`, `order_id`, `lat`, `lng`, `floor`, `flat_number`, `details`, `created_at`, `updated_at`) VALUES
(2, 11, '24.676637111886496', '46.76147472113371', '2', '4', 'تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل', '2017-08-29 10:46:08', '2017-08-29 10:46:08'),
(3, 12, '23.75501421505346', '44.760131891816854', '2', '4', 'تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل', '2017-08-29 10:48:20', '2017-08-29 10:48:20'),
(4, 13, '24.47681679377065', '39.59838878363371', '2', '4', 'تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل', '2017-08-29 10:50:56', '2017-08-29 10:50:56'),
(5, 14, '24.47681679377065', '39.55444347113371', '2', '4', 'تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل تفاصيل', '2017-08-29 10:54:13', '2017-08-29 10:54:13');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE IF NOT EXISTS `feedbacks` (
  `id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `delivery` int(2) NOT NULL DEFAULT '0',
  `food` int(2) NOT NULL DEFAULT '0',
  `services` int(2) NOT NULL DEFAULT '0',
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `branch_id`, `user_id`, `delivery`, `food`, `services`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 5, 4, 4, 'بصراحة التوصيل سريع جدا.والتعامل جيدجدا لكن الاكل متوسط', NULL, NULL),
(3, 1, 5, 5, 4, 4, 'بصراحة التوصيل سريع جدا.والتعامل جيدجدا لكن الاكل متوسط', NULL, NULL),
(4, 1, 23, 4, 3, 4, 'بصراحة التوصيل سريع جدا.والتعامل جيدجدا لكن الاكل متوسط', NULL, NULL),
(5, 10, 13, 4, 4, 3, 'بصراحة التوصيل سريع جدا.والتعامل جيدجدا لكن الاكل متوسط', '2017-08-29 11:17:02', '2017-08-29 11:17:02');

-- --------------------------------------------------------

--
-- Table structure for table `foods`
--

CREATE TABLE IF NOT EXISTS `foods` (
  `id` int(10) unsigned NOT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_additional` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en_additional` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `foods`
--

INSERT INTO `foods` (`id`, `menu_id`, `ar_name`, `en_name`, `size`, `photo`, `ar_additional`, `en_additional`, `created_at`, `updated_at`) VALUES
(1, 2, 'سوبر اتشكن برجر', 'super checkn burger', '{"one_size":"12"}', 'order-pic.png', 'سوبر اتشكن برجر سوبر اتشكن برجر', 'super checkn burger super checkn burger', '2017-07-31 09:40:17', '2017-07-31 09:40:17'),
(2, 2, 'سوبر اتشكن برجر', 'super checkn burger', '{"one_size":"12"}', 'order-pic.png', 'سوبر اتشكن برجر سوبر اتشكن برجر', 'super checkn burger super checkn burger', '2017-07-31 09:41:14', '2017-07-31 09:41:14'),
(3, 2, 'سوبر اتشكن برجر', 'super checkn burger', '{"one_size":"12"}', 'order-pic.png', 'سوبر اتشكن برجر سوبر اتشكن برجر', 'super checkn burger super checkn burger', '2017-07-31 09:43:40', '2017-07-31 09:43:40'),
(4, 3, 'سوبر اتشكن برجر', 'super checkn burger', '{"large":"13"}', 'order-pic.png', 'سوبر اتشكن برجر سوبر اتشكن برجر', 'super checkn burger super checkn burger', '2017-07-31 10:07:00', '2017-07-31 10:07:00'),
(5, 2, 'سوبر اتشكن برجر', 'super checkn burger', '{"large":"30"}', 'order-pic.png', 'سوبر اتشكن برجر سوبر اتشكن برجر', 'super checkn burger super checkn burger', '2017-07-31 10:08:44', '2017-07-31 10:08:44'),
(6, 5, 'ريش', 'Ribs', '{"one_size":"11"}', 'order-pic.png', 'ريش', 'Ribs', '2017-08-01 07:41:34', '2017-08-01 07:41:34'),
(7, 5, 'كباب', 'kabab', '{"large":"3"}', 'order-pic.png', 'كباب', 'kabab', '2017-08-01 07:42:59', '2017-08-01 07:42:59'),
(8, 4, 'خضار', 'Vegetables', '{"large":"8"}', 'order-pic.png', 'Vegetables', 'Vegetables', '2017-08-01 07:45:35', '2017-08-01 07:45:35'),
(9, 2, 'شاورما', 'shawrma', '{"large":"18"}', 'order-pic.png', 'شاورما الدجاج الطازجة وامجهز على الفجم ', 'shawrma', '2017-08-01 07:47:36', '2017-08-01 07:47:36'),
(10, 6, 'جبنه موزريلا', 'Cheese Mozzarella', '{"medium":"60"}', 'order-pic.png', '2 كيلو جبن موزريلا', 'two kilo of Cheese Mozzarella', '2017-08-01 09:05:15', '2017-08-01 09:05:15'),
(11, 7, 'كفته', 'Kufta', '{"one_size":"75"}', 'order-pic.png', 'كيلو كفته مشوي علي الفحمكيلو كفته مشوي علي الفحم', 'Kilo quatto grilled on coal Kilo quatto grilled on coal', '2017-08-01 09:09:52', '2017-08-01 09:09:52'),
(12, 8, 'فطائر لحوم', 'Meat Pies', '{"medium":"35"}', 'order-pic.png', 'فطائر لحوم مشكل', 'Stuffed meat pies', '2017-08-01 09:20:02', '2017-08-01 09:20:02'),
(13, 9, 'جمبري', 'shrimp', '{"one_size":"100"}', 'order-pic.png', 'shrimp', 'shrimp', '2017-08-01 09:28:34', '2017-08-01 09:28:34'),
(14, 9, 'سمك', 'fish', '{"large":"100"}', 'order-pic.png', 'fish', 'fish', '2017-08-01 09:30:36', '2017-08-01 09:30:36'),
(15, 11, 'ميلك شيك بوريو', 'porio milkshake', '{"medium":"40"}', '2017_11:35:42_AM_maxresdefaultبببب.jpg', 'عباره عن ميلك شيك شيكولاته مع ايس كريم وقطع من بسكوت البوريو وصوص الشيكولاته', 'Milk chocolate with ice cream and pieces of burrito biscuits and chocolate sauce', '2017-08-01 09:35:42', '2017-08-01 09:35:42'),
(17, 12, 'الحلويات الشرقيه', 'Eastern sweets', '{"medium":"40"}', 'order-pic.png', 'مجموعه من الحلويات الشرقيه المختلفه (الكنافه - البسبوسه - زلابيه - صوابع زينب)', 'A variety of oriental desserts (Alknafh - Albbsbosh - Zlabih - Zainab stamps)', '2017-08-01 09:44:02', '2017-08-01 09:44:02'),
(18, 15, 'ريش', 'Ribs', '{"one_size":"200"}', 'order-pic.png', 'Ribs', 'Ribs', '2017-08-01 09:53:55', '2017-08-01 09:53:55'),
(19, 13, 'كب كيك', 'cupcake', '{"medium":"60"}', 'order-pic.png', 'كيك بي نكهات مختلفه مجموعه من الكب كيك بي نكهات مختلفهمجموعه من الكب كيك بي نكهات مختلفهمجموعه من الكب', 'A variety of cupcakes with different flavors ', '2017-08-01 10:02:31', '2017-08-01 10:02:31'),
(20, 14, 'عصير كوكتيل', 'Juice Cocktail', '{"one_size":"35"}', 'order-pic.png', 'Juice Cocktail', 'عصير كوكتيل', '2017-08-01 10:03:42', '2017-08-01 10:03:42'),
(21, 16, 'مكرونه بالفراخ', 'chicken pasta', '{"one_size":"50"}', '316919820.jpg', 'مكرونه بالصوص الاحمر ,مقطع عليها قطع فراخ', 'Macaroni with red sauce, cut with chopped chicks', '2017-08-01 12:33:06', '2017-08-01 12:33:06'),
(22, 16, 'مكرونه بالخضراوات', 'pasta with vegetables', '{"one_size":"50"}', '507268500.jpg', 'مكرونه بالصوص الاحمر ,مقطع عليها قطع فراخ', 'Macaroni with red sauce, cut with chopped chicks', '2017-08-01 12:36:11', '2017-08-01 12:36:11');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `food_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `branch_id`, `ar_name`, `en_name`, `created_at`, `updated_at`) VALUES
(2, 1, 'الساندوتش', 'sandwich', '2017-07-21 16:53:51', '2017-07-21 16:53:51'),
(3, 1, 'مكرونة', 'pasta', '2017-07-21 16:54:12', '2017-07-21 16:54:12'),
(4, 1, 'بيتزا', 'pizza', '2017-07-31 12:54:15', '2017-07-31 12:54:15'),
(5, 1, 'المشويات', 'Grills', '2017-08-01 07:24:40', '2017-08-01 07:24:40'),
(6, 3, 'منتجات الألبان', 'milk products', '2017-08-01 08:50:10', '2017-08-01 08:50:10'),
(7, 3, 'مشويات', 'meet', '2017-08-01 09:07:33', '2017-08-01 09:07:33'),
(8, 3, 'الفطائر', 'Pancakes', '2017-08-01 09:18:09', '2017-08-01 09:18:09'),
(9, 4, 'أسماك', 'Fish', '2017-08-01 09:26:22', '2017-08-01 09:26:22'),
(10, 4, 'شوربات', 'soup', '2017-08-01 09:27:03', '2017-08-01 09:27:03'),
(11, 5, 'ميلك شيك', 'Milkshake', '2017-08-01 09:30:37', '2017-08-01 09:30:37'),
(12, 5, 'حلويات شرقيه', 'Eastern sweets', '2017-08-01 09:31:04', '2017-08-01 09:31:04'),
(13, 5, 'كب كيك', 'cupcake', '2017-08-01 09:31:26', '2017-08-01 09:31:26'),
(14, 5, 'مشروبات', 'drinks', '2017-08-01 09:31:52', '2017-08-01 09:31:52'),
(15, 6, 'المشويات', 'Grills', '2017-08-01 09:53:16', '2017-08-01 09:53:16'),
(16, 8, 'مكرونه بالصوص الأحمر', 'Red Sauce pasta', '2017-08-01 12:26:35', '2017-08-01 12:26:35'),
(17, 8, 'مكرونه بالصوص الأبيض', 'White Sauce Pasta', '2017-08-01 12:28:18', '2017-08-01 12:28:18');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `branch_id`, `user_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 13, 'تم قبول الطلب من الادارة .', '2017-08-29 12:29:46', '2017-08-29 12:29:46'),
(2, 1, 13, 'تم قبول الطلب من الادارة .', '2017-08-29 12:30:17', '2017-08-29 12:30:17'),
(3, 1, 13, 'تم قبول الطلب من الادارة .', '2017-08-29 12:31:36', '2017-08-29 12:31:36'),
(4, 1, 13, 'تم قبول الطلب من الادارة .', '2017-08-29 12:38:52', '2017-08-29 12:38:52'),
(5, 1, 13, 'تم قبول الطلب من الادارة .', '2017-08-29 12:40:29', '2017-08-29 12:40:29'),
(6, 1, 13, 'ارسال رساله الي المستخدم عمر محمد من مدير فرع احدي الفروع ردا ع طلبه', '2017-08-29 12:43:39', '2017-08-29 12:43:39'),
(7, 1, 13, 'ارساله رسالة اخرى', '2017-08-29 12:51:37', '2017-08-29 12:51:37');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_06_19_132752_create_countries_table', 1),
(4, '2017_06_19_132808_create_cities_table', 1),
(5, '2017_06_19_132819_create_areas_table', 1),
(6, '2017_06_19_133024_create_categories_table', 1),
(7, '2017_06_19_133041_create_brands_table', 1),
(8, '2017_06_19_133101_create_branches_table', 1),
(9, '2017_06_19_133102_create_branch_photos_table', 1),
(10, '2017_06_19_133259_create_menus_table', 1),
(11, '2017_06_19_133311_create_foods_table', 1),
(13, '2017_06_19_133313_create_offers_table', 1),
(14, '2017_06_19_133617_create_resevations_table', 1),
(15, '2017_06_19_133632_create_orders_table', 1),
(16, '2017_06_19_133633_create_order_products_table', 1),
(17, '2017_06_19_133658_create_likes_table', 1),
(18, '2017_06_19_133735_create_feedbacks_table', 1),
(19, '2017_06_19_133805_create_ads_table', 1),
(20, '2017_06_19_134013_create_admins_table', 1),
(21, '2017_06_21_140025_create_messages_table', 1),
(22, '2017_07_12_133937_create_additionals_table', 1),
(24, '2017_08_10_223321_create_site_feedbacks_table', 2),
(25, '2017_08_10_223218_create_settings_table', 3),
(27, '2017_08_29_104835_create_deliveries_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `ar_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ar_describe` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_describe` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `branch_id`, `ar_name`, `en_name`, `price`, `photo`, `ar_describe`, `en_describe`, `created_at`, `updated_at`) VALUES
(1, 1, 'هرفي', 'HERFY', '100', 'harfy.jpg', 'وجبة عائلية', 'Family meal', '2017-08-01 08:11:15', '2017-08-01 08:11:15'),
(2, 3, 'مشويات مشكل', 'Grilled grill', '200', '316919820.jpg', 'كيلو كفته و كيلو كباب و نصف كيلو', 'Kilo kofta, kilo kebab, half a kilo of feathers and pulled ', '2017-08-01 09:13:29', '2017-08-01 09:13:29'),
(3, 4, 'وجبة عائلية', 'family meal', '200', '507268500.jpg', 'وجبة عائلية', 'family meal', '2017-08-01 09:33:35', '2017-08-01 09:33:35'),
(4, 6, 'وجبة عائلية', 'Family meal', '200', 'sheraton.jpg', 'وجبة عائلية', 'Family meal', '2017-08-01 09:54:42', '2017-08-01 09:54:42'),
(5, 1, 'عرض', 'offer', '17', '425431176.png', 'عرض عرض عرض عرض عرض عرض عرض عرض عرض عرض', 'offer offer offer offer offer offer offer offer offer offer offer offer', '2017-08-01 11:46:20', '2017-08-01 11:46:20');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `branch_id`, `status`, `type`, `total`, `notes`, `created_at`, `updated_at`) VALUES
(5, 13, 6, 1, 'prepare', '200', NULL, '2017-08-29 09:38:42', '2017-08-29 09:38:42'),
(6, 13, 6, 1, 'prepare', '200', NULL, '2017-08-29 09:39:01', '2017-08-29 09:39:01'),
(7, 13, 6, 1, 'prepare', '200', NULL, '2017-08-29 09:41:03', '2017-08-29 09:41:03'),
(11, 13, 6, 1, 'delivery', '400', NULL, '2017-08-29 10:46:07', '2017-08-29 10:46:07'),
(12, 13, 1, 1, 'delivery', '81', NULL, '2017-08-29 10:48:20', '2017-08-29 10:48:20'),
(13, 13, 4, 2, 'delivery', '400', NULL, '2017-08-29 10:50:56', '2017-08-29 12:28:11'),
(14, 13, 1, 2, 'delivery', '142', NULL, '2017-08-29 10:54:12', '2017-08-29 12:40:29');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE IF NOT EXISTS `order_products` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `size` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `order_id`, `size`, `quantity`, `model_name`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 5, NULL, 1, 'offers', 4, '2017-08-29 09:38:42', '2017-08-29 09:38:42'),
(2, 6, 'one_size', 1, 'offers', 4, '2017-08-29 09:39:01', '2017-08-29 09:39:01'),
(3, 7, 'one_size', 1, 'offers', 4, '2017-08-29 09:41:03', '2017-08-29 09:41:03'),
(6, 11, 'one_size', 2, 'foods', 18, '2017-08-29 10:46:07', '2017-08-29 10:46:07'),
(7, 12, 'one_size', 1, 'foods', 4, '2017-08-29 10:48:20', '2017-08-29 10:48:20'),
(8, 12, NULL, 1, 'additionals', 4, '2017-08-29 10:48:20', '2017-08-29 10:48:20'),
(9, 12, NULL, 1, 'additionals', 2, '2017-08-29 10:48:20', '2017-08-29 10:48:20'),
(10, 12, NULL, 1, 'additionals', 3, '2017-08-29 10:48:20', '2017-08-29 10:48:20'),
(11, 13, 'one_size', 4, 'foods', 13, '2017-08-29 10:50:56', '2017-08-29 10:50:56'),
(12, 14, 'large', 2, 'foods', 4, '2017-08-29 10:54:12', '2017-08-29 10:54:12'),
(13, 14, NULL, 2, 'additionals', 4, '2017-08-29 10:54:12', '2017-08-29 10:54:12'),
(14, 14, NULL, 2, 'additionals', 2, '2017-08-29 10:54:12', '2017-08-29 10:54:12'),
(15, 14, NULL, 2, 'additionals', 3, '2017-08-29 10:54:13', '2017-08-29 10:54:13');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `resevations`
--

CREATE TABLE IF NOT EXISTS `resevations` (
  `id` int(10) unsigned NOT NULL,
  `branch_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `numder` int(11) NOT NULL,
  `occasion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `other_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resevations`
--

INSERT INTO `resevations` (`id`, `branch_id`, `user_id`, `numder`, `occasion`, `date`, `other_phone`, `comment`, `created_at`, `updated_at`) VALUES
(1, 1, 13, 3, 'مناسبة عيدميلاد', '2017-10-10 00:00:00', '01222222222', NULL, '2017-08-27 20:19:06', '2017-08-27 20:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instgram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ar_what` text COLLATE utf8mb4_unicode_ci,
  `en_what` text COLLATE utf8mb4_unicode_ci,
  `ar_why` text COLLATE utf8mb4_unicode_ci,
  `en_why` text COLLATE utf8mb4_unicode_ci,
  `ar_about` text COLLATE utf8mb4_unicode_ci,
  `en_about` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `facebook`, `twitter`, `instgram`, `google`, `ar_what`, `en_what`, `ar_why`, `en_why`, `ar_about`, `en_about`, `created_at`, `updated_at`) VALUES
(1, 'https://www.facebook.com', 'https://www.twitter.com', 'https://www.instgram.com', 'https://www.google.plus.com', 'لا داعي للاتصال بالمطعم و البحث عن أرقام المطاعم بعد الآن! مع موقع اسرع طعام كل ما عليك هو اختيار طلبك من مطعمك المفضل و تنفيذ الطلب و بإمكانك الدفع نقداً عند استلام الطلب، موقع اسرع طعام من أوائل المنصات لطلب الطعام من خلال الانترنت في المنطقة و يعتبر أكبر منصات المطاعم ', 'No need to contact the restaurant and look for restaurant numbers anymore! With Fast Food, you can choose your order from your favorite restaurant and apply. You can pay in cash upon receipt of the order. Fastest food is one of the first online catering platforms in the region.', 'في كل فترة لمرة واحدة على الاقل قد تواجه لحظات مزعجة جدا في عملية طلب الطعام بالطرق التقليدية، - المطعم لا يجيب على الهاتف، ! اسرع طعام هو الحل لمعالجة هذه الحالات و توفير و ضمان للخدمة المتميزة', 'At least once for a time you may encounter very troublesome moments in the process of ordering food in traditional ways, - the restaurant does not answer the phone,! Fast Food is the solution to address these situations and provide and guarantee the service excellence', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص', 'This text is an example of text that can be replaced in the same space. This text has been generated from the Arabic text generator, where you can generate such text or many other texts in addition to increasing the number of characters generated by the application. This text is an example of text that can be replaced In the same space, this text has been generated', NULL, '2017-08-11 21:38:33');

-- --------------------------------------------------------

--
-- Table structure for table `site_feedbacks`
--

CREATE TABLE IF NOT EXISTS `site_feedbacks` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_feedbacks`
--

INSERT INTO `site_feedbacks` (`id`, `user_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 13, 'جربت الموقع وكنت الخدمة جميلة ومريحة بالتوفيق لهم وشئ جميل بصراحة', '2017-08-10 21:08:48', '2017-08-10 21:08:48'),
(2, 13, 'جربت الموقع وكنت الخدمة جميلة ومريحة بالتوفيق لهم وشئ جميل بصراحة', '2017-08-10 21:08:57', '2017-08-10 21:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_active` tinyint(1) NOT NULL DEFAULT '0',
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `password`, `token`, `mobile_token`, `mobile_type`, `email_active`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'ahmed', 'ahmed@gmail.com', '0111', '$2y$10$aJT63yeYtYTztBSLXSyX3OID0oANAsLoKWcpBiOZS6/EgUOb2tv1y', '2y10uXVwwo4d2RNWngNIM93POcdb4IkLssfuAvJU8VevpjUn9aCdHQoK', NULL, NULL, 1, 'restaurant', '8LfDtber72JBau9YD3Atgt0FDaPjaM2kZgRSsvIHrwuubVawIhJiVKQCsZTZ', '2017-07-18 08:38:14', '2017-07-18 08:38:14'),
(13, 'Omar Mohamed', 'omar@gmail.com', '2222', '$2y$10$qPAWNG/NlgkoWw0sgQkEqO4Z5YHbl0YUyfSZPxg9YAmq55FvkGqB.', '2y10AW74n6FcweB0OGnKA0BNuZ25MQOX9FPyxG80mq3VDhAPHsPhS3be', NULL, NULL, 1, 'branch', 'm2ObXDGoOidpjVeWrfZvCRpCwbYWAeNr0yAt1lZn0FJaNptKBlkSbB8lOh6z', '2017-07-21 14:22:20', '2017-08-27 21:16:34'),
(16, 'noha', 'noha@hotmail.com', '123456789', '$2y$10$RtSfu.V9QQHkcVT0zVBLT.QTQ2zsZ0oAMrDn1C0j1ug.DqJmnxp8m', '2y104j7g3oLaGGpLXt44CCVubHfATclqmyUVjsEWGB3frmGTXa6q', NULL, NULL, 1, 'branch', NULL, '2017-08-01 08:13:06', '2017-08-01 08:13:06'),
(21, 'mona', 'mona@hotmail.com', '111111111', '$2y$10$aJT63yeYtYTztBSLXSyX3OID0oANAsLoKWcpBiOZS6/EgUOb2tv1y', '2y10HD4RiswCD0lrs2t7AUqeQOqfBhvIS7MKCIO2dhcur4oMsH4W', NULL, NULL, 1, 'productive_family', 'elpkFVsT24tJtUZnfOTwnCWEu5Nh3BWqY5Dvnak2DTf0z0sSTJNgNhb0rTHo', '2017-08-01 08:48:46', '2017-08-01 08:48:46'),
(23, 'salma', 'khalij.albarmaja@gmail.com', '123456788', '$2y$10$xtH6gePyeGBnCLdrIkcilem0SD442A6cfReBGJutcd.xFfm1z1yeG', '2y10vAxILanDgMTBwwsIvTuQrOUZSSKjrzTDhTzfWv5ZAVcQ7t9JcW', NULL, NULL, 1, 'smart_restaurant', 'KyxQqRa7A239tZZQjNnymMzQZCx1rILUAvTV00rsSWGa5Cj09YioMYiYet5f', '2017-08-01 09:16:22', '2017-08-01 09:16:22'),
(24, 'salma', 'khalij.albarmaj@gmail.com', '12345678', '$2y$10$mkvbuvJ9sfs/WQKOCsUpjuuKMN2f2/.QT39V3pBYxm5ILgCVDZ39m', '2y10Ks9yIQnwm6R53zsJKb0CFOeoUHsRaEbV4G3ejcxSUATZGEdEZ6IF6', NULL, NULL, 1, 'branch', 'OkIaGSsn5zsRGWbG61XMhvwfqgG4SQD9PVgRGPhHgZLC9EJ5Q5XsbCKRTLhK', '2017-08-01 09:23:36', '2017-08-01 09:23:36'),
(25, 'Sara', 'sara@hotmail.com', '222222222', '$2y$10$yzvDKShqakoYbIlNaqMioOHTUyZy21JGKQdsmdkD0v1Npg7Dp2Ley', '2y10H4qGKEA8rTQEL2858VZSOO974tGlNdhjdI3Y2RvQSXuQFMVLWLpW', NULL, NULL, 1, 'productive_family', NULL, '2017-08-01 09:29:38', '2017-08-01 09:29:38'),
(26, 'نورهان', 'design@yahoo.com', '11223344', '$2y$10$aJT63yeYtYTztBSLXSyX3OID0oANAsLoKWcpBiOZS6/EgUOb2tv1y', '2y10aOUsfWwxfJcEde0jQp6zOu7lS3Lvvpb5YmxgU1LEUXOACv9Si', NULL, NULL, 1, 'smart_restaurant', '5jLIKo9vk0aShzynSioGgpWWTJYrzMnLxaK7BQJ04ixnkt2YlNwQlXabAKJP', '2017-08-01 09:40:42', '2017-08-01 09:40:42'),
(27, '  نورهان احمد', '2design@yahoo.com', '112233444', '$2y$10$QBubI0MrVI.qpXB6jfbWQei60IYPMvPTZfJv5IXVw1bB4ZlMrSrru', '2y10w72WpNKTZZIojTj6829UNLbNqxXCmC2YzZTBqKueMcvccJcHEwG', NULL, NULL, 1, 'branch', 'eSORHQMjXTKcokLhm87z57lG0vXYYAH9U0QRsfguc6WnlvX99uszRpO9yi8F', '2017-08-01 09:49:28', '2017-08-01 09:49:28'),
(28, 'نورهان الشناوي', '3design@yahoo.com', '1122334455', '$2y$10$2vEnPW5f3SK.RqsfC5d7D.TOSHv6DrOvl3yY2XlV5Q33Hh7d4IE9u', '2y10d183emKR5g7kBxQr1AM2icktRKhbxnHg3ZNTTKeSNBdwzSzM79a', NULL, NULL, 1, 'branch', NULL, '2017-08-01 09:51:23', '2017-08-01 09:51:23'),
(29, 'mohamed', 'mohamed@hotmail.com', '333333333', '$2y$10$fMxN46wQ5usYNLhozW1w4ehnfxIgmabfmZ45dZvo8NUjJe9IKGR/e', '2y10NYaxLjnFIJ1KgsjRTvIpIed0Uo6FJ5TBEI8BXLU0I94Go9BpxdYW', NULL, NULL, 1, 'productive_family', NULL, '2017-08-01 12:24:09', '2017-08-01 12:24:09'),
(30, 'nada', 'nada@hotmail.com', '444444444', '$2y$10$aJT63yeYtYTztBSLXSyX3OID0oANAsLoKWcpBiOZS6/EgUOb2tv1y', '2y10h7A6d0PGgpg1VKUooLM4OwBh5paJMA4bDAuvPsZg0aSaGsYZdtRW', NULL, NULL, 1, 'productive_family', 'P6dpaPzrOM9nkLUV06hPQFzAnIKAivGWPMw36DR8EtzmvkpOu53846GcyUcx', '2017-08-02 07:35:06', '2017-08-02 07:35:06'),
(31, 'IOS user', '', '1234', '$2y$10$4lbF/XBmJpiQ7Qd3vvsdvemkN9kGY.aUMQ6yhY.XfIwfTypLSopFu', '2y10J46cG8OHzJtjU7Yi7iluZe80FaTlnkSkrgReRznDZLGvYOXoEUKo', NULL, NULL, 1, 'user', NULL, '2017-08-03 12:53:29', '2017-08-03 12:53:29'),
(32, 'xyz', 'norhanelnezamy@gmail.com', '123456', '$2y$10$agrQTSCN87We30gfdE7Xcu9QOo8MXtUZ5SGZYDg371dBcmpuEYrq.', '2y10DcZOXLXbBkzR1Plba47kOD5oUecwcy1MP1Z7Yl4HHD1rq3aDeIq', NULL, NULL, 1, 'productive_family', NULL, '2017-09-11 14:12:14', '2017-09-11 14:12:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `additionals`
--
ALTER TABLE `additionals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `additionals_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ads_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `areas_city_id_foreign` (`city_id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `branches_area_id_foreign` (`area_id`),
  ADD KEY `branches_brand_id_foreign` (`brand_id`),
  ADD KEY `branches_user_id_foreign` (`user_id`);

--
-- Indexes for table `branch_photos`
--
ALTER TABLE `branch_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `branch_photos_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brands_user_id_foreign` (`user_id`),
  ADD KEY `brands_country_id_foreign` (`country_id`),
  ADD KEY `brands_category_id_foreign` (`category_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_country_id_foreign` (`country_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deliveries`
--
ALTER TABLE `deliveries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deliveries_order_id_foreign` (`order_id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `feedbacks_user_id_foreign` (`user_id`),
  ADD KEY `feedbacks_brand_id_foreign` (`branch_id`);

--
-- Indexes for table `foods`
--
ALTER TABLE `foods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foods_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `likes_user_id_foreign` (`user_id`),
  ADD KEY `likes_food_id_foreign` (`food_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menus_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messages_branch_id_foreign` (`branch_id`),
  ADD KEY `messages_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offers_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_branch_id_foreign` (`branch_id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_products_order_id_foreign` (`order_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `resevations`
--
ALTER TABLE `resevations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_feedbacks`
--
ALTER TABLE `site_feedbacks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `site_feedbacks_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `additionals`
--
ALTER TABLE `additionals`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `branch_photos`
--
ALTER TABLE `branch_photos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `deliveries`
--
ALTER TABLE `deliveries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `foods`
--
ALTER TABLE `foods`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `resevations`
--
ALTER TABLE `resevations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `site_feedbacks`
--
ALTER TABLE `site_feedbacks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `additionals`
--
ALTER TABLE `additionals`
  ADD CONSTRAINT `additionals_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ads`
--
ALTER TABLE `ads`
  ADD CONSTRAINT `ads_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `branches`
--
ALTER TABLE `branches`
  ADD CONSTRAINT `branches_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `branches_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `branches_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `branch_photos`
--
ALTER TABLE `branch_photos`
  ADD CONSTRAINT `branch_photos_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `brands`
--
ALTER TABLE `brands`
  ADD CONSTRAINT `brands_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `brands_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `brands_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `deliveries`
--
ALTER TABLE `deliveries`
  ADD CONSTRAINT `deliveries_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD CONSTRAINT `feedbacks_brand_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `feedbacks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `foods`
--
ALTER TABLE `foods`
  ADD CONSTRAINT `foods_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `menus`
--
ALTER TABLE `menus`
  ADD CONSTRAINT `menus_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `offers`
--
ALTER TABLE `offers`
  ADD CONSTRAINT `offers_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_products`
--
ALTER TABLE `order_products`
  ADD CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `site_feedbacks`
--
ALTER TABLE `site_feedbacks`
  ADD CONSTRAINT `site_feedbacks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
